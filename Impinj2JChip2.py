
import os
import sys
import socket
import time
import threading
import datetime
import pprint
import Impinj3
import enum
import six
import keepalive


from enum import Enum
from Tags import TagInfo, Tag, BestEst, StatsHistory, ReaderHistory, Estimate, ReaderPriority, Algorithm
import numpy as np
from queue import Empty
from queue import Queue
from Utils import readDelimitedData, timeoutSecs, Bell

from TagGroup import TagGroup, QuadraticRegressionMethod, StrongestReadMethod, FirstReadMethod, MostReadsChoice, DBMaxChoice


ProcessingMethodDefault                 = QuadraticRegressionMethod
AntennaChoiceDefault                    = MostReadsChoice
RemoveOutliersDefault           = True
ProcessingMethod = ProcessingMethodDefault
AntennaChoice    = AntennaChoiceDefault
RemoveOutliers   = RemoveOutliersDefault



import os
import datetime
getTimeNow = datetime.datetime.now
HOME_DIR = os.path.expanduser("~")

ConnectionTimeoutSecondsDefault = 4             # Interval for connection timeout
ConnectionRetrySecondsDefault = 6             # Interval for connection timeout

RepeatSecondsDefault                    = 10            # Interval in which a tag is considered a repeat read.


ConnectionTimeoutSeconds        = ConnectionTimeoutSecondsDefault
ConnectionRetrySeconds        = ConnectionRetrySecondsDefault
RepeatSeconds                   = RepeatSecondsDefault

class ReaderPosition (Enum):
    pair = 1
    linear = 2
    none = 0

ImpinjReaderPosition = ReaderPosition

# -----------------------------------

class Impinj2Queue( object ):
    def __init__( self, statQ, dataQ0, dataQ, messageQ, shutdownEvent, repeatSeconds, readerPriority):
        ''' Queues:
                        dataQ0:                tag/timestamp data to be written out to CrossMgr.
                        dataQ[]:                tag/timestamp data to be written out to CrossMgr.
                        messageQ:        queue to write status messages.
                        shutdownEvent:        event to signal shutdown.
        '''


        print('TagServer __init__: started AAAA', file=sys.stderr)


        self.tagGroup = None
        #self.tagGroup = TagGroup()
        self.testmode = True
        self.statQ = statQ
        self.dataQ0 = dataQ0
        self.dataQ = dataQ
        self.messageQ = messageQ
        self.shutdownEvent = shutdownEvent
        self.repeatSeconds = repeatSeconds
        self.keepGoing = True
        self.tagCount = 0
        self.readerPriority = readerPriority
        self.messageQ.put( ('Impinj2Queue', 'Impin2Queue Started...' ) )

        print('Impin2Queue: __init__ readerPriority: %s' % self.readerPriority, file=sys.stderr)

        # Create a log file name.
        tNow = getTimeNow()
        dataDir = os.path.join( HOME_DIR, 'ImpinjData' )
        if not os.path.isdir( dataDir ):
            os.makedirs( dataDir )

        self.fname = os.path.join( dataDir, tNow.strftime('Impinj-%Y-%m-%d-%H-%M-%S.txt') )
        with open(self.fname, 'w') as pf:
            pf.write( 'Reader,EPC,FirstTime,LastTime,BestTime,PeakRSSI (-DB*10),RFPhaseAngle,DopplerFreq (Hz),TagSeenCount,Det,Alg,Err,Criteria\n' )
            #pf.write( ' Host, RFMode, SearchMode, TriggerType, Tags, Attempts, T, Timeout\n')
        #pf.close();



    def shutdown( self ):
        self.keepGoing = False

    def checkKeepGoing( self, timeout=0 ):
        #print('Impinj2Queue: checkKeepGoing: %d %s' % (timeout, self.shutdownEvent.is_set()), file=sys.stderr)

        return not self.shutdownEvent.wait( timeout )

    def dologstart (self, d):

        return

        # Open the log file.
        try:
            pf = open( self.fname, 'a' )

            readerID = d[1]
            impinjHost = d[2]
            impinjModeStr = d[3]
            impinjSearchModeStr = d[5]
            impinjTriggerTypeStr = d[5]
            impinjT = d[6]
            impinjTimeout = d[7]


            print('ImpinjMode: %s' % (impinjModeStr), file=sys.stderr)
            print('ImpinjSearchMode: %s' % (impinjSearchModeStr), file=sys.stderr)
            print('ImpinjTriggerMode: %s' % (impinjTriggerTypeStr), file=sys.stderr)

            #                   0   1  2  3  4  5  6  7
            #pf.write( '{},,,,,,,,,,{},{},{},{},{},{},{}\n'.format(
            #                readerID,                 #0
            #                impinjHost,                  #6
            #                impinjModeStr,          #7
            #                impinjSearchModeStr,         #8
            #                impinjTriggerTypeStr,         #9
            #                impinjT, #6
            #                impinjTimeout, #7
            #                ) )

            # Close the log file.
            pf.close()
        except:
            print('dologstart: error', file=sys.stderr);


    def doMsgInternal(self, style, readerID, EPC, count, msg1, msg2, msg3, time, criteria=''):
        self.messageQ.put( (style, '%s [%d] %s (%03d) %s %s %s %s' % ( time.strftime('%H:%M:%S.%f')[:-3], readerID, EPC, count, msg1, msg2, msg3, criteria.name) ))

    def doMsg (self, readerID, EPC, count, msg1, msg2, msg3, time, criteria=None):
        self.doMsgInternal('Impinj', readerID, EPC, count, msg1, msg2, msg3, time, criteria=criteria)

    def doWarning (self, readerID, EPC, count, msg1):
        self.doMsgInternal('Impinj-Warning', readerID, EPC, count, msg1, '', '', getTimeNow())



    #           1     2         3    4          5         6         7     8           9                 10     11   12
    def dolog (self, readerID, epc, firstTime, lastTime, bestTime, RSSI, phaseAngle, dopplerFrequency, count, det, alg, err, cri):

        # Open the log file.
        pf = open( self.fname, 'a' )

        beststr = bestTime.strftime('%H%M%S.%f')
        beststr = beststr[:-1]

        laststr = lastTime.strftime('%H%M%S.%f')
        laststr = laststr[:-1]

        firststr = firstTime.strftime('%H%M%S.%f')
        firststr = firststr[:-1]

        #          1    2  3  4  5  6  7  8     9  10 11
        pf.write( '%02d,%s,%s,%s,%s,%d,%d,%4.2f,%d,%d,%s,%s,%s\n' % (
                                readerID,                 #1
                                epc,                        #2
                                firststr,                #3
                                laststr,                #4
                                beststr,                #5
                                -RSSI,                         #6
                                phaseAngle,                #7
                                dopplerFrequency,        #8
                                count,                         #9
                                det,                         #10
                                alg,                        #11
                                err,                        #11
                                cri                     #12
                                ) )
        # Close the log file.
        pf.close()


    # getTags
    # Get tags from the Impinj process has placed in the data queue. These are
    # added into the relevant TagInfo.
    #
    def getTags( self, tags):

        # ##############
        #
        # Get the next entry from the receiver, continue loop if there is no message and
        # we just timed out.
        #
        active = 0
        # XXX
        #for tagKey, tag in tags.iteritems():
        for tagKey, tag in six.iteritems(tags):
            if not tag.forwarded:
                active += 1

        try:
            d = None
            timeout = 4 if active == 0 else .15
            d = self.dataQ0.get(True, timeout)

        except Empty:
            pass

        # no tag data
        if d is None:
            #print('getTags: d is None', file=sys.stderr)
            return None

        # ##############
        #
        # special message to let us know to quit, break out of the while loop to exit
        #
        if d == 'shutdown':
            print('TagServer: received shutdown message', file=sys.stderr)
            self.keepGoing = False
            print('getTags: shutdown', file=sys.stderr)
            return None


        # ##############
        #
        # Check if this is a special message to provide us with details
        # about the reader setup
        if d[0] == 'started':
            self.dologstart(d)
            #print('getTags: started', file=sys.stderr)
            return None

        # ##############
        #
        # Must be tag read data from reader, unpack.
        #
        #1        #2   #3             #4    #5          #6         #7
        EPC = None

        #try:
        # 1       2    3              4     5           6           7               8          9
        readerID, EPC, discoveryTime, RSSI, phaseAngle, impinjRSSI, impinjDoppler, antennaID, sequence = d
        #except:
        #        print('Could not unpack d', file=sys.stderr)
        #        return

        if EPC == None:
            print('getTags: EPC None', file=sys.stderr)
            return None

        #print('getTags: d: %s' % (str(d)), file=sys.stderr)
        #print('getTags: readerID: %s epc: %s discoveryTime: %s RSSI: %s' % (readerID, EPC, discoveryTime, RSSI), file=sys.stderr)


        readerID = readerID * 10 + antennaID

        # Ransac Solution
        #if (self.tagGroup):
        #        if RSSI is not None:
        #                self.tagGroup.add(int(readerID/10), EPC, discoveryTime, RSSI)
        #        return discoveryTime


        if impinjRSSI > 0:
            RSSI = impinjRSSI / 10
        else:
            RSSI *= 10



        # Poly Solution

        #print('\n******************************\n[%d] EPC %s!' % (readerID, EPC), file=sys.stderr)

        # ###############
        #
        # Check if in dictionary, add if it is not present.
        #
        if not EPC in tags:

            #print('[%d] EPC %s NOT IN tags! %s' % (readerID, EPC, discoveryTime.strftime('%H%M%S.%f')), file=sys.stderr)

            # Create tagzero and add to dictionary
            #
            tags[EPC] = Tag(time = discoveryTime, epc = EPC, messageQ = self.messageQ )
        #else:
        #        print('[%d] EPC %s IN tags! %s' % (readerID, EPC, discoveryTime.strftime('%H%M%S.%f')), file=sys.stderr)


        # ###############
        #
        # Get old EPC data
        tag = tags[EPC]


        # Update taginfo
        #
        if tag.add(readerID, discoveryTime, RSSI, phaseAngle):
            self.dolog(readerID, EPC, tag.firstTime, tag.lastTime, discoveryTime, RSSI, phaseAngle, impinjDoppler, tag.readCount(readerID), 0, '', '', '')

        #print('getTags: discoveryTime: %s' % (discoveryTime.strftime('%H%M%S.%f')), file=sys.stderr)
        return discoveryTime


    # doForward
    # Forward a tag read to the CrossMgr processes that are active.
    #
    def doForward(self, tagKey, bestTime):

        # forward to crossmgr processes
        #
        print('doForward: tagKey: %s bestTime: %s' % (tagKey, bestTime), file=sys.stderr)

        for i in range(3):
            if self.dataQ[i] is not None: self.dataQ[i].put ((tagKey, bestTime))


    #
#         def doRansac(self):
#                 # Ransac Solution
#
#                 reads, strays = self.tagGroup.getReadsStrays(method=ProcessingMethod, antennaChoice=AntennaChoice)
#
#                 print('doReview: START tagGroup reads[%d]: %s strays[%d]: %s' % (len(reads), reads, len(strays), strays), file=sys.stderr)
#
#                 for tagID, discoveryTime, sampleSize, antennaID in reads:
#                         print('doreview[%s:%s]: ransac solution: %s sampleSize: %d' % (antennaID, tagID, discoveryTime, sampleSize), file=sys.stderr)
#
#                         # XXX need bestEst firstTime lastTime
#                         zdate = datetime.datetime.utcfromtimestamp(0)
#                         self.dolog(antennaID*10, tagID,  zdate, zdate, discoveryTime, 0, 0, 0, sampleSize, 0, Algorithm.Ransac, 'QR')
#
#                         self.doForward(tagID, discoveryTime)
#                         self.doMsg(antennaID*10+1, tagID, sampleSize, 'FORWARDED', Algorithm.Ransac.name, '', discoveryTime)
#
#                 print('doRansac: FINISH', file=sys.stderr)


    def doReview(self, oldtags, curTime, statsHistory, readersHistory, msg):

        lastUpdateTime = curTime

        print('doReview[%s]: %s' % (msg, lastUpdateTime), file=sys.stderr)


        # Poly Solution

        # Create a new tag queue for aging tags, we are going to iterate across
        # the OldTags list and either forward or save in NewTags for more aging.
        #
        newtags = dict()
        strays = []

        timeLimit = .5

        count = 1

        # Iterate across the tags,
        # First, we review current tags and determine which can be forwarded, removed, or just kept.
        # Second, get the best estimated time for tags being forwarded.
        #
        # XXX
        #for tagKey, tag in oldtags.iteritems():
        for tagKey, tag in six.iteritems(oldtags):

            if not self.checkKeepGoing():
                break

            elapsed = (curTime - tag.lastTime).total_seconds()
            print('doReview[%s %d %s] %s - %s = %5.2f %s' % (msg, count, tagKey, tag.lastTime.strftime('%H%M%S.%f'), curTime.strftime('%H%M%S.%f'), elapsed, msg), file=sys.stderr)
            count += 1

            # stray tags have the first read forwarded
            #
            if tag.stray and not tag.forwarded:
                self.doForward(tagKey, tag.firstTime)
                tag.forwarded = True
                print('doReview[%s %d %s] FORWARDED: %6.2f < 1' % (msg, count, tagKey, elapsed), file=sys.stderr)
                self.dolog(0, tagKey, tag.firstTime, tag.lastTime, tag.firstTime, 0, 0, 0, tag.count, 0, 'Stray', '', '')
                #self.doWarning(0, tagKey, tag.count, 'FORWARDED')
                newtags[tagKey] = tag
                continue

            # Check if missing tag is still missing, can we get rid of it?
            if tag.missing:
                if tag.checkIfStillMissing():
                    newtags[tagKey] = tag
                    continue
                print('doReview[%s %d %s] DROPPING MISSING TAG' % (msg, count, tagKey), file=sys.stderr)
                continue

            # Check if gone flag is missing,
            #
            if tag.gone:
                tag.checkIfMissing()
                newtags[tagKey] = tag
                continue

            # Check if stray tag is gone,
            #
            if tag.stray:
                #strays.append((tagKey, tag.firstTime))
                strays.append([tagKey, tag.firstTime])
                tag.checkIfGone()
                newtags[tagKey] = tag
                continue


            # Keep tags around for a short period, then drop, this helps keep additional reads from
            # a person stopping just past the line.
            #
            if tag.forwarded:
                if elapsed < 2*timeLimit:
                    newtags[tagKey] = tag
                    continue
                print('doReview[%s %d %s] DROPPING elapsed: %6.2f < %3.1f' % (msg, count, tagKey, elapsed, timeLimit), file=sys.stderr)
                continue


            # Active tags past here, if last read less than timeLimit, just retain
            #
            if elapsed < timeLimit:
                print('doReview[%s %d %s] WAITING elapsed: %6.2f < %3.1f' % (msg, count, tagKey, elapsed, timeLimit), file=sys.stderr)
                newtags[tagKey] = tag
                continue


            # Active tag, have not seen within timeLimit, forward best estimated time
            #
            if tag.count < 10 and elapsed < 3:
                print('doReview[%s %d %s] WAITING elapsed: %6.2f > %3.1f but COUNT is < 10' % (msg, count, tagKey, elapsed, timeLimit), file=sys.stderr)
                newtags[tagKey] = tag
                continue


            # Tag needs to be forwarded, get the best estimated time from all readers/antenna pairs
            #
            print('doReview[%s %d %s] elapsed: %6.2f > %3.1f count: %d FORWARD' % (msg, count, tagKey, elapsed, timeLimit, tag.count), file=sys.stderr)

            estimateDefault = Algorithm.maxDB
            bestEst = BestEst(tagKey, self.readerPriority, estimateDefault )

            # iterate across accumulated reads per reader for this specific tag
            # XXX
            #for readerKey, taginfo in tag.tagdict.iteritems():
            for readerKey, taginfo in six.iteritems(tag.tagdict):

                if not self.checkKeepGoing():
                    break

                # get the best estimate of the tag crossing the zero line in FOV
                #
                print('doReview[%s %d %s-%s]: START' % (msg, count, readerKey, tagKey), file=sys.stderr)
                e = Estimate(readerKey, tagKey, taginfo, estimateDefault)
                print('doReview[%s %d %s-%s]: Estimate: %s' % (msg, count, readerKey, tagKey, e), file=sys.stderr)

                #t = taginfo.addRefTime(e.x)

                # save if better
                #
                bestEst.add(readerKey, e)

                # Log
                #          2          3       4                  5                 6  7  8  9  10          11           12
                self.dolog(readerKey, tagKey, e.firstTime, e.lastTime, e.t, e.y, 0, 0, e.count, e.det*100, e.alg, e.err, '')
                print('doReview[%s %d %s:%s] x: %s y: %d det: %5.2f count: %d %s %s' % (msg, count, tagKey, readerKey, e.t.strftime('%H%M%S.%f'), e.y, e.det, e.count, e.alg, e.err), file=sys.stderr)


            e, criteria, readerHist = bestEst.getBest()

            print('doReview[%s %d %s] best estimate:\n%s' % (msg, count, tagKey, e), file=sys.stderr)
            print('doReview[%s %d %s] readerHist:\n%s' % (msg, count, tagKey, readerHist), file=sys.stderr)

            # Forward and log
            print('doReview[%s %d %s]: FORWARD[%s:%s] x: %s y: %d det: %5.2f count: %d %s %s' % (msg, count, tagKey, 
                    e.reader, e.epc, e.t.strftime('%H%M%S.%f'), e.y, e.det, e.count, e.alg, e.err), file=sys.stderr,)

            tag.forwarded = True
            self.doForward(tagKey, e.t)
            self.doMsg(e.reader, e.epc, e.count, ' ', e.alg.name, e.err, e.t, criteria)

            # XXX need bestEst firstTime lastTime
            self.dolog(e.reader, e.epc, tag.firstTime, tag.lastTime, e.t, e.y, 0, 0, e.count, e.det*100, e.alg, 'Sel', criteria.name)

            newtags[tagKey] = tag

            statsHistory.append(e)
            readersHistory.append(readerHist)


        print('doReview[%s]: strays: %s SENDING' % (msg, strays), file=sys.stderr)
        self.statQ.put(('strays', strays))

        stats = statsHistory.getStats()
        readerStats = readersHistory.getReaderStats()
        print('doReview[%s]: readerStats: %s ' % (msg, readerStats), file=sys.stderr)
        if not stats is None:
            self.statQ.put(('stats', stats))
        if not readerStats is None:
            self.statQ.put(('readerStats', readerStats))
        return newtags


#def queueRansacServer (self):
#
#        #print('queueRansacServer:', file=sys.stderr)
#        # Loop until shutdown
#        #
#        lastUpdateTime = getTimeNow()
#        lastDataTime = getTimeNow()
#        lastEmptyTime = getTimeNow()
#
#        statsHistory = StatsHistory()
#        readersHistory = ReaderHistory()
#
#        while self.checkKeepGoing():
#
#
#                # XXX Need to know when there are no more tags!
#                #
#                lastTime = self.getTagsRansac()
#
#                if lastTime == None:
#                        #print('queueRansacServer: lastTime None', file=sys.stderr)
#                        lastEmptyTime = getTimeNow()
#                        elapsed = (lastEmptyTime - lastUpdateTime).total_seconds()
#                else:
#                        lastDataTime = lastTime
#                        elapsed = (lastDataTime - lastUpdateTime).total_seconds()
#
#                #
#                # Review and Update the existing tags about every 500ms
#                #
#                #print('queueRansacServer: elapsed: %s lastDataTime: %s lastUpdateTime: %s lastEmptyTime: %s' % (elapsed, lastDataTime, lastUpdateTime, lastEmptyTime), file=sys.stderr)
#                if elapsed  > .3:
#                        self.doRansac()
#                        lastUpdateTime = lastDataTime
#                        continue



    def queueRunServer( self ):


        #print('queueRunServer:', file=sys.stderr)

        # set lastUpdatetime
        lastUpdateTime = getTimeNow()
        lastDataTime = getTimeNow()
        lastEmptyTime = getTimeNow()

        self.messageQ.put( ('Impinj2Queue', 'Impin2Queue Start queuing data '))

        # OldTags keeps the current set of aging tag reads
        #
        OldTags = dict();

        statsHistory = StatsHistory()
        readersHistory = ReaderHistory()

        # Loop until shutdown
        #
        while self.checkKeepGoing():

            #
            # Get next RO Report or timeout, get lastTime a tag was received to pass to doReview.
            # N.B. we need to use lastTime instead of curTime for doReview as a heavily loaded system
            # can have this process get behind in clocktime. Generally this is not a problem, but it
            # means that doReview cannot use clocktime which would not be the correct reference for
            # tags waiting to be forwarded.
            #

            # XXX Need to know when there are no more tags!
            #
            #print('queueRunServer: getTags', file=sys.stderr)
            lastTime = self.getTags(OldTags)

            if lastTime == None:
                lastEmptyTime = getTimeNow()
            else:
                lastDataTime = lastTime

            #
            # Review and Update the existing tags about every 500ms
            #
            elapsed = (lastDataTime - lastUpdateTime).total_seconds()
            if elapsed  > .3 and len(OldTags) > 0:
                OldTags = self.doReview(OldTags, lastDataTime, statsHistory, readersHistory, 'dataTime')
                lastUpdateTime = lastDataTime
                #print('queueRunServer: dateTime continue', file=sys.stderr)
                continue

            elapsed = (lastEmptyTime - lastUpdateTime).total_seconds()
            if elapsed  > 2 and len(OldTags) > 0:
                OldTags = self.doReview(OldTags, getTimeNow(), statsHistory, readersHistory, 'curTime1')
                lastUpdateTime = getTimeNow()
                #print('queueRunServer: curTime1 continue', file=sys.stderr)
                continue

            if elapsed > 5 and (len(statsHistory.history) or len(readersHistory.history)):
                OldTags = self.doReview(OldTags, getTimeNow(), statsHistory, readersHistory, 'curTime2')
                lastUpdateTime = getTimeNow()
                #print('queueRunServer: curTime2 continue', file=sys.stderr)
                continue

            #print('queueRunServer: continue', file=sys.stderr)






def TagServer( statQ, dataQ0, dataQ, messageQ, shutdownEvent, repeatSeconds, readerPriority ):

    impinj2Queue = Impinj2Queue( statQ, dataQ0, dataQ, messageQ, shutdownEvent, repeatSeconds, readerPriority )
    #if (impinj2Queue.tagGroup):
    #impinj2Queue.queueRansacServer()
    #else:
    impinj2Queue.queueRunServer()
