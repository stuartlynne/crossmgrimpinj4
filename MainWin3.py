import sys
import threading
import socket
import atexit
import time
from roundbutton import RoundButton
import Utils
from Queue import Empty
from threading import Thread as Process
from Queue import Queue
import Impinj3
import Impinj2JChip2

from Impinj3 import ImpinjServer
from Queue2JChip import CrossMgrServer
from Impinj2JChip2 import TagServer

from AutoDetect import AutoDetect
import os
import datetime
getTimeNow = datetime.datetime.now
HOME_DIR = os.path.expanduser("~")


import wx
import wx.lib.masked                    as masked
import wx.lib.intctrl                   as intctrl
import sys
import os
import re
import datetime

from Version import AppVerName

CrossMgrPort1 = 53135
CrossMgrPort2 = 53135
CrossMgrPort3 = 53136
ImpinjHostNamePrefix = 'SpeedwayR-'
ImpinjHostNameSuffix = '.local'
ImpinjInboundPort = 5084
#ImpinjInboundPort = 50840

clipboard_xpm = [
"16 15 23 1",
"+ c #769CDA",
": c #DCE6F6",
"X c #3365B7",
"* c #FFFFFF",
"o c #9AB6E4",
"< c #EAF0FA",
"# c #B1C7EB",
". c #6992D7",
"3 c #F7F9FD",
", c #F0F5FC",
"$ c #A8C0E8",
"  c None",
"- c #FDFEFF",
"& c #C4D5F0",
"1 c #E2EAF8",
"O c #89A9DF",
"= c #D2DFF4",
"4 c #FAFCFE",
"2 c #F5F8FD",
"; c #DFE8F7",
"% c #B8CCEC",
"> c #E5EDF9",
"@ c #648FD6",
" .....XX        ",
" .oO+@X#X       ",
" .$oO+X##X      ",
" .%$o........   ",
" .&%$.*=&#o.-.  ",
" .=&%.*;=&#.--. ",
" .:=&.*>;=&.... ",
" .>:=.*,>;=&#o. ",
" .<1:.*2,>:=&#. ",
" .2<1.*32,>:=&. ",
" .32<.*432,>:=. ",
" .32<.*-432,>:. ",
" .....**-432,>. ",
"     .***-432,. ",
"     .......... "
]


class MessageManager( object ):
    MessagesMax = 400       # Maximum number of messages before we start throwing some away.

    def __init__( self, messageList ):
        self.messageList = messageList
        self.messageList.Bind( wx.EVT_RIGHT_DOWN, self.skip )
        self.messageList.SetDoubleBuffered( True )
        self.clear()

    def skip(self, evt):
        return

    def write( self, message ):
        if len(self.messages) >= self.MessagesMax:
            self.messages = self.messages[int(self.MessagesMax):]
            s = '\n'.join( self.messages )
            self.messageList.ChangeValue( s + '\n' )
        self.messages.append( message )
        self.messageList.AppendText( message + '\n' )

    def clear( self ):
        self.messages = []
        self.messageList.ChangeValue( '' )
        self.messageList.SetInsertionPointEnd()

def setFont( font, w ):
    w.SetFont( font )
    return w

class AdvancedSetup( wx.Dialog ):
    def __init__( self, parent, id = wx.ID_ANY ):

        wx.Dialog.__init__( self, parent, id, "Advanced Setup",
                                        style=wx.DEFAULT_DIALOG_STYLE|wx.TAB_TRAVERSAL )

        #'''
        #Impinj3.ImpinjRFControlMode            = 1             # Impinj RF Control Mode
        #Impinj3.ImpinjTriggerType              = 3             # Impinj Tag Trigger Type
        #Impinj3.ImpinjT                                = 6             # Impinj T
        #Impinj3.ImpinjTimeout                  = 7             # Impinj Timeout
        #Impinj3.ConnectionTimeoutSeconds       = 8             # Interval for connection timeout
        #Impinj3.KeepaliveSeconds               = 9             # Interval to request a Keepalive message
        #Impinj3.RepeatSeconds                  = 10            # Interval in which a tag is considered a repeat read.
        #'''

        bs = wx.GridBagSizer(vgap=5, hgap=5)

        border = 8
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Advanced Reader Options:'), pos = (0,0), span=(1, 2), border = border, flag=wx.ALL )

        row = 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Impinj RFControl Mode'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.ImpinjRFControlMode = intctrl.IntCtrl( self, wx.ID_ANY, min=-1, max=9999, limited = True,
                value = Impinj3.ImpinjRFControlMode, size=(32,-1) )
        bs.Add( self.ImpinjRFControlMode, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Impinj RFControl: 0: max throughput; 4: MaxMiller; 1: hybrid, 2: dense reader M=4 hispd; 3: dense reader M=8 lospd'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Impinj Tag Trigger Type'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.ImpinjTriggerType = intctrl.IntCtrl( self, wx.ID_ANY, min=0, max=2, limited = True,
                value = Impinj3.ImpinjTriggerType, size=(32,-1) )
        bs.Add( self.ImpinjTriggerType, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Impinj Tag Trigger Type: 0: N tags or Timeout; 1: No new for T ms; 2: N attempts or Timeout'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'T'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.ImpinjT = intctrl.IntCtrl( self, wx.ID_ANY, min=5, max=1000, limited = True,
                value = Impinj3.ImpinjT, size=(32,-1) )
        bs.Add( self.ImpinjT, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Impinj T: Idle time between tag responses in ms, ignored unless TriggerType !=1 (>4'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Timeout'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.ImpinjTimeout = intctrl.IntCtrl( self, wx.ID_ANY, min=10, max=1000, limited = True,
                value = Impinj3.ImpinjTimeout, size=(32,-1) )
        bs.Add( self.ImpinjTimeout, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Impinj Timeout: Trigger timeout in ms (>9ms)'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'TransmitPower'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.TransmitPower= intctrl.IntCtrl( self, wx.ID_ANY, min=0, max=3250, limited = True,
                value = Impinj3.TransmitPower, size=(32,-1) )
        bs.Add( self.TransmitPower, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Transmit PowerLevel Transmit Power 1000-3250, default is zero, which is maximum power'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Connection Timeout Seconds'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.ConnectionTimeoutSeconds = intctrl.IntCtrl( self, wx.ID_ANY, min=1, max=60, limited = True,
                value = Impinj3.ConnectionTimeoutSeconds, size=(32,-1) )
        bs.Add( self.ConnectionTimeoutSeconds, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'maximum time to wait for a reader response'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Keepalive Seconds'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.KeepaliveSeconds = intctrl.IntCtrl( self, wx.ID_ANY, min=1, max=60, limited = True,
                value = Impinj3.KeepaliveSeconds, size=(32,-1) )
        bs.Add( self.KeepaliveSeconds, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'frequency of "heartbeat" messages indicating the reader is still connected'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Repeat Seconds'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.RepeatSeconds = intctrl.IntCtrl( self, wx.ID_ANY, min=1, max=120, limited = True,
                value = Impinj3.RepeatSeconds, size=(32,-1) )
        bs.Add( self.RepeatSeconds, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'interval in which multiple tag reads are considered "repeats" and not reported'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        self.restoreDefaultButton = wx.Button( self, wx.ID_ANY, 'Restore Defaults' )
        self.restoreDefaultButton.Bind( wx.EVT_BUTTON, self.onRestoreDefault )
        bs.Add( self.restoreDefaultButton, pos=(row, 0), span=(1,3), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Reminder: Press "Reset" for these changes to take effect.'),
                        pos=(row, 0), span=(1,3), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_RIGHT )

        self.okBtn = wx.Button( self, wx.ID_OK )
        self.Bind( wx.EVT_BUTTON, self.onOK, self.okBtn )

        self.cancelBtn = wx.Button( self, wx.ID_CANCEL )
        self.Bind( wx.EVT_BUTTON, self.onCancel, self.cancelBtn )

        row += 1
        hs = wx.BoxSizer( wx.HORIZONTAL )
        hs.Add( self.okBtn, border = border, flag=wx.ALL )
        self.okBtn.SetDefault()
        hs.Add( self.cancelBtn, border = border, flag=wx.ALL )

        bs.Add( hs, pos=(row, 0), span=(1,3), flag=wx.ALIGN_RIGHT )

        self.SetSizerAndFit(bs)
        bs.Fit( self )

        self.CentreOnParent(wx.BOTH)
        self.SetFocus()

    def onRestoreDefault( self, event ):

        self.ImpinjRFControlMode.SetValue( Impinj3.ImpinjRFControlDefault )
        self.ImpinjTriggerType.SetValue( Impinj3.ImpinjTriggerTypeDefault )
        self.ImpinjT.SetValue( Impinj3.ImpinjTDefault )
        self.ImpinjTimeout.SetValue( Impinj3.ImpinjTimeoutDefault )

        self.TransmitPower.SetValue( Impinj3.TransmitPowerDefault)
        self.ConnectionTimeoutSeconds.SetValue( Impinj3.ConnectionTimeoutSecondsDefault )
        self.KeepaliveSeconds.SetValue( Impinj3.KeepaliveSecondsDefault )
        self.RepeatSeconds.SetValue( Impinj3.RepeatSecondsDefault )

    def onOK( self, event ):
        Impinj3.ImpinjRFControlMode = self.ImpinjRFControlMode.GetValue()
        Impinj3.ImpinjTriggerType = self.ImpinjTriggerType.GetValue()
        Impinj3.ImpinjT = self.ImpinjT.GetValue()
        Impinj3.ImpinjTimeout = self.ImpinjTimeout.GetValue()

        Impinj3.TransmitPower= self.TransmitPower.GetValue()
        Impinj3.ConnectionTimeoutSeconds = self.ConnectionTimeoutSeconds.GetValue()
        Impinj3.KeepaliveSeconds = self.KeepaliveSeconds.GetValue()
        Impinj3.RepeatSeconds = self.RepeatSeconds.GetValue()
        self.EndModal( wx.ID_OK )

    def onCancel( self, event ):
        self.EndModal( wx.ID_CANCEL )

class MainWin( wx.Frame ):
    def __init__( self, parent, id = wx.ID_ANY, title='', size=(600,800) ):
        wx.Frame.__init__(self, parent, id, title, size=size)

        self.config = wx.Config(appName="CrossMgrImpinj3cfg",
                                        vendorName="SmartCyclingSolutions",
                                        style=wx.CONFIG_USE_LOCAL_FILE)

        self.SetBackgroundColour( wx.Colour(232,232,232) )

        self.LightGreen = wx.Colour(153,255,153)
        #self.LightOrange = wx.Colour(204,85,0) # burnt orange
        self.LightOrange = wx.Colour(255,179,71)
        self.LightRed = wx.Colour(255,153,153)

        font = self.GetFont()
        bigFont = wx.Font( font.GetPointSize() * 1.5, font.GetFamily(), font.GetStyle(), wx.FONTWEIGHT_BOLD )
        italicFont = wx.Font( bigFont.GetPointSize()*2.2, bigFont.GetFamily(), wx.FONTSTYLE_ITALIC, bigFont.GetWeight() )

        self.vbs = wx.BoxSizer( wx.VERTICAL )

        bs = wx.BoxSizer( wx.HORIZONTAL )

        self.reset = RoundButton(self, wx.ID_ANY, 'Reset', size=(80, 80))
        self.reset.SetBackgroundColour( wx.WHITE )
        self.reset.SetForegroundColour( wx.Colour(0,128,128) )
        self.reset.SetFontToFitLabel()  # Use the button's default font, but change the font size to fit the label.
        self.reset.Bind( wx.EVT_BUTTON, self.doReset )
        self.reset.Refresh()
        bs.Add( self.reset, border = 8, flag=wx.LEFT|wx.ALIGN_CENTER_VERTICAL )
        bs.Add( setFont(italicFont,wx.StaticText(self, wx.ID_ANY, 'CrossMgrImpinj3')), border = 8, flag=wx.LEFT|wx.ALIGN_CENTER_VERTICAL )
        bs.AddStretchSpacer()
        bitmap = wx.Bitmap( clipboard_xpm )
        self.copyToClipboard = wx.BitmapButton( self, wx.ID_ANY, bitmap )
        self.copyToClipboard.SetToolTip(wx.ToolTip('Copy Configuration and Logs to Clipboard...'))
        self.copyToClipboard.Bind( wx.EVT_BUTTON, self.doCopyToClipboard )
        bs.Add( self.copyToClipboard, border = 32, flag = wx.LEFT|wx.ALIGN_CENTER_VERTICAL )
        self.tStart = datetime.datetime.now()

        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Last Reset: %s' % self.tStart.strftime('%H:%M:%S')), border = 10, flag=wx.LEFT|wx.ALIGN_CENTER_VERTICAL )

        self.runningTime = wx.StaticText(self, wx.ID_ANY, '00:00:00' )
        bs.Add( self.runningTime, border = 20, flag=wx.LEFT|wx.ALIGN_CENTER_VERTICAL )

        bs.Add( wx.StaticText(self, wx.ID_ANY, ' / '), flag=wx.ALIGN_CENTER_VERTICAL )

        self.time = wx.StaticText(self, wx.ID_ANY, '00:00:00' )
        bs.Add( self.time, flag=wx.ALIGN_CENTER_VERTICAL )

        self.vbs.Add( bs, flag=wx.ALL|wx.EXPAND, border = 4 )

        fgs = wx.FlexGridSizer( rows = 2, cols = 2, vgap = 4, hgap = 4 )
        fgs.AddGrowableRow( 1 )
        fgs.AddGrowableCol( 0 )
        fgs.AddGrowableCol( 1 )
        fgs.SetFlexibleDirection( wx.BOTH )

        self.vbs.Add( fgs, flag=wx.EXPAND, proportion=5 )

        #------------------------------------------------------------------------------------------------
        # Impinj configuration.
        #
        self.gbs = wx.GridBagSizer( 7, 4 )
        fgs.Add( self.gbs, flag=wx.EXPAND|wx.ALL, border = 4 )

        iRow = 0
        hb = wx.BoxSizer( wx.HORIZONTAL )
        hb.Add( setFont(bigFont,wx.StaticText(self, wx.ID_ANY, 'Impinj Configuration:')), flag=wx.ALIGN_CENTER_VERTICAL )
        self.autoDetectButton = wx.Button(self, wx.ID_ANY, 'Auto Detect')
        self.autoDetectButton.Bind( wx.EVT_BUTTON, self.doAutoDetect )
        hb.Add( self.autoDetectButton, flag=wx.LEFT, border = 6 )

        self.advancedButton = wx.Button(self, wx.ID_ANY, 'Advanced...' )
        self.advancedButton.Bind( wx.EVT_BUTTON, self.doAdvanced )
        hb.Add( self.advancedButton, flag=wx.LEFT, border = 6 )

        self.gbs.Add( hb, pos=(iRow,0), span=(1,2), flag=wx.ALIGN_LEFT )


        # #############################################################
        # Antennas row
        iRow += 1

        gs = wx.GridSizer( rows=0, cols=5, vgap=2, hgap=2 )
        self.antennas1 = []
        gs.Add( wx.StaticText(self, wx.ID_ANY, ''), flag=wx.ALIGN_CENTER )
        for i in xrange(4):
            gs.Add( wx.StaticText(self, wx.ID_ANY, '%d' % (i+1)), flag=wx.ALIGN_CENTER )
        gs.Add( wx.StaticText(self, wx.ID_ANY, 'ANT: '), flag=wx.ALIGN_CENTER_VERTICAL )
        for i in xrange(4):
            cb = wx.CheckBox( self, wx.ID_ANY, '')
            if i < 2:
                cb.SetValue( True )
            cb.Bind( wx.EVT_CHECKBOX, lambda x: self.getAntennaStr1() )
            gs.Add( cb, flag=wx.ALIGN_CENTER )
            self.antennas1.append( cb )

        self.gbs.Add( gs, pos=(iRow,0), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )


        gs = wx.GridSizer( rows=0, cols=5, vgap=2, hgap=2 )
        self.antennas2 = []
        gs.Add( wx.StaticText(self, wx.ID_ANY, ''), flag=wx.ALIGN_CENTER )
        for i in xrange(4):
            gs.Add( wx.StaticText(self, wx.ID_ANY, '%d' % (i+1)), flag=wx.ALIGN_CENTER )
        gs.Add( wx.StaticText(self, wx.ID_ANY, 'ANT: '), flag=wx.ALIGN_CENTER_VERTICAL )
        for i in xrange(4):
            cb = wx.CheckBox( self, wx.ID_ANY, '')
            if i < 2:
                cb.SetValue( True )
            cb.Bind( wx.EVT_CHECKBOX, lambda x: self.getAntennaStr2() )
            gs.Add( cb, flag=wx.ALIGN_CENTER )
            self.antennas2.append( cb )

        self.gbs.Add( gs, pos=(iRow,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )


        gs = wx.GridSizer( rows=0, cols=5, vgap=2, hgap=2 )
        self.antennas3 = []
        gs.Add( wx.StaticText(self, wx.ID_ANY, ''), flag=wx.ALIGN_CENTER )
        for i in xrange(4):
            gs.Add( wx.StaticText(self, wx.ID_ANY, '%d' % (i+1)), flag=wx.ALIGN_CENTER )
        gs.Add( wx.StaticText(self, wx.ID_ANY, 'ANT: '), flag=wx.ALIGN_CENTER_VERTICAL )
        for i in xrange(4):
            cb = wx.CheckBox( self, wx.ID_ANY, '')
            if i < 2:
                cb.SetValue( True )
            cb.Bind( wx.EVT_CHECKBOX, lambda x: self.getAntennaStr3() )
            gs.Add( cb, flag=wx.ALIGN_CENTER )
            self.antennas3.append( cb )

        self.gbs.Add( gs, pos=(iRow,2), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )


        # #############################################################
        # Reader one column
        iRow += 1
        self.useHostName1 = wx.RadioButton( self, label='Host Name:', style=wx.RB_GROUP )
        hb = wx.BoxSizer( wx.HORIZONTAL )
        hb.Add( self.useHostName1, flag=wx.ALIGN_CENTER_VERTICAL  )
        hb.Add( wx.StaticText(self, wx.ID_ANY, ImpinjHostNamePrefix), flag=wx.ALIGN_CENTER_VERTICAL )
        self.impinjHostName1 = masked.TextCtrl( self, wx.ID_ANY,
                                                mask         = 'NN-NN-NN',
                                                defaultValue = '00-00-00',
                                                useFixedWidthFont = True,
                                                size=(80, -1),
                                        )
        hb.Add( self.impinjHostName1 )
        self.gbs.Add( hb, pos=(iRow+2,0), span=(1,1), flag=wx.ALIGN_LEFT )

        self.useStaticAddress1 = wx.RadioButton( self, label='Address:' )
        hb = wx.BoxSizer( wx.HORIZONTAL )
        hb.Add( self.useStaticAddress1, flag=wx.ALIGN_CENTER_VERTICAL  )
        self.impinjHost1 = masked.IpAddrCtrl( self, wx.ID_ANY, style = wx.TE_PROCESS_TAB )
        hb.Add( self.impinjHost1 )
        hb.Add( wx.StaticText(self, wx.ID_ANY, ' : ' + '{}'.format(ImpinjInboundPort)), flag=wx.ALIGN_CENTER_VERTICAL )

        self.gbs.Add( hb, pos=(iRow+1,0), span=(1,1), flag=wx.ALIGN_LEFT )

        self.impinjDisable1 = wx.RadioButton( self, label='Disable:' )
        self.gbs.Add( self.impinjDisable1, pos=(iRow+0,0), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL , border=80)


        # #############################################################
        # Reader two column

        self.useHostName2 = wx.RadioButton( self, label='Host Name:', style=wx.RB_GROUP )
        hb = wx.BoxSizer( wx.HORIZONTAL )
        hb.Add( self.useHostName2, flag=wx.ALIGN_CENTER_VERTICAL  )
        hb.Add( wx.StaticText(self, wx.ID_ANY, ImpinjHostNamePrefix), flag=wx.ALIGN_CENTER_VERTICAL )
        self.impinjHostName2 = masked.TextCtrl( self, wx.ID_ANY,
                                                mask         = 'NN-NN-NN',
                                                defaultValue = '00-00-00',
                                                useFixedWidthFont = True,
                                                size=(80, -1),
                                        )
        hb.Add( self.impinjHostName2 )
        self.gbs.Add( hb, pos=(iRow+2,1), span=(1,1), flag=wx.ALIGN_LEFT )

        self.useStaticAddress2 = wx.RadioButton( self, label='' )
        hb = wx.BoxSizer( wx.HORIZONTAL )
        hb.Add( self.useStaticAddress2, flag=wx.ALIGN_CENTER_VERTICAL  )
        self.impinjHost2 = masked.IpAddrCtrl( self, wx.ID_ANY, style = wx.TE_PROCESS_TAB )
        hb.Add( self.impinjHost2 )
        hb.Add( wx.StaticText(self, wx.ID_ANY, ' : ' + '{}'.format(ImpinjInboundPort)), flag=wx.ALIGN_CENTER_VERTICAL )

        self.gbs.Add( hb, pos=(iRow+1,1), span=(1,1), flag=wx.ALIGN_LEFT )

        self.impinjDisable2 = wx.RadioButton( self, label='Disable:' )
        self.gbs.Add( self.impinjDisable2, pos=(iRow+0,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )


        # #############################################################
        # Reader three column
        self.useHostName3 = wx.RadioButton( self, label='', style=wx.RB_GROUP )
        hb = wx.BoxSizer( wx.HORIZONTAL )
        hb.Add( self.useHostName3, flag=wx.ALIGN_CENTER_VERTICAL  )
        hb.Add( wx.StaticText(self, wx.ID_ANY, ImpinjHostNamePrefix), flag=wx.ALIGN_CENTER_VERTICAL )
        self.impinjHostName3 = masked.TextCtrl( self, wx.ID_ANY,
                                                mask         = 'NN-NN-NN',
                                                defaultValue = '00-00-00',
                                                useFixedWidthFont = True,
                                                size=(80, -1),
                                        )
        hb.Add( self.impinjHostName3 )
        self.gbs.Add( hb, pos=(iRow+2,2), span=(1,1), flag=wx.ALIGN_LEFT )

        self.useStaticAddress3 = wx.RadioButton( self, label='' )
        hb = wx.BoxSizer( wx.HORIZONTAL )
        hb.Add( self.useStaticAddress3, flag=wx.ALIGN_CENTER_VERTICAL  )
        self.impinjHost3 = masked.IpAddrCtrl( self, wx.ID_ANY, style = wx.TE_PROCESS_TAB )
        hb.Add( self.impinjHost3 )
        hb.Add( wx.StaticText(self, wx.ID_ANY, ' : ' + '{}'.format(ImpinjInboundPort)), flag=wx.ALIGN_CENTER_VERTICAL )

        self.gbs.Add( hb, pos=(iRow+1,2), span=(1,1), flag=wx.ALIGN_LEFT )

        self.impinjDisable3 = wx.RadioButton( self, label='Disable:' )
        self.gbs.Add( self.impinjDisable3, pos=(iRow+0,2), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )


        # #############################################################

        #self.impinjCounterLabel1 = wx.StaticText(self, wx.ID_ANY, '--' )
        #self.gbs.Add( self.impinjCounterLabel1, pos=(5,0), span=(1,1),  flag=wx.ALIGN_CENTER )

        #self.impinjCounterLabel2 = wx.StaticText(self, wx.ID_ANY, '--' )
        #self.gbs.Add( self.impinjCounterLabel2, pos=(5,1), span=(1,1),  flag=wx.ALIGN_CENTER )

        #self.impinjCounterLabel3 = wx.StaticText(self, wx.ID_ANY, '--' )
        #self.gbs.Add( self.impinjCounterLabel3, pos=(5,2), span=(1,1),  flag=wx.ALIGN_CENTER )

        #self.gbs.Add( setFont(bigFont,wx.StaticText(self, wx.ID_ANY, '--' )), pos=(5,0), span=(1,1), flag=wx.ALIGN_CENTER , border=80)

        #self.gbs.Add( setFont(bigFont,wx.StaticText(self, wx.ID_ANY, '--' )), pos=(5,0), span=(1,1), flag=wx.ALIGN_CENTER , border=80)
        #self.gbs.Add( setFont(bigFont,wx.StaticText(self, wx.ID_ANY, '--' )), pos=(5,1), span=(1,1), flag=wx.ALIGN_CENTER , border=80)
        #self.gbs.Add( setFont(bigFont,wx.StaticText(self, wx.ID_ANY, '--' )), pos=(5,2), span=(1,1), flag=wx.ALIGN_CENTER , border=80)

        # set default values
        self.useHostName1.SetValue( False )
        self.useHostName2.SetValue( False )
        self.useHostName3.SetValue( False )

        self.useStaticAddress1.SetValue( False )
        self.useStaticAddress2.SetValue( False )
        self.useStaticAddress3.SetValue( False )

        self.impinjDisable1.SetValue( False )
        self.impinjDisable2.SetValue( False )
        self.impinjDisable3.SetValue( False )


        #------------------------------------------------------------------------------------------------
        # CrossMgr configuration.
        #
        self.gbs = wx.GridBagSizer( 9, 5 )
        fgs.Add( self.gbs, flag=wx.EXPAND|wx.ALL, border = 4 )

        #self.gbs.Add( setFont(bigFont,wx.StaticText(self, wx.ID_ANY, 'CrossMgr Configuration:')), pos=(0,0), span=(1,2), flag=wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, 'CrossMgr IP:'), pos=(0,0), span=(1,1), flag=wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, 'CrossMgr IP:'), pos=(1,0), span=(1,1), flag=wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, 'CrossMgr IP:'), pos=(2,0), span=(1,1), flag=wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )

        # Backup file name

        #self.gbs.Add( wx.StaticText(self, wx.ID_ANY, 'Backup File:'), pos=(3,0), span=(1,1), flag=wx.ALIGN_RIGHT )
        #self.backupFile = wx.StaticText( self, wx.ID_ANY, '' )
        #self.gbs.Add( self.backupFile, pos=(3,1), span=(1,1), flag=wx.ALIGN_LEFT )

        hb = wx.BoxSizer( wx.HORIZONTAL )
        self.crossMgrHost1 = masked.IpAddrCtrl( self, wx.ID_ANY, style = wx.TE_PROCESS_TAB )
        hb.Add( self.crossMgrHost1, flag=wx.ALIGN_LEFT )
        hb.Add( wx.StaticText( self, wx.ID_ANY, ' : 53135' ), flag=wx.ALIGN_CENTER_VERTICAL )
        self.gbs.Add( hb, pos=(0,1), span=(1,1), flag=wx.ALIGN_LEFT )

        hb = wx.BoxSizer( wx.HORIZONTAL )
        self.crossMgrHost2 = masked.IpAddrCtrl( self, wx.ID_ANY, style = wx.TE_PROCESS_TAB )
        hb.Add( self.crossMgrHost2, flag=wx.ALIGN_LEFT )
        hb.Add( wx.StaticText( self, wx.ID_ANY, ' : 53135' ), flag=wx.ALIGN_CENTER_VERTICAL )
        self.gbs.Add( hb, pos=(1,1), span=(1,1), flag=wx.ALIGN_LEFT )

        hb = wx.BoxSizer( wx.HORIZONTAL )
        self.crossMgrHost3 = masked.IpAddrCtrl( self, wx.ID_ANY, style = wx.TE_PROCESS_TAB )
        hb.Add( self.crossMgrHost3, flag=wx.ALIGN_LEFT )
        hb.Add( wx.StaticText( self, wx.ID_ANY, ' : 53136' ), flag=wx.ALIGN_CENTER_VERTICAL )
        self.gbs.Add( hb, pos=(2,1), span=(1,1), flag=wx.ALIGN_LEFT )


        self.hostEnable1 = wx.cb = wx.CheckBox( self, wx.ID_ANY, '')
        self.hostEnable2 = wx.cb = wx.CheckBox( self, wx.ID_ANY, '')
        self.hostEnable3 = wx.cb = wx.CheckBox( self, wx.ID_ANY, '')

        self.gbs.Add( self.hostEnable1, pos=(0,2), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )
        self.gbs.Add( self.hostEnable2, pos=(1,2), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )
        self.gbs.Add( self.hostEnable3, pos=(2,2), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )



        # reader status area
        # width , height
        cbs = wx.GridBagSizer( 3, 12 )
        self.gbs.Add( cbs, pos=(3,0), span=(1,7), flag=wx.EXPAND|wx.ALL)


        # Counters
        cbs.Add( wx.StaticText(self, wx.ID_ANY, 'Counters:' ), pos=(0,0), flag=wx.ALIGN_CENTER )

        self.impinjCounterDisplay1 = wx.StaticText(self, wx.ID_ANY, '     -0-       ' )
        cbs.Add( self.impinjCounterDisplay1, pos=(0,1), flag=wx.ALIGN_CENTER )
        self.impinjCounterDisplay2 = wx.StaticText(self, wx.ID_ANY, '     -0-'        )
        cbs.Add( self.impinjCounterDisplay2, pos=(0,4), flag=wx.ALIGN_CENTER )
        self.impinjCounterDisplay3 = wx.StaticText(self, wx.ID_ANY, '     -0-'        )
        cbs.Add( self.impinjCounterDisplay3, pos=(0,7), flag=wx.ALIGN_CENTER )


        # Rate
        cbs.Add( wx.StaticText(self, wx.ID_ANY, 'Rate:' ), pos=(1,0), flag=wx.ALIGN_CENTER )

        self.impinjRateDisplay1 = wx.StaticText(self, wx.ID_ANY, '     -0-       ' )
        cbs.Add( self.impinjRateDisplay1, pos=(1,1), flag=wx.ALIGN_CENTER )
        self.impinjRateDisplay2 = wx.StaticText(self, wx.ID_ANY, '     -0-'        )
        cbs.Add( self.impinjRateDisplay2, pos=(1,4), flag=wx.ALIGN_CENTER )
        self.impinjRateDisplay3 = wx.StaticText(self, wx.ID_ANY, '     -0-'        )
        cbs.Add( self.impinjRateDisplay3, pos=(1,7), flag=wx.ALIGN_CENTER )


        #sbs = wx.GridBagSizer( 12, 1 )
        #self.gbs.Add( sbs, pos=(5,0), span=(1,7), flag=wx.EXPAND|wx.ALL)

    # Status
        cbs.Add( wx.StaticText(self, wx.ID_ANY, 'Status:' ), pos=(2,0), flag=wx.ALIGN_CENTER )

        self.impinjCounterLabel1 = wx.StaticText(self, wx.ID_ANY, '          ' )
        cbs.Add( self.impinjCounterLabel1, pos=(2,1),   flag=wx.ALIGN_CENTER )

        self.impinjCounterLabel2 = wx.StaticText(self, wx.ID_ANY, '          ' )
        cbs.Add( self.impinjCounterLabel2, pos=(2,4),   flag=wx.ALIGN_CENTER )

        self.impinjCounterLabel3 = wx.StaticText(self, wx.ID_ANY, '          ' )
        cbs.Add( self.impinjCounterLabel3, pos=(2,7),   flag=wx.ALIGN_CENTER )

    # Antennas

        cbs.Add( wx.StaticText(self, wx.ID_ANY, 'Antennas:' ), pos=(3,0), flag=wx.ALIGN_CENTER )

        self.impinjAntennaLabel1 = wx.StaticText(self, wx.ID_ANY, '          ' )
        cbs.Add( self.impinjAntennaLabel1, pos=(3,1),   flag=wx.ALIGN_CENTER )

        self.impinjAntennaLabel2 = wx.StaticText(self, wx.ID_ANY, '          ' )
        cbs.Add( self.impinjAntennaLabel2, pos=(3,4),   flag=wx.ALIGN_CENTER )

        self.impinjAntennaLabel3 = wx.StaticText(self, wx.ID_ANY, '          ' )
        cbs.Add( self.impinjAntennaLabel3, pos=(3,7),   flag=wx.ALIGN_CENTER )

        #------------------------------------------------------------------------------------------------
        # Add messages

        self.impinjMessagesText = wx.TextCtrl( self, wx.ID_ANY, style=wx.TE_READONLY|wx.TE_MULTILINE|wx.HSCROLL, size=(-1,400) )
        fgs.Add( self.impinjMessagesText, flag=wx.EXPAND, proportion=2 )
        self.impinjMessages = MessageManager( self.impinjMessagesText )
        self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

        self.crossMgrMessagesText = wx.TextCtrl( self, wx.ID_ANY, style=wx.TE_READONLY|wx.TE_MULTILINE|wx.HSCROLL, size=(-1,400) )
        fgs.Add( self.crossMgrMessagesText, flag=wx.EXPAND, proportion=2 )
        self.crossMgrMessages = MessageManager( self.crossMgrMessagesText )
        self.crossMgrMessages.messageList.SetBackgroundColour(self.LightGreen )

        self.fgs = fgs

        #------------------------------------------------------------------------------------------------
        # Create a timer to update the messages.
        #
        self.timer = wx.Timer()
        self.timer.Bind( wx.EVT_TIMER, self.updateMessages )
        self.timer.Start( 1000, False )

        self.Bind(wx.EVT_CLOSE, self.onCloseWindow)

        self.readOptions()

        self.SetSizer( self.vbs )
        self.start()

    def start( self ):

        self.messageQ = Queue()
        self.shutdownTQ = Queue()       # Queue to tell the Impinj monitor to shut down.
        self.shutdownCQ1 = Queue()      # Queue to tell the Impinj monitor to shut down.
        self.shutdownCQ2 = Queue()      # Queue to tell the Impinj monitor to shut down.
        self.shutdownCQ3 = Queue()      # Queue to tell the Impinj monitor to shut down.
        self.shutdownIQ1 = Queue()      # Queue to tell the Impinj monitor to shut down.
        self.shutdownIQ2 = Queue()      # Queue to tell the Impinj monitor to shut down.
        self.shutdownIQ3 = Queue()      # Queue to tell the Impinj monitor to shut down.

        self.messageQ.put('start TEST');

        self.readerName1 = ""
        self.readerName2 = ""
        self.readerName3 = ""
        self. impinjCounter1 = [0,0,0,0,0];
        self. impinjCounter2 = [0,0,0,0,0];
        self. impinjCounter3 = [0,0,0,0,0];
        self. impinjRate1 = [0,0,0,0,0];
        self. impinjRate2 = [0,0,0,0,0];
        self. impinjRate3 = [0,0,0,0,0];
        self. impinjStatus1 = ['--'];
        self. impinjStatus2 = ['--'];
        self. impinjStatus3 = ['--'];
        self. impinjAntenna1 = [[]];
        self. impinjAntenna2 = [[]];
        self. impinjAntenna3 = [[]];

        self.lastReadTime = {}


        # start up to three CrossMgr processes
        # this also creates the three dataQ's that the Intermediate TagServer will use
        #
        if self.hostEnable1.GetValue() :
            self.dataQ1 = Queue()

            print('start: starting crossmgrprocess1 BBBB');

            self.crossMgrProcess1 = Process( name='CrossMgrProcess1', target=CrossMgrServer,
                            args=(self.dataQ1, self.messageQ, self.shutdownCQ1, self.getCrossMgrHost1(), CrossMgrPort1) )
            self.crossMgrProcess1.daemon = True
            self.crossMgrProcess1.start()
        else:
            self.dataQ1 = None
            self.crossMgrProcess1 = None


        if self.hostEnable2.GetValue() :
            self.dataQ2 = Queue()
            self.crossMgrProcess2 = Process( name='CrossMgrProcess2', target=CrossMgrServer,
                            args=(self.dataQ2, self.messageQ, self.shutdownCQ2, self.getCrossMgrHost2(), CrossMgrPort2) )
            self.crossMgrProcess2.daemon = True
            self.crossMgrProcess2.start()
        else:
            self.dataQ2 = None
            self.crossMgrProcess2 = None

        if self.hostEnable3.GetValue() :
            self.dataQ3 = Queue()
            self.crossMgrProcess3 = Process( name='CrossMgrProcess3', target=CrossMgrServer,
                            args=(self.dataQ3, self.messageQ, self.shutdownCQ3, self.getCrossMgrHost3(), CrossMgrPort3) )
            self.crossMgrProcess3.daemon = True
            self.crossMgrProcess3.start()
        else:
            self.dataQ3 = None
            self.crossMgrProcess3 = None

        # start the intermediate TagServer process
        # This will also create the dataQ that the three Impinj processes will use
        #
        print('start: CCCC');
        self.messageQ.put('start TEST2');
        self.dataQ0 = Queue()
        self.tagServerProcess = Process( name='TagServer', target=TagServer,
                        args=(self.dataQ0, self.dataQ1, self.dataQ2, self.dataQ3, self.messageQ, self.shutdownTQ, Impinj3.RepeatSeconds ) )
        self.tagServerProcess.daemon = True
        self.tagServerProcess.start()

        print('start: DDDD');
        self.messageQ.put('start TEST3');
        # start up to three Impinj processes
        if not self.impinjDisable1.GetValue():
            print('start: EEEE');
            if self.useHostName1.GetValue():
                self.readerName1 = ImpinjHostNamePrefix + self.impinjHostName1.GetValue() + ImpinjHostNameSuffix
            else:
                self.readerName1 = self.impinjHost1.GetAddress()

            # Session - 2
            # ModeIndex - 0 - Max Throughput
            # SearchMode - 1 - Single Target
            self.impinjProcess1 = Process( name='ImpinjProcess1', target=ImpinjServer,
                            args=(1, self.dataQ0, self.messageQ,
                            self.shutdownIQ1, self.readerName1, ImpinjInboundPort,
                            self.getAntennaStr1(), 2, Impinj3.ImpinjRFControlMode, self.lastReadTime, self.impinjCounter1, self.impinjRate1,
                            self.impinjStatus1, self.impinjAntenna1) )

            self.impinjProcess1.daemon = True
            self.impinjProcess1.start()
        else:
            self.impinjProcess1 = None

        if not self.impinjDisable2.GetValue():
            print('start: FFFF');
            if self.useHostName2.GetValue():
                self.readerName2 = ImpinjHostNamePrefix + self.impinjHostName2.GetValue() + ImpinjHostNameSuffix
            else:
                self.readerName2 = self.impinjHost2.GetAddress()

            # Session - 3
            # ModeIndex - 0 - Max Throughput
            # SearchMode - 1 - Single Target
            self.impinjProcess2 = Process( name='ImpinjProcess2', target=ImpinjServer,
                            args=(2, self.dataQ0, self.messageQ,
                            self.shutdownIQ2, self.readerName2, ImpinjInboundPort,
                            self.getAntennaStr2(), 3, Impinj3.ImpinjRFControlMode, self.lastReadTime, self.impinjCounter2, self.impinjRate2,
                            self.impinjStatus2, self.impinjAntenna2 ) )

            self.impinjProcess2.daemon = True
            self.impinjProcess2.start()
        else:
            self.impinjProcess2 = None

        if not self.impinjDisable3.GetValue():
            print('start: GGGG');
            if self.useHostName3.GetValue():
                self.readerName3 = ImpinjHostNamePrefix + self.impinjHostName3.GetValue() + ImpinjHostNameSuffix
            else:
                self.readerName3 = self.impinjHost3.GetAddress()

            # Session - 1
            # ModeIndex - 0 - Max Throughput
            # SearchMode - 2 - Dual Target
            self.impinjProcess3 = Process( name='ImpinjProcess3', target=ImpinjServer,
                            args=(3, self.dataQ0, self.messageQ,
                            self.shutdownIQ3, self.readerName3, ImpinjInboundPort,
                            self.getAntennaStr3(), 1, Impinj3.ImpinjRFControlMode, self.lastReadTime, self.impinjCounter3, self.impinjRate3,
                            self.impinjStatus3, self.impinjAntenna3 ) )

            self.impinjProcess3.daemon = True
            self.impinjProcess3.start()
        else:
            self.impinjProcess3 = None



    def shutdown( self ):
        print('MainWin3: shutdown!!')
        self.impinjProcess1 = None
        self.impinjProcess2 = None
        self.impinjProcess3 = None
        self.crossMgrProcess1 = None
        self.crossMgrProcess2 = None
        self.crossMgrProcess3 = None
        self.tagServerProcess = None
        self.messageQ = None
        self.dataQ0 = None
        self.dataQ1 = None
        self.dataQ2 = None
        self.dataQ3 = None
        self.shutdownQ = None
        self.shutdownCQ1 = None
        self.shutdownCQ2 = None
        self.shutdownCQ3 = None
        self.shutdownIQ1 = None
        self.shutdownIQ2 = None
        self.shutdownIQ3 = None

    def doReset( self, event, confirm = True ):
        if confirm:
            dlg = wx.MessageDialog(self, 'Reset CrossMgrImpinj Adapter?',
                                                            'Confirm Reset',
                                                            wx.OK | wx.CANCEL | wx.ICON_WARNING )
            ret = dlg.ShowModal()
            dlg.Destroy()
            if ret != wx.ID_OK:
                return

        self.reset.Enable( False )              # Prevent multiple clicks while shutting down.
        self.writeOptions()

        self.gracefulShutdown()

        self.impinjMessages.clear()
        self.crossMgrMessages.clear()
        self.shutdown()

        self.reset.Enable( True )

        wx.CallAfter( self.start )

    def doAutoDetect( self, event ):
        wx.BeginBusyCursor()
        self.gracefulShutdown()
        self.shutdown()
        impinjHost, crossMgrHost1 = AutoDetect(ImpinjInboundPort)[0], '127.0.0.1'
        wx.EndBusyCursor()

        if impinjHost and crossMgrHost1:
            self.useStaticAddress1.SetValue( True )
            self.useHostName1.SetValue( False )

            self.impinjHost1.SetValue( impinjHost )
            self.crossMgrHost1.SetValue( crossMgrHost1 )
        else:
            dlg = wx.MessageDialog(self, 'Auto Detect Failed.\nCheck that reader has power and is connected to the router.',
                                                            'Auto Detect Failed',
                                                            wx.OK | wx.ICON_INFORMATION )
            dlg.ShowModal()
            dlg.Destroy()

        self.doReset( event, False )

    def doAdvanced( self, event ):
        dlg = AdvancedSetup( self )
        dlg.ShowModal()
        dlg.Destroy()

    def gracefulShutdown( self ):
        # Shutdown the CrossMgr process by sending it a shutdown command.

        print('gracefulshutdown: entered')
        if self.shutdownTQ:
            print('gracefulshutdown: shutdownTQ')
            self.shutdownTQ.put( 'shutdown' )
            self.shutdownTQ.put( 'shutdown' )

        if self.shutdownCQ1:
            print('gracefulshutdown: shutdownCQ1')
            self.shutdownCQ1.put( 'shutdown' )
            self.shutdownCQ1.put( 'shutdown' )
        if self.shutdownCQ2:
            print('gracefulshutdown: shutdownCQ2')
            self.shutdownCQ2.put( 'shutdown' )
            self.shutdownCQ2.put( 'shutdown' )
        if self.shutdownCQ3:
            print('gracefulshutdown: shutdownCQ3')
            self.shutdownCQ3.put( 'shutdown' )
            self.shutdownCQ3.put( 'shutdown' )
        if self.shutdownIQ1:
            print('gracefulshutdown: shutdownIQ1')
            self.shutdownIQ1.put( 'shutdown' )
            self.shutdownIQ1.put( 'shutdown' )
        if self.shutdownIQ2:
            print('gracefulshutdown: shutdownIQ2')
            self.shutdownIQ2.put( 'shutdown' )
            self.shutdownIQ2.put( 'shutdown' )
        if self.shutdownIQ3:
            print('gracefulshutdown: shutdownIQ3')
            self.shutdownIQ3.put( 'shutdown' )
            self.shutdownIQ3.put( 'shutdown' )

        if self.dataQ0:
            print('gracefulshutdown: shutdownDQ0')
            self.dataQ0.put( 'shutdown' )
            self.dataQ0.put( 'shutdown' )
            self.dataQ0.put( 'shutdown' )
        if self.dataQ1:
            print('gracefulshutdown: shutdownDQ1')
            self.dataQ1.put( 'shutdown' )
            self.dataQ1.put( 'shutdown' )
            self.dataQ1.put( 'shutdown' )
        if self.dataQ2:
            print('gracefulshutdown: shutdownDQ2')
            self.dataQ2.put( 'shutdown' )
            self.dataQ2.put( 'shutdown' )
            self.dataQ2.put( 'shutdown' )
        if self.dataQ3:
            print('gracefulshutdown: shutdownDQ3')
            self.dataQ3.put( 'shutdown' )
            self.dataQ3.put( 'shutdown' )
            self.dataQ3.put( 'shutdown' )

        if self.crossMgrProcess1 is not None:
            print('gracefulshutdown: join crossMgrProcess1')
            self.crossMgrProcess1.join()
        if self.crossMgrProcess2 is not None:
            print('gracefulshutdown: join crossMgrProcess2')
            self.crossMgrProcess2.join()
        if self.crossMgrProcess3 is not None:
            print('gracefulshutdown: join crossMgrProcess3')
            self.crossMgrProcess3.join()

        if self.tagServerProcess is not None:
            print('gracefulshutdown: join tagServerProcess')
            self.tagServerProcess.join()

        if self.impinjProcess1 is not None:
            print('gracefulshutdown: join impinjProcess1')
            self.impinjProcess1.join()
        if self.impinjProcess2 is not None:
            print('gracefulshutdown: join impinjProcess2')
            self.impinjProcess2.join()
        if self.impinjProcess3 is not None:
            print('gracefulshutdown: join impinjProcess2')
            self.impinjProcess3.join()

        self.crossMgrProcess1 = None
        self.crossMgrProcess2 = None
        self.crossMgrProcess3 = None
        self.tagServerProcess = None

        #self.impinjProcess = None

    def onCloseWindow( self, event ):
        self.gracefulShutdown()
        wx.Exit()

    def doCopyToClipboard( self, event ):
        cc = [
                'Configuration: CrossMgrImpinj3',
                '    RunningTime:   {}'.format(self.runningTime.GetLabel()),
                '    Time:          {}'.format(self.time.GetLabel()),
                '    BackupFile:    {}'.format(self.backupFile.GetLabel()),
                '',
                'Configuration: Impinj:',
                '    ImpinjDisable1:{}'.format('True' if self.impinjDisable1.GetValue() else 'False'),
                '    ImpinjDisable2:{}'.format('True' if self.impinjDisable2.GetValue() else 'False'),
                '    ImpinjDisable3:{}'.format('True' if self.impinjDisable3.GetValue() else 'False'),

                '    HostEnable1:{}'.format('True' if self.hostEnable1.GetValue() else 'True'),
                '    HostEnable2:{}'.format('True' if self.hostEnable2.GetValue() else 'False'),
                '    HostEnable3:{}'.format('True' if self.hostEnable3.GetValue() else 'False'),

                '    Use Host Name1:{}'.format('True' if self.useHostName1.GetValue() else 'False'),
                '    Use Host Name2:{}'.format('True' if self.useHostName2.GetValue() else 'False'),
                '    Use Host Name3:{}'.format('True' if self.useHostName3.GetValue() else 'False'),

                '    HostName1:     {}'.format((ImpinjHostNamePrefix + self.impinjHostName1.GetValue()) + ImpinjHostNameSuffix),
                '    HostName2:     {}'.format((ImpinjHostNamePrefix + self.impinjHostName2.GetValue()) + ImpinjHostNameSuffix),
                '    HostName3:     {}'.format((ImpinjHostNamePrefix + self.impinjHostName3.GetValue()) + ImpinjHostNameSuffix),

                '    ImpinjHost1:   {}'.format(self.impinjHost1.GetAddress()),
                '    ImpinjHost2:   {}'.format(self.impinjHost2.GetAddress()),
                '    ImpinjHost3:   {}'.format(self.impinjHost3.GetAddress()),

                '    ImpinjPort:    {}'.format(ImpinjInboundPort),
                ''
                '    ImpinjRFControlMode: {}'.format(Impinj3.ImpinjRFControlMode),
                '    ImpinjTagObservatgionTrigger: {}'.format(Impinj3.ImpinjTagObservatgionTrigger),
                '    ImpinjT: {}'.format(Impinj3.ImpinjT),
                '    ImpinjTimeout: {}'.format(Impinj3.ImpinjTimeout),
                '    TransmitPower: {}'.format(Impinj3.TransmitPower),
                '    ConnectionTimeoutSeconds: {}'.format(Impinj3.ConnectionTimeoutSeconds),
                '    KeepaliveSeconds:         {}'.format(Impinj3.KeepaliveSeconds),
                '    RepeatSeconds:            {}'.format(Impinj3.RepeatSeconds),
                '',
                'Configuration: CrossMgr',
                '    CrossMgrHost1:  {}'.format(self.getCrossMgrHost1()),
                '    CrossMgrPort1:  {}'.format(CrossMgrPort1),
                '    CrossMgrHost2:  {}'.format(self.getCrossMgrHost2()),
                '    CrossMgrPort2:  {}'.format(CrossMgrPort2),
                '    CrossMgrHost3:  {}'.format(self.getCrossMgrHost3()),
                '    CrossMgrPort3:  {}'.format(CrossMgrPort3),
        ]
        cc.append( '\nLog: Impinj' )
        log = self.impinjMessagesText.GetValue()
        cc.extend( ['    ' + line for line in log.split('\n')] )

        cc.append( '\nLog: CrossMgr' )
        log = self.crossMgrMessagesText.GetValue()
        cc.extend( ['    ' + line for line in log.split('\n')] )

        cc.append( '\nLog: Application\n' )
        try:
            with open(redirectFileName, 'r') as fp:
                for line in fp:
                    cc.append( line )
        except:
            pass

        if wx.TheClipboard.Open():
            do = wx.TextDataObject()
            do.SetText( '\n'.join(cc) )
            wx.TheClipboard.SetData(do)
            wx.TheClipboard.Close()
            dlg = wx.MessageDialog(self, 'Configuration and Logs copied to the Clipboard.',
                                                            'Copy to Clipboard Succeeded',
                                                            wx.OK | wx.ICON_INFORMATION )
            ret = dlg.ShowModal()
            dlg.Destroy()
        else:
            # oops... something went wrong!
            wx.MessageBox("Unable to open the clipboard", "Error")

    def getCrossMgrHost1( self ):
        return self.crossMgrHost1.GetAddress()

    def getCrossMgrHost2( self ):
        return self.crossMgrHost2.GetAddress()

    def getCrossMgrHost3( self ):
        return self.crossMgrHost3.GetAddress()

    def getAntennaStr1( self ):
        s = []
        for i in xrange(4):
            if self.antennas1[i].GetValue():
                s.append( '%d' % (i + 1) )
        return ' '.join( s )

    def setAntennaStr1( self, s ):
        antennas = set( int(a) for a in s.split() )
        for i in xrange(4):
            self.antennas1[i].SetValue( (i+1) in antennas )

    def getAntennaStr2( self ):
        s = []
        for i in xrange(4):
            if self.antennas2[i].GetValue():
                s.append( '%d' % (i + 1) )
        return ' '.join( s )

    def setAntennaStr2( self, s ):
        antennas = set( int(a) for a in s.split() )
        for i in xrange(4):
            self.antennas2[i].SetValue( (i+1) in antennas )

    def getAntennaStr3( self ):
        s = []
        for i in xrange(4):
            if self.antennas3[i].GetValue():
                s.append( '%d' % (i + 1) )
        return ' '.join( s )

    def setAntennaStr3( self, s ):
        antennas = set( int(a) for a in s.split() )
        for i in xrange(4):
            self.antennas3[i].SetValue( (i+1) in antennas )

    def writeOptions( self ):
        self.config.Write( 'CrossMgrHost1', self.getCrossMgrHost1() )
        self.config.Write( 'CrossMgrHost2', self.getCrossMgrHost2() )
        self.config.Write( 'CrossMgrHost3', self.getCrossMgrHost3() )

        self.config.Write( 'ImpinjDisable1', 'True' if self.impinjDisable1.GetValue() else 'False' )
        self.config.Write( 'ImpinjDisable2', 'True' if self.impinjDisable2.GetValue() else 'False' )
        self.config.Write( 'ImpinjDisable3', 'True' if self.impinjDisable3.GetValue() else 'False' )

        self.config.Write( 'HostEnable1', 'True' if self.hostEnable1.GetValue() else 'False' )
        self.config.Write( 'HostEnable2', 'True' if self.hostEnable2.GetValue() else 'False' )
        self.config.Write( 'HostEnable3', 'True' if self.hostEnable3.GetValue() else 'False' )

        self.config.Write( 'UseHostName1', 'True' if self.useHostName1.GetValue() else 'False' )
        self.config.Write( 'UseHostName2', 'True' if self.useHostName2.GetValue() else 'False' )
        self.config.Write( 'UseHostName3', 'True' if self.useHostName3.GetValue() else 'False' )

        self.config.Write( 'ImpinjHostName1', ImpinjHostNamePrefix + self.impinjHostName1.GetValue() + ImpinjHostNameSuffix )
        self.config.Write( 'ImpinjHostName2', ImpinjHostNamePrefix + self.impinjHostName2.GetValue() + ImpinjHostNameSuffix )
        self.config.Write( 'ImpinjHostName3', ImpinjHostNamePrefix + self.impinjHostName3.GetValue() + ImpinjHostNameSuffix )

        self.config.Write( 'ImpinjAddr1', self.impinjHost1.GetAddress() )
        self.config.Write( 'ImpinjAddr2', self.impinjHost2.GetAddress() )
        self.config.Write( 'ImpinjAddr3', self.impinjHost3.GetAddress() )

        self.config.Write( 'ImpinjPort', '{}'.format(ImpinjInboundPort) )
        self.config.Write( 'Antennas1', self.getAntennaStr1() )
        self.config.Write( 'Antennas2', self.getAntennaStr2() )
        self.config.Write( 'Antennas3', self.getAntennaStr3() )

        self.config.Write( 'ImpinjRFControlMode', '{}'.format(Impinj3.ImpinjRFControlMode) )
        self.config.Write( 'ImpinjTriggerType', '{}'.format(Impinj3.ImpinjTriggerType) )
        self.config.Write( 'ImpinjT', '{}'.format(Impinj3.ImpinjT) )
        self.config.Write( 'ImpinjTimeout', '{}'.format(Impinj3.ImpinjTimeout) )
        self.config.Write( 'TransmitPower', '{}'.format(Impinj3.TransmitPower) )
        self.config.Write( 'ConnectionTimeoutSeconds', '{}'.format(Impinj3.ConnectionTimeoutSeconds) )
        self.config.Write( 'KeepaliveSeconds', '{}'.format(Impinj3.KeepaliveSeconds) )
        self.config.Write( 'RepeatSeconds', '{}'.format(Impinj3.RepeatSeconds) )
        self.config.Flush()

    def readOptions( self ):
        self.crossMgrHost1.SetValue( self.config.Read('CrossMgrHost1', Utils.DEFAULT_HOST) )
        self.crossMgrHost2.SetValue( self.config.Read('CrossMgrHost2', Utils.DEFAULT_HOST) )
        self.crossMgrHost3.SetValue( self.config.Read('CrossMgrHost3', Utils.DEFAULT_HOST) )

        impinjDisable1 = (self.config.Read('ImpinjDisable1', 'False').upper()[:1] == 'T')
        impinjDisable2 = (self.config.Read('ImpinjDisable2', 'False').upper()[:1] == 'T')
        impinjDisable3 = (self.config.Read('ImpinjDisable3', 'False').upper()[:1] == 'T')

        self.impinjDisable1.SetValue( impinjDisable1 )
        self.impinjDisable2.SetValue( impinjDisable2 )
        self.impinjDisable3.SetValue( impinjDisable3 )

        hostEnable1 = (self.config.Read('HostEnable1', 'False').upper()[:1] == 'T')
        hostEnable2 = (self.config.Read('HostEnable2', 'False').upper()[:1] == 'T')
        hostEnable3 = (self.config.Read('HostEnable3', 'False').upper()[:1] == 'T')

        self.hostEnable1.SetValue( hostEnable1 )
        self.hostEnable2.SetValue( hostEnable2 )
        self.hostEnable3.SetValue( hostEnable3 )


        useHostName1 = (self.config.Read('UseHostName1', 'True').upper()[:1] == 'T')
        useHostName2 = (self.config.Read('UseHostName2', 'True').upper()[:1] == 'T')
        useHostName3 = (self.config.Read('UseHostName3', 'True').upper()[:1] == 'T')

        self.useHostName1.SetValue( useHostName1 and not impinjDisable1 )
        self.useHostName2.SetValue( useHostName2 and not impinjDisable2 )
        self.useHostName3.SetValue( useHostName3 and not impinjDisable2 )

        self.useStaticAddress1.SetValue( not useHostName1 and not impinjDisable1 )
        self.useStaticAddress2.SetValue( not useHostName2 and not impinjDisable2 )
        self.useStaticAddress3.SetValue( not useHostName3 and not impinjDisable3 )


        self.impinjHostName1.SetValue( self.config.Read('ImpinjHostName1', ImpinjHostNamePrefix + '00-00-00' + ImpinjHostNameSuffix)[len(ImpinjHostNamePrefix):-len(ImpinjHostNameSuffix)] )
        self.impinjHostName2.SetValue( self.config.Read('ImpinjHostName2', ImpinjHostNamePrefix + '00-00-00' + ImpinjHostNameSuffix)[len(ImpinjHostNamePrefix):-len(ImpinjHostNameSuffix)] )
        self.impinjHostName3.SetValue( self.config.Read('ImpinjHostName3', ImpinjHostNamePrefix + '00-00-00' + ImpinjHostNameSuffix)[len(ImpinjHostNamePrefix):-len(ImpinjHostNameSuffix)] )

        self.impinjHost1.SetValue( self.config.Read('ImpinjAddr1', '0.0.0.0') )
        self.impinjHost2.SetValue( self.config.Read('ImpinjAddr2', '0.0.0.0') )
        self.impinjHost3.SetValue( self.config.Read('ImpinjAddr3', '0.0.0.0') )

        self.setAntennaStr1( self.config.Read('Antennas1', '1 2 3 4') )
        self.setAntennaStr2( self.config.Read('Antennas2', '1 2 3 4') )
        self.setAntennaStr3( self.config.Read('Antennas3', '1 2 3 4') )

        Impinj3.ImpinjRFControlMode = int(self.config.Read( 'ImpinjRFControlMode', '{}'.format(Impinj3.ImpinjRFControlMode)))
        if Impinj3.ImpinjRFControlMode < 0:
            Impinj3.ImpinjRFControlMode = 0
        Impinj3.ImpinjTriggerType = int(self.config.Read( 'ImpinjTriggerType', '{}'.format(Impinj3.ImpinjTriggerType)))
        if Impinj3.ImpinjTriggerType < 1:
            Impinj3.ImpinjTriggerType = 1
        Impinj3.ImpinjT = int(self.config.Read( 'ImpinjT', '{}'.format(Impinj3.ImpinjT)))
        Impinj3.ImpinjTimeout = int(self.config.Read( 'ImpinjTimeout', '{}'.format(Impinj3.ImpinjTimeout)))
        Impinj3.TransmitPower= int(self.config.Read( 'TransmitPower', '{}'.format(Impinj3.TransmitPower)))
        Impinj3.ConnectionTimeoutSeconds = int(self.config.Read( 'ConnectionTimeoutSeconds', '{}'.format(Impinj3.ConnectionTimeoutSeconds)))
        Impinj3.KeepaliveSeconds = int(self.config.Read( 'KeepaliveSeconds', '{}'.format(Impinj3.KeepaliveSeconds)))
        Impinj3.RepeatSeconds = int(self.config.Read( 'RepeatSeconds', '{}'.format(Impinj3.RepeatSeconds)))

    def updateMessages( self, event ):
        tNow = datetime.datetime.now()
        running = int((tNow - self.tStart).total_seconds())
        self.runningTime.SetLabel( '%02d:%02d:%02d' % (running // (60*60), (running // 60) % 60, running % 60) )
        self.time.SetLabel( tNow.strftime('%H:%M:%S') )

        self.impinjCounterLabel1.SetLabel( '%s' % (self.impinjStatus1[0]) )
        self.impinjCounterLabel2.SetLabel( '%s' % (self.impinjStatus2[0]) )
        self.impinjCounterLabel3.SetLabel( '%s' % (self.impinjStatus3[0]) )

        self.impinjCounterDisplay1.SetLabel( '%6d' % (self.impinjCounter1[0]) )
        self.impinjCounterDisplay2.SetLabel( '%6d' % (self.impinjCounter2[0]) )
        self.impinjCounterDisplay3.SetLabel( '%6d' % (self.impinjCounter3[0]) )

        self.impinjRateDisplay1.SetLabel( '%4d' % (self.impinjRate1[0]) )
        self.impinjRateDisplay2.SetLabel( '%4d' % (self.impinjRate2[0]) )
        self.impinjRateDisplay3.SetLabel( '%4d' % (self.impinjRate3[0]) )


        self.impinjAntennaLabel1.SetLabel( '%s' % (self.impinjAntenna1[0]) )
        self.impinjAntennaLabel2.SetLabel( '%s' % (self.impinjAntenna2[0]) )
        self.impinjAntennaLabel3.SetLabel( '%s' % (self.impinjAntenna3[0]) )

        if not self.messageQ:
            print('updateMessages: no messageQ')
            return
        while 1:
            try:
                d = self.messageQ.get( False )
            except Empty:
                break

            message = ' '.join( str(x) for x in d[1:] )
            if   d[0] == 'Impinj':
                self.impinjMessages.write( message )

            elif   d[0] == 'Impinj-Alert':
                self.impinjMessages.messageList.SetBackgroundColour(self.LightRed )
                self.impinjMessages.write( message )
                self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif   d[0] == 'Impinj-Warning':
                self.impinjMessages.messageList.SetBackgroundColour(self.LightOrange )
                self.impinjMessages.write( message )
                self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'Impinj2Queue':
                self.crossMgrMessages.write( message )

            elif d[0] == 'Impinj2Queue-Alert':
                self.impinjMessages.messageList.SetBackgroundColour(self.LightRed )
                self.crossMgrMessages.write( message )
                self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'Impinj2Queue-Warning':
                self.impinjMessages.messageList.SetBackgroundColour(self.LightOrange )
                self.crossMgrMessages.write( message )
                self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'Queue2JChip':
                self.crossMgrMessages.write( message )

            elif d[0] == 'Queue2JChip-Alert':
                self.crossMgrMessages.messageList.SetBackgroundColour(self.LightRed )
                self.crossMgrMessages.write( message )
                self.crossMgrMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'Queue2JChip-Warning':
                self.crossMgrMessages.messageList.SetBackgroundColour(self.LightOrange )
                self.crossMgrMessages.write( message )
                self.crossMgrMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'BackupFile':
                self.backupFile.SetLabel( os.path.basename(d[1]) )


def disable_stdout_buffering():
    fileno = sys.stdout.fileno()
    temp_fd = os.dup(fileno)
    #sys.stdout.close()
    os.dup2(temp_fd, fileno)
    os.close(temp_fd)
    sys.stdout = os.fdopen(fileno, "w", 0)

redirectFileName = None
mainWin = None
def MainLoop():
    global mainWin, redirectFileName

    app = wx.App(False)
    app.SetAppName("CrossMgrImpinj3")

    mainWin = MainWin( None, title=AppVerName, size=(1000,1000) )

    dataDir = Utils.getHomeDir()
    print("dataDir: %s" % (dataDir))

    redirectFileName = os.path.join(dataDir, 'CrossMgrImpinj3.log')

    print("redirectFileName: %s" % (redirectFileName))


    # Set up the log file.  Otherwise, show errors on the screen.
    if __name__ == '__main__':
        print('__main__, disable buffering');
        disable_stdout_buffering()
    else:
        print('Redirect to log');
        try:
            logSize = os.path.getsize( redirectFileName )
            if logSize > 1000000:
                os.remove( redirectFileName )
        except:
            pass

        try:
            app.RedirectStdio( redirectFileName )
        except:
            pass

        try:
            with open(redirectFileName, 'a') as pf:
                pf.write( '********************************************\n' )
                pf.write( '%s: %s Started.\n' % (datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S'), AppVerName) )
        except:
            pass

    mainWin.Show()

    # Set the upper left icon.
    try:
        icon = wx.Icon( os.path.join(getImageFolder(), 'CrossMgrImpinj.ico'), wx.BITMAP_TYPE_ICO )
        mainWin.SetIcon( icon )
    except:
        pass

    # start processing events.
    mainWin.Refresh()
    app.MainLoop()

@atexit.register
def shutdown():
    if mainWin:
        mainWin.shutdown()

if __name__ == '__main__':
    MainLoop()
