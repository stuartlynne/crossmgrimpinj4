#
# vim: syntax=python tabstop=4 expandtab shiftwidth=4 softtabstop=8
#
#

import sys
import threading
import socket
import atexit
import time
import Utils
from queue import Empty
from threading import Thread as Process
from threading import Event
from queue import Queue
import Impinj3
import Impinj2JChip2
import enum
from enum import Enum
import traceback
import six
from functools import partial
import Tags
from Tags import ReaderPriority
from Impinj3 import ImpinjServer, ConnectionStatus
from Queue2JChip import CrossMgrServer
from Impinj2JChip2 import TagServer

from AutoDetect import AutoDetect
import os
import datetime
getTimeNow = datetime.datetime.now
HOME_DIR = os.path.expanduser("~")


import wx
import wx.lib.masked as masked
import wx.lib.intctrl as intctrl
import wx._adv as adv
import wx._html as html
import wx._xml as xml
import wx.richtext as rt
import sys
import os
import re
import datetime

from Version import AppVerName

CrossMgrPort = [53135 for i in range(3)]
ImpinjHostNamePrefix = 'SpeedwayR-'
ImpinjHostNameSuffix = '.local'
ImpinjInboundPort = 5084
#ReaderPositionPairs = True
#ReaderPositionLinear = False
#ReaderPositionNone = False

RFControlMaxThroughput = True
#RFControlHybrid80k160k = False
#RFControlDenseReaderM4 = False
#RFControlDenseReaderM8 = False
#RFControlMaxMiller = False

ReaderPositionPairs = False
ReaderPositionLinear = False
ReaderPositionNone = False

#ImpinjInboundPort = 50840

clipboard_xpm = [
"16 15 23 1",
"+ c #769CDA",
": c #DCE6F6",
"X c #3365B7",
"* c #FFFFFF",
"o c #9AB6E4",
"< c #EAF0FA",
"# c #B1C7EB",
". c #6992D7",
"3 c #F7F9FD",
", c #F0F5FC",
"$ c #A8C0E8",
"  c None",
"- c #FDFEFF",
"& c #C4D5F0",
"1 c #E2EAF8",
"O c #89A9DF",
"= c #D2DFF4",
"4 c #FAFCFE",
"2 c #F5F8FD",
"; c #DFE8F7",
"% c #B8CCEC",
"> c #E5EDF9",
"@ c #648FD6",
" .....XX        ",
" .oO+@X#X       ",
" .$oO+X##X      ",
" .%$o........   ",
" .&%$.*=&#o.-.  ",
" .=&%.*;=&#.--. ",
" .:=&.*>;=&.... ",
" .>:=.*,>;=&#o. ",
" .<1:.*2,>:=&#. ",
" .2<1.*32,>:=&. ",
" .32<.*432,>:=. ",
" .32<.*-432,>:. ",
" .....**-432,>. ",
"     .***-432,. ",
"     .......... "
]




class MessageManager( object ):
    MessagesMax = 400        # Maximum number of messages before we start throwing some away.

    def __init__( self, messageList ):
        self.messageList = messageList
        self.messageList.Bind( wx.EVT_RIGHT_DOWN, self.skip )
        self.messageList.SetDoubleBuffered( True )
        self.clear()

    def skip(self, evt):
        return

    def write( self, message ):
        if len(self.messages) >= self.MessagesMax:
            self.messages = self.messages[int(self.MessagesMax):]
            s = '\n'.join( self.messages )
            self.messageList.ChangeValue( s + '\n' )
        self.messages.append( message )
        self.messageList.AppendText( message + '\n' )

    def clear( self ):
        self.messages = []
        self.messageList.ChangeValue( '' )
        self.messageList.SetInsertionPointEnd()

def setFont( font, w ):
    w.SetFont( font )
    return w

class AdvancedSetup( wx.Dialog ):
    def __init__( self, parent, id = wx.ID_ANY ):

        wx.Dialog.__init__( self, parent, id, "Advanced Setup",
                                        style=wx.DEFAULT_DIALOG_STYLE|wx.TAB_TRAVERSAL )

        bs = wx.GridBagSizer(vgap=5, hgap=5)

        border = 8
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Advanced Reader Options:'), pos = (0,0), span=(1, 2), border = border, flag=wx.ALL )

        row = 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Impinj RFControl Mode'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.ImpinjRFControlMode = intctrl.IntCtrl( self, wx.ID_ANY, min=-1, max=9999, limited = True,
                value = Impinj3.ImpinjRFControlMode, size=(32,-1) )
        bs.Add( self.ImpinjRFControlMode, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Impinj RFControl: 0: max throughput; 4: MaxMiller; 1: hybrid, 2: dense reader M=4 hispd; 3: dense reader M=8 lospd'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        ########
        # RF Control Radio Buttons
        #print('RFControlMaxThroughput: %s' % RFControlMaxThroughput, file=sys.stderr)
        #print('RFControlHybrid80k160k: %s' % RFControlHybrid80k160k, file=sys.stderr)
        #print('RFControlDenseReaderM4: %s' % RFControlDenseReaderM4, file=sys.stderr)
        #print('RFControlDenseReaderM8: %s' % RFControlDenseReaderM8, file=sys.stderr)
        #print('RFControlMaxMiller: %s'     % RFControlMaxMiller    , file=sys.stderr)
        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Impinj RF Control Mode'),
                pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        self.RFControlMaxThroughputRB= wx.RadioButton( self, label='MaxThroughput:', style=wx.RB_GROUP)
        bs.Add(self.RFControlMaxThroughputRB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
        bs.Add( wx.StaticText(self, wx.ID_ANY,
                'High Speed 640k bps (no other readers in area)'),
                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
        print('RFControlMaxThroughputRB: %s' % self.RFControlMaxThroughputRB.GetValue(), file=sys.stderr)

        row += 1
        self.RFControlHybrid80k160kRB= wx.RadioButton( self, label='Hybrid80/160:', )
        bs.Add(self.RFControlHybrid80k160kRB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
        bs.Add( wx.StaticText(self, wx.ID_ANY,
                'Hybrid 80k / 160k bps'),
                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
        print('RFControlHybrid80k160kRB: %s' % self.RFControlHybrid80k160kRB.GetValue(), file=sys.stderr)

#                row += 1
#                self.RFControlDenseReaderM4RB= wx.RadioButton( self, label='DenseReaderM4:', )
#                bs.Add(self.RFControlDenseReaderM4RB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
#                bs.Add( wx.StaticText(self, wx.ID_ANY,
#                        'Dense Reader M=4 Lo Speed'),
#                        pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
#                print('NreaderPositionNoneRB: %s' % self.NreaderPositionNoneNB.GetValue(), file=sys.stderr)
#
#                row += 1
#                self.RFControlDenseReaderM8RB= wx.RadioButton( self, label='DenseReaderM8:', )
#                bs.Add(self.RFControlDenseReaderM8RB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
#                bs.Add( wx.StaticText(self, wx.ID_ANY,
#                        'Dense Reader M=8 Hi Speed'),
#                        pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
#
#                row += 1
#                self.RFControlMaxMillerRB= wx.RadioButton( self, label='MaxMiller:', )
#                bs.Add(self.RFControlMaxMillerRB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
#                bs.Add( wx.StaticText(self, wx.ID_ANY,
#                        'Dense Reader MaxMiller (good alternative if other readers in area)'),
#                        pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
#                print('NreaderPositionNoneRB: %s' % self.NreaderPositionNoneNB.GetValue(), file=sys.stderr)
#
#
#                if ReaderPositionPairs:
#                        self.RFControlMaxThroughputRB.SetValue(True)
#                elif ReaderPositionLinear:
#                        self.RFControlHybrid80k160kRB.SetValue(True)
#                else:
#                        self.NreaderPositionNoneRB.SetValue(True)
#
#        row += 1
#        self.RFControlMaxThroughputRB = wx.RadioButton( self, label='MaxThroughput:', style=wx.RB_GROUP)
#        bs.Add(self.RFControlMaxThroughputRB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
#        bs.Add( wx.StaticText(self, wx.ID_ANY,
#                'High Speed 640k bps (no other readers in area)'),
#                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
#
#        row += 1
#        self.RFControlHybrid80k160kRB = wx.RadioButton( self, label='Hybrid80/160:', style=wx.RB_GROUP)
#        bs.Add(self.RFControlHybrid80k160kRB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
#        bs.Add( wx.StaticText(self, wx.ID_ANY,
#                'Hybrid 80k / 160k bps'),
#                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
#
#        row += 1
#        self.RFControlDenseReaderM4RB = wx.RadioButton( self, label='DenseM4:', style=wx.RB_GROUP)
#        bs.Add(self.RFControlDenseReaderM4RB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
#        bs.Add( wx.StaticText(self, wx.ID_ANY,
#                'Dense Reader M=4 Lo Speed'),
#                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
#
#        row += 1
#        self.RFControlDenseReaderM8RB = wx.RadioButton( self, label='DenseM4:', style=wx.RB_GROUP)
#        bs.Add(self.RFControlDenseReaderM8RB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
#        bs.Add( wx.StaticText(self, wx.ID_ANY,
#                'Dense Reader M=8 Hi Speed'),
#                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
#
#        row += 1
#        self.RFControlMaxMillerRB = wx.RadioButton( self, label='MaxMiller:', style=wx.RB_GROUP)
#        bs.Add(self.RFControlMaxMillerRB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
#        bs.Add( wx.StaticText(self, wx.ID_ANY,
#                'Dense Reader MaxMiller (good alternative if other readers in area)'),
#                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )



#        if RFControlMaxThroughput:
#                self.RFControlMaxThroughputRB.SetValue(True)
#        elif RFControlYhbrid80k160k:
#                self.RFControlHybrid80k160kRB.SetValue(True)
#        elif RFControlDenseReaderM4:
#                self.RFControlDenseReaderM4RB.SetValue(True)
#        elif RFControlDenseReaderM8:
#                self.RFControlDenseReaderM8RB.SetValue(True)
#        elif RFControMaxMillerRB:
#                self.RFControlMaxMillerRB.SetValue(True)

#        print('RFControlMaxThroughputRB: %s' % self.RFControlMaxThroughputRB.GetValue(), file=sys.stderr)
#        print('RFControlHybrid80k160kRB: %s' % self.RFControlHybrid80k160kRB.GetValue(), file=sys.stderr)
#        print('RFControlDenseReaderM4RB: %s' % self.RFControlDenseReaderM4RB.GetValue(), file=sys.stderr)
#        print('RFControlDenseReaderM8RB: %s' % self.RFControlDenseReaderM8RB.GetValue(), file=sys.stderr)
#        print('RFControlMaxMillerRB: %s' % self.RFControlMaxMillerRB.GetValue(), file=sys.stderr)


        # Reader Positioning Radio Buttons
        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Reader/Antenna Positioning - how reader/antennas are positioned'),
                pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        row += 1
        self.readerPositionPairsRB = wx.RadioButton( self, label='Pairs:', style=wx.RB_GROUP)
        bs.Add(self.readerPositionPairsRB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
        bs.Add( wx.StaticText(self, wx.ID_ANY,
                'Readers are paired, one on each side of street, Reader 1 and 2, 1.2m before the finish line, Reader 3 and 4, 2-3m past finish line'),
                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
        print('readerPositionPairsRB: %s' % self.readerPositionPairsRB.GetValue(), file=sys.stderr)

        row += 1
        self.readerPositionLinearRB = wx.RadioButton( self, label='Linear:', )
        bs.Add(self.readerPositionLinearRB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
        bs.Add( wx.StaticText(self, wx.ID_ANY,
                'Readers are all on one side, in order, 1, 2, 3, 4'),
                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
        print('readerPositionLinearRB: %s' % self.readerPositionLinearRB.GetValue(), file=sys.stderr)

        row += 1
        self.readerPositionNoneRB = wx.RadioButton( self, label='None:', )
        bs.Add(self.readerPositionNoneRB, pos=(row,1), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL)
        bs.Add( wx.StaticText(self, wx.ID_ANY,
                'Do not prioritize any readers'),
                pos=(row, 2), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )
        print('readerPositionLinearRB: %s' % self.readerPositionLinearRB.GetValue(), file=sys.stderr)

        if ReaderPositionPairs:
            self.readerPositionPairsRB.SetValue(True)
        elif ReaderPositionLinear:
            self.readerPositionLinearRB.SetValue(True)
        else:
            self.readerPositionNoneRB.SetValue(True)


        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'TransmitPower'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.TransmitPower= intctrl.IntCtrl( self, wx.ID_ANY, min=0, max=3250, limited = True,
                value = Impinj3.TransmitPower, size=(62,-1) )
        bs.Add( self.TransmitPower, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Transmit PowerLevel Transmit Power 1000-3250, default is zero, which is maximum power'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Connection Timeout Seconds'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.ConnectionTimeoutSeconds = intctrl.IntCtrl( self, wx.ID_ANY, min=1, max=60, limited = True,
                value = Impinj3.ConnectionTimeoutSeconds, size=(32,-1) )
        bs.Add( self.ConnectionTimeoutSeconds, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'maximum time to wait for a reader response'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Keepalive Seconds'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.KeepaliveSeconds = intctrl.IntCtrl( self, wx.ID_ANY, min=1, max=60, limited = True,
                value = Impinj3.KeepaliveSeconds, size=(32,-1) )
        bs.Add( self.KeepaliveSeconds, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'frequency of "heartbeat" messages indicating the reader is still connected'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Repeat Seconds'), pos=(row, 0), span=(1,1), border = border, flag=wx.LEFT|wx.TOP|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
        self.RepeatSeconds = intctrl.IntCtrl( self, wx.ID_ANY, min=1, max=120, limited = True,
                value = Impinj3.RepeatSeconds, size=(32,-1) )
        bs.Add( self.RepeatSeconds, pos=(row, 1), span=(1,1), border = border, flag=wx.TOP )
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'interval in which multiple tag reads are considered "repeats" and not reported'),
                        pos=(row, 2), span=(1,1), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL )

        row += 1
        self.restoreDefaultButton = wx.Button( self, wx.ID_ANY, 'Restore Defaults' )
        self.restoreDefaultButton.Bind( wx.EVT_BUTTON, self.onRestoreDefault )
        bs.Add( self.restoreDefaultButton, pos=(row, 0), span=(1,3), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_CENTER )

        row += 1
        bs.Add( wx.StaticText(self, wx.ID_ANY, 'Reminder: Press "Reset" for these changes to take effect.'),
                        pos=(row, 0), span=(1,3), border = border, flag=wx.TOP|wx.RIGHT|wx.ALIGN_RIGHT )

        self.okBtn = wx.Button( self, wx.ID_OK )
        self.Bind( wx.EVT_BUTTON, self.onOK, self.okBtn )

        self.cancelBtn = wx.Button( self, wx.ID_CANCEL )
        self.Bind( wx.EVT_BUTTON, self.onCancel, self.cancelBtn )

        row += 1
        hs = wx.BoxSizer( wx.HORIZONTAL )
        hs.Add( self.okBtn, border = border, flag=wx.ALL )
        self.okBtn.SetDefault()
        hs.Add( self.cancelBtn, border = border, flag=wx.ALL )

        bs.Add( hs, pos=(row, 0), span=(1,3), flag=wx.ALIGN_RIGHT )

        self.SetSizerAndFit(bs)
        bs.Fit( self )

        self.CentreOnParent(wx.BOTH)
        self.SetFocus()

    def onRestoreDefault( self, event ):

        #self.ImpinjRFControlMode.SetValue( Impinj3.ImpinjRFControlDefault )

        self.RFControlMaxThroughputRB.SetValue( True)
        self.RFControlHybrid80k160kRB.SetValue( False)
        self.RFControlDenseReaderM4RB.SetValue( False)
        self.RFControlDenseReaderM8RB.SetValue( False)
        self.RFControlMaxMillerRB.SetValue( False)

        self.readerPositionPairsRB.SetValue( True)
        self.readerPositionLinearRB.SetValue( False )
        self.readerPositionNone.SetValueRB( False )

        self.TransmitPower.SetValue( Impinj3.TransmitPowerDefault)
        self.ConnectionTimeoutSeconds.SetValue( Impinj3.ConnectionTimeoutSecondsDefault )
        self.KeepaliveSeconds.SetValue( Impinj3.KeepaliveSecondsDefault )
        self.RepeatSeconds.SetValue( Impinj3.RepeatSecondsDefault )

    def onOK( self, event ):

        global RFControlMaxThroughput
        global RFControlHybrid80k160kRB
        global RFControlDenseReaderM4RB
        global RFControlDenseReaderM8RB
        global RFControlMaxMillerRB

        global ReaderPositionPairs
        global ReaderPositionLinear
        global ReaderPositionNone


        #RFControlMaxThroughput = self.RFControlMaxThroughputRB.GetValue()
        #RFControlHybrid80k160k = self.RFControlHybrid80k160kRB.GetValue()
        #RFControlDenseReaderM4 = self.RFControlDenseReaderM4RB.GetValue()
        #RFControlDenseReaderM8 = self.RFControlDenseReaderM8RB.GetValue()
        #RFControlMaxMiller = self.RFControlMaxMillerRB.GetValue()

        ReaderPositionPairs = self.readerPositionPairsRB.GetValue()
        ReaderPositionLinear = self.readerPositionLinearRB.GetValue()
        ReaderPositionNone = self.readerPositionNoneRB.GetValue()

        Impinj3.TransmitPower= self.TransmitPower.GetValue()
        Impinj3.ConnectionTimeoutSeconds = self.ConnectionTimeoutSeconds.GetValue()
        Impinj3.KeepaliveSeconds = self.KeepaliveSeconds.GetValue()
        Impinj3.RepeatSeconds = self.RepeatSeconds.GetValue()
        self.EndModal( wx.ID_OK )

    def onCancel( self, event ):
        self.EndModal( wx.ID_CANCEL )

class MainWin( wx.Frame ):
    def __init__( self, parent, id = wx.ID_ANY, title='', size=(900,900) ):

        print('MainWin.__init__', file=sys.stderr)
        wx.Frame.__init__(self, parent, id, title, size=size)

        self.config = wx.Config(appName="CrossMgrImpinj4cfg",
                                        vendorName="SmartCyclingSolutions",
                                        style=wx.CONFIG_USE_LOCAL_FILE)

        self.SetBackgroundColour( wx.Colour(232,232,232) )

        self.LightGreen = wx.Colour(153,255,153)
        self.LightOrange = wx.Colour(255,179,71)
        self.LightRed = wx.Colour(255,153,153)


        font = self.GetFont()
        bigFont = wx.Font( font.GetPointSize() * 1, font.GetFamily(), font.GetStyle(), wx.FONTWEIGHT_BOLD )
        # XXX
        # italicFont = wx.Font( bigFont.GetPointSize()*2.2, bigFont.GetFamily(), wx.FONTSTYLE_ITALIC, bigFont.GetWeight() )
        italicFont = wx.Font( bigFont.GetPointSize()*2, bigFont.GetFamily(), wx.FONTSTYLE_ITALIC, bigFont.GetWeight() )

        self.vbs = wx.BoxSizer( wx.VERTICAL )

        bs = wx.BoxSizer( wx.HORIZONTAL  )


        #------------------------------------------------------------------------------------------------
        # Impinj configuration.
        #
        # #############################################################


        # Row:         0         1        2        3                4
        #        ANT        Host        Status        Antennas        Rate
        ANT=0
        HOST=1
        ANTENNAS=2
        STATUS=3
        COUNTER=4
        RATE=COUNTER+2
        USAGE=RATE+2
        QFIT=USAGE+2
        DBS=QFIT+2
        DBA=DBS+2
        OFFSET=DBA+2
        STRAYS=OFFSET+4
        def READER(irow,n):
            return irow+n
        def POS(irow,n,c):
            return (READER(irow,n),c)
        iRow = 0

        # GridBag - 10x2
        #
        #self.gbs = wx.GridBagSizer( 10, 2)
        self.gbs = wx.GridBagSizer( vgap=4, hgap=4)
        self.vbs.Add( self.gbs, flag=wx.EXPAND|wx.ALL, border=4 )

        #------------------------------------------------------------------------------------------------
        # Add strays
        #
        cmcs = wx.BoxSizer( wx.VERTICAL )
        cmcs.Add( wx.StaticLine(self, style=wx.LI_HORIZONTAL), flag=wx.EXPAND|wx.TOP|wx.BOTTOM, border=2 )
        self.strayTagsLabel = wx.StaticText(self, label='Stray Tags:         ')
        cmcs.Add( self.strayTagsLabel, flag=wx.LEFT|wx.RIGHT, border=4 )

        self.strays = wx.ListCtrl( self, style=wx.LC_REPORT|wx.BORDER_SUNKEN, size=(350,150) )
        self.strays.InsertColumn( 0, 'Time', wx.LIST_AUTOSIZE_USEHEADER )
        self.strays.InsertColumn( 1, 'Tag', wx.LIST_AUTOSIZE_USEHEADER )

        cmcs.Add( self.strays, 1, flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, border=4 )
        self.gbs.Add( cmcs, pos=(0, STRAYS), span=(8,9), flag=wx.EXPAND|wx.ALL, border = 4 )


        # #############################################################
        iRow = 1
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, 'Select\nAntennas ') , pos=POS(iRow,0,ANT), span=(1,1), flag=wx.ALIGN_CENTER_HORIZONTAL, border=4 )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, 'LLRP RFID\nReader IP') , pos=POS(iRow,0,HOST), span=(1,1),  flag=wx.ALIGN_CENTER_HORIZONTAL )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, 'Found\nAntennas') , pos=POS(iRow,0,ANTENNAS), span=(1,1),  flag=wx.ALIGN_CENTER_HORIZONTAL )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, 'Connection\nStatus ') , pos=POS(iRow,0,STATUS), span=(1,1),  flag=wx.ALIGN_CENTER_HORIZONTAL )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, '   Tag    \n   Reads') , pos=POS(iRow,0,COUNTER), span=(1,2), flag=wx.ALIGN_RIGHT )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, '   Rate   \n    t/s') , pos=POS(iRow,0,RATE), span=(1,2),  flag=wx.ALIGN_RIGHT )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, '  Usage   \n    %') , pos=POS(iRow,0,USAGE), span=(1,2), flag=wx.ALIGN_RIGHT )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, '   QFit   \n    %') , pos=POS(iRow,0,QFIT), span=(1,2), flag=wx.ALIGN_RIGHT )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, '  RSSI(s) \n    dB') , pos=POS(iRow,0,DBS), span=(1,2), flag=wx.ALIGN_RIGHT )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, '  RSSI(a) \n    dB') , pos=POS(iRow,0,DBA), span=(1,2), flag=wx.ALIGN_RIGHT )
        self.gbs.Add( wx.StaticText(self, wx.ID_ANY, '  OffSet  \n    S') , pos=POS(iRow,0,OFFSET), span=(1,2), flag=wx.ALIGN_RIGHT )


        # Four Impinj Reader configurations
        # common reader init

        self.impinjStatusLabel = [wx.StaticText(self, wx.ID_ANY, '' ) for i in range(4)]
        [self.impinjStatusLabel[i].SetLabel( '-' ) for i in range(4)]

        self.impinjCounterDisplay = [wx.StaticText(self, wx.ID_ANY, '-0-' ) for i in range(4)]
        self.impinjRateDisplay = [wx.StaticText(self, wx.ID_ANY, '-0-' ) for i in range(4)]
        self.impinjAntennaLabel = [wx.StaticText(self, wx.ID_ANY, '' ) for i in range(4)]
        self.impinjUsageDisplay = [wx.StaticText(self, wx.ID_ANY, '0%' ) for i in range(4)]
        self.impinjOffSetDisplay = [wx.StaticText(self, wx.ID_ANY, '  0.0' ) for i in range(4)]
        self.impinjQFitDisplay = [wx.StaticText(self, wx.ID_ANY, '0%' ) for i in range(4)]
        self.impinjDBSDisplay = [wx.StaticText(self, wx.ID_ANY, '-' ) for i in range(4)]
        self.impinjDBADisplay = [wx.StaticText(self, wx.ID_ANY, '-' ) for i in range(4)]
        #self.impinjHost = [masked.IpAddrCtrl( self, wx.ID_ANY, style = wx.TE_PROCESS_TAB ) for i in range(4)]
        self.impinjHost =    [wx.TextCtrl( self, wx.ID_ANY, style = wx.TE_PROCESS_TAB, size=(130,-1) ) for i in range(4)]
        self.antennas = [[] for i in range(4)]

        for r in range(4):
            gs = wx.GridSizer( rows=0, cols=5, vgap=2, hgap=0 )
            gs.Add( wx.StaticText(self, wx.ID_ANY, '%d: ' % (r+1)), flag=wx.ALIGN_CENTER_VERTICAL )
            for i in range(4):
                cb = wx.CheckBox( self, wx.ID_ANY, '')
                if i < 2:
                    cb.SetValue( True )
                cb.Bind( wx.EVT_CHECKBOX, lambda x: self.getAntennaStr(0) )
                gs.Add( cb, flag=wx.ALIGN_CENTER )
                self.antennas[r].append( cb )
            self.gbs.Add( gs, pos=POS(iRow,r+1,ANT), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )

            self.gbs.Add( self.impinjHost[r], pos=POS(iRow,r+1,HOST), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )

            self.gbs.Add( self.impinjStatusLabel[r], pos=POS(iRow,r+1,STATUS), span=(1,1), flag=wx.ALIGN_LEFT )
            self.gbs.Add( self.impinjAntennaLabel[r], pos=POS(iRow,r+1,ANTENNAS), span=(1,1), flag=wx.ALIGN_LEFT )

            self.gbs.Add( self.impinjCounterDisplay[r], pos=POS(iRow,r+1,COUNTER), span=(1,1), flag=wx.ALIGN_CENTER_HORIZONTAL )
            self.gbs.Add( self.impinjRateDisplay[r], pos=POS(iRow,r+1,RATE), span=(1,2), flag=wx.ALIGN_CENTER_HORIZONTAL )
            self.gbs.Add( self.impinjUsageDisplay[r], pos=POS(iRow,r+1,USAGE), span=(1,2), flag=wx.ALIGN_CENTER_HORIZONTAL )
            self.gbs.Add( self.impinjQFitDisplay[r], pos=POS(iRow,r+1,QFIT), span=(1,2), flag=wx.ALIGN_CENTER_HORIZONTAL )
            self.gbs.Add( self.impinjDBSDisplay[r], pos=POS(iRow,r+1,DBS), span=(1,2),  flag=wx.ALIGN_CENTER_HORIZONTAL )
            self.gbs.Add( self.impinjDBADisplay[r], pos=POS(iRow,r+1,DBA), span=(1,2),  flag=wx.ALIGN_CENTER_HORIZONTAL )
            self.gbs.Add( self.impinjOffSetDisplay[r], pos=POS(iRow,r+1,OFFSET), span=(1,2), flag=wx.ALIGN_CENTER_HORIZONTAL )

        #------------------------------------------------------------------------------------------------
        # Advanced and info

        self.advancedButton = wx.Button(self, wx.ID_ANY, 'Reset...' )
        self.advancedButton.Bind( wx.EVT_BUTTON, self.doReset )
        self.gbs.Add( self.advancedButton, pos=POS(iRow,5,0), flag=wx.LEFT, border = 6 )

        self.advancedButton = wx.Button(self, wx.ID_ANY, 'Quit...' )
        self.advancedButton.Bind( wx.EVT_BUTTON, self.doQuit )
        self.gbs.Add( self.advancedButton, pos=POS(iRow,5,1), flag=wx.LEFT, border = 6 )

        self.advancedButton = wx.Button(self, wx.ID_ANY, 'Advanced...' )
        self.advancedButton.Bind( wx.EVT_BUTTON, self.doAdvanced )
        self.gbs.Add( self.advancedButton, pos=POS(iRow,5,2), flag=wx.LEFT, border = 6 )

        #bitmap = wx.BitmapFromXPMData( clipboard_xpm )
        #self.copyToClipboard = wx.BitmapButton( self, wx.ID_ANY, bitmap )
        #self.copyToClipboard = wx.BitmapButton( self, wx.ID_ANY)

        self.copyToClipboard = wx.Button(self, wx.ID_ANY, 'Clipboard' )
        self.copyToClipboard.SetToolTip(wx.ToolTip('Copy Configuration and Logs to Clipboard...'))
        self.copyToClipboard.Bind( wx.EVT_BUTTON, self.doCopyToClipboard )
        self.gbs.Add( self.copyToClipboard, pos=POS(iRow,5,3), border = 8, flag = wx.LEFT )
        self.tStart = datetime.datetime.now()
        self.heartbeat = 0

        #self.resetInfo = wx.StaticText(self, wx.ID_ANY, '--' )
        #self.gbs.Add( self.resetInfo, border = 20, pos=POS(iRow,5,3), flag=wx.LEFT|wx.ALIGN_CENTER_VERTICAL )

        # Backup file name

        #self.gbs.Add( wx.StaticText(self, wx.ID_ANY, 'Backup File:'), pos=(3,0), span=(1,1), flag=wx.ALIGN_RIGHT )
        #self.backupFile = wx.StaticText( self, wx.ID_ANY, '' )
        #self.gbs.Add( self.backupFile, pos=(3,1), span=(1,1), flag=wx.ALIGN_LEFT )

        #------------------------------------------------------------------------------------------------
        # CrossMgr configuration.
        #
        self.gbs = wx.GridBagSizer( 9, 5 )
        self.vbs.Add( self.gbs, flag=wx.EXPAND|wx.ALL, border = 4 )


        # common crossmgr host init
        self.crossMgrHost = [wx.TextCtrl( self, wx.ID_ANY, style = wx.TE_PROCESS_TAB, size=(130,-1) ) for i in range(3)]
        #self.impinjMessagesText = wx.TextCtrl( self, wx.ID_ANY, style=wx.TE_READONLY|wx.TE_MULTILINE|wx.HSCROLL, size=(-1,800) )
        self.hostEnable = [wx.CheckBox( self, wx.ID_ANY, '') for i in range(3)]
        self.crossMgrStatusLabel = [wx.StaticText(self, wx.ID_ANY, '-' ) for i in range(3)]

            #self.impinjStatusLabel[i].SetLabel( '%s' % (self.impinjStatus[i][0].name) )
            #self.impinjCounterDisplay[i].SetLabel( '%6d' % (self.impinjCounter[i][0]) )
            #self.impinjRateDisplay[i].SetLabel( '%4d' % (self.impinjRate[i][0]) )
            #self.impinjAntennaLabel[i].SetLabel( '%s' % (self.impinjAntenna[i][0]) )

        hb = wx.BoxSizer( wx.HORIZONTAL )
        hb.Add( wx.StaticText( self, wx.ID_ANY, 'CrossMgr Hosts:' ), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT )
        self.gbs.Add( hb,                          pos=(0,0), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT )
        for i in range(3):
            hb = wx.BoxSizer( wx.HORIZONTAL )
            hb.Add( self.crossMgrHost[i], flag=wx.ALIGN_LEFT )
            #hb.Add( wx.StaticText( self, wx.ID_ANY, ' : 53135' ), flag=wx.ALIGN_CENTER_VERTICAL )
            self.gbs.Add( self.hostEnable[i],          pos=(0,2+i*8), span=(1,1), flag=wx.ALIGN_CENTER_VERTICAL )
            self.gbs.Add( hb,                          pos=(0,3+i*8), span=(1,1), flag=wx.ALIGN_LEFT )
            self.gbs.Add( self.crossMgrStatusLabel[i], pos=(0,4+i*8), span=(1,1), flag=wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )

        #------------------------------------------------------------------------------------------------
        # Add messages
        fgs = wx.FlexGridSizer( rows = 1, cols = 2, vgap = 4, hgap = 4 )
        fgs.AddGrowableCol( 0 )
        fgs.AddGrowableCol( 1 )
        fgs.SetFlexibleDirection( wx.BOTH )

        self.vbs.Add( fgs, flag=wx.EXPAND, proportion=5 )


        self.impinjMessagesText = wx.TextCtrl( self, wx.ID_ANY, style=wx.TE_READONLY|wx.TE_MULTILINE|wx.HSCROLL, size=(-1,800) )
        fgs.Add( self.impinjMessagesText, flag=wx.EXPAND, proportion=2 )
        self.impinjMessages = MessageManager( self.impinjMessagesText )
        self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

        self.crossMgrMessagesText = wx.TextCtrl( self, wx.ID_ANY, style=wx.TE_READONLY|wx.TE_MULTILINE|wx.HSCROLL, size=(-1,800) )
        fgs.Add( self.crossMgrMessagesText, flag=wx.EXPAND, proportion=2 )
        self.crossMgrMessages = MessageManager( self.crossMgrMessagesText )
        self.crossMgrMessages.messageList.SetBackgroundColour(self.LightGreen )

        self.fgs = fgs


        [self.impinjStatusLabel[i].SetLabel( '1' ) for i in range(4)]


        #------------------------------------------------------------------------------------------------
        # Create a timer to update the messages.
        #
        #self.timer = wx.Timer()
        #self.timer.Bind( wx.EVT_TIMER, self.updateMessages )
        #self.timer.Start( 1000, False )
        wx.CallAfter(self.startTimer, None)

        self.Bind(wx.EVT_CLOSE, self.onCloseWindow)

        self.readOptions()

        self.SetSizer( self.vbs )

        print('__init__: ReaderPositionPairs: %s' % ReaderPositionPairs, file=sys.stderr)
        print('__init__: ReaderPositionLinear: %s' % ReaderPositionLinear, file=sys.stderr)
        print('__init__: ReaderPositionNone: %s' % ReaderPositionNone, file=sys.stderr)

        print('MainWin.__init__ calling start', file=sys.stderr)
        self.start()
        [self.impinjStatusLabel[i].SetLabel( '2' ) for i in range(4)]
        print('MainWin.__init__ finished **************************', file=sys.stderr)

    def startTimer(self, event):
        wx.CallLater(1000, self.updateMessages, event)


    def readerStatusCB( self, **kwargs ):
        # As this is called from another thread, make, make sure all UI updates are done from CallAfter.
        print('readerStatsCB: kwargs: %s' % (kwargs), file=sys.stderr)
        connectedAntennas = set(kwargs.get( 'connectedAntennas', [] ))
        #for i in range(4):
        #        if self.antennas[i].GetValue():
        #                c = self.antennaConnectedChar if (i+1) in connectedAntennas else self.antennaUnconnectedChar
        #        else:
        #                c = self.antennaInactiveChar
        #        wx.CallAfter( self.antennaConnected[i].SetLabel, c)
        #        wx.CallAfter( self.connected.Enable, True )


    def refreshStrays( self, strays ):
        #print('refreshStrays: %s' % strays, file=sys.stderr)
        if self.strays.GetItemCount() != len(strays):
            self.strayTagsLabel.SetLabel( 'Stray Tags: {}'.format(len(strays)) )
        if not strays:
            if self.strays.GetItemCount():
                self.strays.DeleteAllItems()
            return

        if ( self.strays.GetItemCount() == len(strays) and
                strays[0][0] == self.strays.GetItemText(0) and strays[-1][0] == self.strays.GetItemText(self.strays.GetItemCount()-1)):
            return

        self.strays.DeleteAllItems()
        for xstray in strays:
            tag = xstray[0]
            discovered = xstray[1]
            try:
                # this is the modern API method, but causes exception linux, 2019-03-27 sl
                #
                i = self.strays.InsertItem( 1000000, discovered.strftime('%H:%M:%S'))
                self.strays.SetItem( i, 1, tag)
                #print "Mainwin:refreshStrays InsertItem ok exception, file=sys.stderr"
            except:
                try:
                    # this is the deprecated way, but works under linux, 2019-03-27 sl
                    #
                    i = self.strays.InsertStringItem( 10000, discovered.strftime('%H:%M:%S'))
                    self.strays.SetStringItem( i, 1, tag)
                    #print "Mainwin:refreshStrays InsertStringItem ok, file=sys.stderr"
                except:
                    print("Mainwin:refreshStrays ignore exception", file=sys.stderr)

        for c in range(self.strays.GetColumnCount()):
            self.strays.SetColumnWidth( c, wx.LIST_AUTOSIZE_USEHEADER )

    class RStat(object):
        def __init__(self, count, alg, dbtot):
            self.count = 0
            self.qfit = 0
            self.dbtot = 0
            self.add(count, alg, dbtot)
        def add(self, count, qfit, dbtot):
            self.count += count
            self.qfit += qfit
            self.dbtot += dbtot
        def __repr__(self):
            return '[RStat: count: %s qfit: %s dbtot: %s]' % ( self.count, self.qfit, self.dbtot)

    def refreshStats( self, stats ):
        #print('refreshStats: %s' % stats, file=sys.stderr)
        readerStats = dict()
        totalCount = 0
        # XXX
        #for k, s in stats.iteritems():
        for k, s in six.iteritems(stats):
            r, e = k.split("-")
            #print('refreshStats[%s]: %s' % (r, s), file=sys.stderr)
            totalCount += s.count
            if not r in readerStats:
                readerStats[r] = self.RStat(s.count, s.qfit, s.dbtot)
            else:
                readerStats[r].add(s.count, s.qfit, s.dbtot)

        # XXX should be aggregated antennas
        # XXX
        #for k, r in readerStats.iteritems():
        for k, r in six.iteritems(readerStats):
            up = (float(r.count) / float(totalCount))*100 if totalCount > 0 else 0
            qp = (float(r.qfit) / float(r.count))*100 if r.count  > 0 else 0
            db = (float(r.dbtot) / float(r.count))/10.0 if r.count  > 0 else 0
            #print('refreshStats[%s]: tc: %d rc: %d qf: %d up: %6.2f qp: %6.2f db: %5.2f' % (k, totalCount, r.count, r.qfit, up, qp, db), file=sys.stderr)
            i = (int(k) // 10) - 1
            self.impinjUsageDisplay[i].SetLabel( '%3.0f%%' % up)
            self.impinjQFitDisplay[i].SetLabel( '%3.0f%%' % qp)
            self.impinjDBSDisplay[i].SetLabel( '%3.0f' % db)

    def refreshReaderStats( self, offsets ):
        #print('refreshReaderStats: %s' % offsets, file=sys.stderr)
        readerStats = dict()
        totalCount = 0
        # XXX
        #for k, s in offsets.iteritems():
        for k, s in six.iteritems(offsets):
            o, d = s
            #print('refreshReaderStats[%s]: offset:%s db: %s type(db): %s' % (k, o, d, type(d)), file=sys.stderr)
            d = d / 10.0
            i = k // 10
            i = (k //10) - 1
            self.impinjOffSetDisplay[i].SetLabel( ' %5.3f' % float(o))
            self.impinjDBADisplay[i].SetLabel( '%3.0f' % d)


    def statHandler( self, statQ ):
        #print('statHandler: started', file=sys.stderr)
        while 1:
            #print('statHandler: waiting on get', file=sys.stderr)
            msg = statQ.get()
            #print('statHandler: %s' % msg[0], file=sys.stderr)

            if msg[0] == 'stats':
                #print('statHandler: stats:  %s' % msg[1], file=sys.stderr)
                wx.CallAfter( self.refreshStats, msg[1] )

            elif msg[0] == 'readerStats':
                #print('statHandler: offsets: %s' % msg[1], file=sys.stderr)
                wx.CallAfter( self.refreshReaderStats, msg[1] )

            elif msg[0] == 'strays':
                #print('statHandler: strays: %s' % msg[1], file=sys.stderr)
                wx.CallAfter( self.refreshStrays, msg[1] )

            elif msg[0] == 'shutdown':
                break

    def start( self ):


        print('MainWin.start entered', file=sys.stderr)
        self.messageQ = Queue()
        #self.shutdownTQ = Queue()        # Queue to tell the Impinj monitor to shut down.

        #self.shutdownCQ = [Queue() for i in range(3)]
        self.shutdownEvent = Event()
        self.shutdownEvent.clear()
        #self.shutdownIQ = [Queue() for i in range(4)]

        self.messageQ.put('MainWin.start TEST');

        self. impinjCounter = [[0,0,0,0,0] for i in range(4)];
        self. impinjRate = [[0,0,0,0,0] for i in range(4)];
        self. impinjAntenna = [[[]] for i in range(4)];

        self.impinjStatus = [[ConnectionStatus.Disabled] for i in range(4)]
        self.crossMgrStatus = [[ConnectionStatus.Disabled] for i in range(3)]


        # stray handler
        self.statQ = Queue()
        self.strayProcess = Process( name='StrayProcess', target=self.statHandler, args=(self.statQ,) )
        self.strayProcess.daemon = True
        self.strayProcess.start()


        # start up to three CrossMgr processes
        # this also creates the three dataQ's that the Intermediate TagServer will use
        #
        # Common init

        self.dataQ = [None for i in range(3)]
        self.crossMgrProcess = [None for i in range(3)]


        for i in range(3):
            if self.hostEnable[i].GetValue() :
                self.dataQ[i] = Queue()
                self.crossMgrProcess[i] = Process( name='CrossMgrProcess%d' % i, target=CrossMgrServer,
                                args=(self.dataQ[i], self.messageQ, self.shutdownEvent, self.getCrossMgrHost(i),
                                      CrossMgrPort[i], self.crossMgrStatus[i], f'Impinj4-{i}', ) )
                self.crossMgrProcess[i].daemon = True
                self.crossMgrProcess[i].start()
            else:
                self.dataQ[i] = None
                self.crossMgrProcess[i] = None

        # start the intermediate TagServer process
        # This will also create the dataQ that the three Impinj processes will use
        #
        self.messageQ.put('start TEST2');
        readerPositioning = 'Pairs'
        print('MainWin.start: ReaderPositionPairs: %s' % ReaderPositionPairs, file=sys.stderr)
        print('MainWin.start: ReaderPositionLinear: %s' % ReaderPositionLinear, file=sys.stderr)
        print('MainWin.start: ReaderPositionNone: %s' % ReaderPositionNone, file=sys.stderr)



        if ReaderPositionPairs:
            readerPriority = ReaderPriority.pairs
        elif ReaderPositionLinear:
            readerPriority = ReaderPriority.linear
        elif ReaderPositionNone:
            readerPriority = ReaderPriority.none

        print('MainWin.start: readerPriority: %s' % readerPriority, file=sys.stderr)
        self.dataQ0 = Queue()
        self.tagServerProcess = Process( name='TagServer', target=TagServer,
                        args=(self.statQ, self.dataQ0, self.dataQ, self.messageQ, self.shutdownEvent, Impinj3.RepeatSeconds, readerPriority ) )
        self.tagServerProcess.daemon = True
        self.tagServerProcess.start()

        self.messageQ.put('MainWin.start TEST3');

        # start four Impinj reader processes
        # common init
        self.impinjProcess = [None for i in range(4)]
        session = (2,3,2,3)
        for i in range(4):
            if self.testAntenna(i) :
                print('MainWin.start: starting reader: %d' % (i + 1), file=sys.stderr)
                # Session - 2 or 3, ModeIndex - 0 - Max Throughput, SearchMode - 1 - Single Target
                self.impinjProcess[i] = Process( name='ImpinjProcess%d' %i, target=ImpinjServer,
                                args=(i+1,
                                        self.dataQ0, self.messageQ,
                                        self.shutdownEvent, self.impinjHost[i].GetValue(), ImpinjInboundPort,
                                        self.getAntennaStr(i), session[i], Impinj3.ImpinjRFControlMode,
                                        self.impinjCounter[i], self.impinjRate[i],
                                        self.impinjStatus[i],
                                        self.impinjAntenna[i],
                                        self.readerStatusCB,) )

                self.impinjProcess[i].daemon = True
                self.impinjProcess[i].start()
            else:
                print('MainWin.start: not starting reader: %d' % (i + 1), file=sys.stderr)
                self.impinjProcess[i] = None

        print('MainWin.start finished', file=sys.stderr)


    def shutdown( self ):
        print('MainWin3: shutdown!!', file=sys.stderr)
        self.impinjProcess = None
        self.crossMgrProcess = None
        self.tagServerProcess = None
        self.messageQ = None
        self.dataQ0 = None
        self.dataQ = None
        self.shutdownQ = None
        #self.shutdownCQ = None
        #for i in range(4):
        #    self.shutdownIQ[i] = None

    def doQuit( self, event, confirm = True ):

        if confirm:
            dlg = wx.MessageDialog(self, 'Quit Impinj4?',
               'Confirm Reset', wx.OK | wx.CANCEL | wx.ICON_WARNING )
            ret = dlg.ShowModal()
            dlg.Destroy()
            if ret != wx.ID_OK:                                                                                       
                return
        #self.reset.Enable( False )                # Prevent multiple clicks while shutting down.
        print('doReset: ReaderPositionPairs: %s' % ReaderPositionPairs, file=sys.stderr)
        print('doReset: ReaderPositionLinear: %s' % ReaderPositionLinear, file=sys.stderr)
        print('doReset: ReaderPositionNone: %s' % ReaderPositionNone, file=sys.stderr)
        self.writeOptions()

        # self.gracefulShutdown()
        wx.CallAfter(partial(self.gracefulShutdown, wxExit=True, restart=True))

        return

    def doReset( self, event, confirm = True ):

        #self.reset.Enable( False )                # Prevent multiple clicks while shutting down.
        print('doReset: ReaderPositionPairs: %s' % ReaderPositionPairs, file=sys.stderr)
        print('doReset: ReaderPositionLinear: %s' % ReaderPositionLinear, file=sys.stderr)
        print('doReset: ReaderPositionNone: %s' % ReaderPositionNone, file=sys.stderr)
        self.writeOptions()

        # self.gracefulShutdown()
        wx.CallAfter(partial(self.gracefulShutdown, wxExit=False, restart=True))

        return

    def doAdvanced( self, event ):
        dlg = AdvancedSetup( self )
        dlg.ShowModal()
        dlg.Destroy()

    #def gracefulShutdownReset(self,):
    #        self.grace
    def gracefulShutdown( self, wxExit=False, restart=False ):
        # Shutdown the CrossMgr process by sending it a shutdown command.

        print('************************************************************************************************************************', file=sys.stderr)
        print('gracefulshutdown: entered', file=sys.stderr)
        self.shutdownEvent.set()
        print('gracefulshutdown: %s' % (self.shutdownEvent.is_set()), file=sys.stderr)

        # kill tag server process
        if self.statQ:
            self.statQ.put(('shutdown',))
        if self.dataQ0:
            print('gracefulshutdown: shutdownDQ0', file=sys.stderr)
            self.dataQ0.put( 'shutdown' )
            self.dataQ0.put( 'shutdown' )
            self.dataQ0.put( 'shutdown' )
        if self.tagServerProcess is not None:
            print('gracefulshutdown: join tagServerProcess', file=sys.stderr)
            self.tagServerProcess.join()


        # kill the three crossmgr processes
        for i in range(3):
            if self.dataQ[i]:
                print('gracefulshutdown: shutdownDQ[%d]' % i, file=sys.stderr)
                self.dataQ[i].put( 'shutdown' )
                self.dataQ[i].put( 'shutdown' )
                self.dataQ[i].put( 'shutdown' )
            if self.crossMgrProcess[i] is not None:
                print('gracefulshutdown: join crossMgrProcess[%d]' % i, file=sys.stderr)
                self.crossMgrProcess[i].join()

        # kill the four impinj reader processes
        for i in range(4):
            if self.impinjProcess[i] is not None:
                print('gracefulshutdown: join impinjProcess[%d]' % i, file=sys.stderr)
                self.impinjProcess[i].join()

        self.crossMgrProcess = None
        self.tagServerProcess = None
        # XXX
        # calling wx.Exit() here causes wx to throw exception
        if wxExit:
            time.sleep(2)         
            wx.CallAfter(self.exitLater)

        if restart:
            time.sleep(2)         
            wx.CallAfter(self.startLater)

    def startLater(self):
        print('startLater', file=sys.stderr)
        self.impinjMessages.clear()
        self.crossMgrMessages.clear()
        self.shutdown()
        #self.reset.Enable( True )
        wx.CallAfter( self.start )

    def exitLater(self):
        print('exitLater', file=sys.stderr)
        wx.Exit()

    def onCloseWindow( self, event ):
        print('onCloseWindow', file=sys.stderr)
        # XXX
        # calling wx.graceFulShutdown() here causes wx to throw exception
        wx.CallAfter(partial(self.gracefulShutdown, wxExit=True, restart=False))

    def doCopyToClipboard( self, event ):
        print('doCopyToClipBoard', file=sys.stderr)
        cc = [
                'Configuration: CrossMgrImpinj4',
                #'    RunningTime:   {}'.format(self.runningTime.GetLabel()),
                #'    Time:          {}'.format(self.time.GetLabel()),
                #'    BackupFile:    {}'.format(self.backupFile.GetLabel()),
                '',
                'Configuration: Impinj:',
                '    ImpinjHost[0]:   {}'.format(self.impinjHost[0].GetValue()),
                '    ImpinjHost[1]:   {}'.format(self.impinjHost[1].GetValue()),
                '    ImpinjHost[2]:   {}'.format(self.impinjHost[2].GetValue()),
                '    ImpinjHost[3]:   {}'.format(self.impinjHost[3].GetValue()),

                '    ImpinjPort:    {}'.format(ImpinjInboundPort),
                ''
                '    ImpinjRFControlMode: {}'.format(Impinj3.ImpinjRFControlMode),
                '    readerPositionPairs: {}'.format(ReaderPositionPairs),
                '    readerPositionLinear: {}'.format(ReaderPositionLinear),
                '    readerPositionNone: {}'.format(ReaderPositionNone),
                '    TransmitPower: {}'.format(Impinj3.TransmitPower),
                '    ConnectionTimeoutSeconds: {}'.format(Impinj3.ConnectionTimeoutSeconds),
                '    KeepaliveSeconds:         {}'.format(Impinj3.KeepaliveSeconds),
                '    RepeatSeconds:            {}'.format(Impinj3.RepeatSeconds),
                '',
                'Configuration: CrossMgr',
                '    CrossMgrHost[0]:  {}'.format(self.getCrossMgrHost(0)),
                '    CrossMgrPort[0]:  {}'.format(CrossMgrPort[0]),
                '    CrossMgrHost[1]:  {}'.format(self.getCrossMgrHost(1)),
                '    CrossMgrPort[1]:  {}'.format(CrossMgrPort[1]),
                '    CrossMgrHost[2]:  {}'.format(self.getCrossMgrHost(2)),
                '    CrossMgrPort[2]:  {}'.format(CrossMgrPort[2]),
        ]
        cc.append( '\nLog: Impinj' )
        log = self.impinjMessagesText.GetValue()
        cc.extend( ['    ' + line for line in log.split('\n')] )

        cc.append( '\nLog: CrossMgr' )
        log = self.crossMgrMessagesText.GetValue()
        cc.extend( ['    ' + line for line in log.split('\n')] )

        cc.append( '\nLog: Application\n' )
        try:
            with open(redirectFileName, 'r') as fp:
                for line in fp:
                    cc.append( line )
        except:
            pass

        print('doCopyToClipBoard: %s' % cc, file=sys.stderr)

        if wx.TheClipboard.Open():
            do = wx.TextDataObject()
            do.SetText( '\n'.join(cc) )
            wx.TheClipboard.SetData(do)
            wx.TheClipboard.Close()
            dlg = wx.MessageDialog(self, 'Configuration and Logs copied to the Clipboard.',
                                                            'Copy to Clipboard Succeeded',
                                                            wx.OK | wx.ICON_INFORMATION )
            ret = dlg.ShowModal()
            dlg.Destroy()
        else:
            # oops... something went wrong!
            wx.MessageBox("Unable to open the clipboard", "Error")

    def getCrossMgrHost( self, i):
        return self.crossMgrHost[i].GetValue()


    def getAntennaStr( self, a ):
        s = []
        for i in range(4):
            if self.antennas[a][i].GetValue():
                s.append( '%d' % (i + 1) )
        return ' '.join( s )

    def setAntennaStr( self, a, s ):
        antennas = set( int(i) for i in s.split() )
        for i in range(4):
            self.antennas[a][i].SetValue( (i+1) in antennas )

    def testAntenna( self, a ):
        return any(v.GetValue() > 0 for v in self.antennas[a])

    def writeOptions( self ):
        print('writeOptions', file=sys.stderr)
        self.config.Write( 'CrossMgrHost1', self.getCrossMgrHost(0) )
        self.config.Write( 'CrossMgrHost2', self.getCrossMgrHost(1) )
        self.config.Write( 'CrossMgrHost3', self.getCrossMgrHost(2) )

        self.config.Write( 'HostEnable1', 'True' if self.hostEnable[0].GetValue() else 'False' )
        self.config.Write( 'HostEnable2', 'True' if self.hostEnable[1].GetValue() else 'False' )
        self.config.Write( 'HostEnable3', 'True' if self.hostEnable[2].GetValue() else 'False' )

        self.config.Write( 'ImpinjAddr1', self.impinjHost[0].GetValue() )
        self.config.Write( 'ImpinjAddr2', self.impinjHost[1].GetValue() )
        self.config.Write( 'ImpinjAddr3', self.impinjHost[2].GetValue() )
        self.config.Write( 'ImpinjAddr4', self.impinjHost[3].GetValue() )

        self.config.Write( 'ImpinjPort', '{}'.format(ImpinjInboundPort) )
        self.config.Write( 'Antennas1', self.getAntennaStr(0) )
        self.config.Write( 'Antennas2', self.getAntennaStr(1) )
        self.config.Write( 'Antennas3', self.getAntennaStr(2) )
        self.config.Write( 'Antennas4', self.getAntennaStr(3) )

        self.config.Write( 'ImpinjRFControlMode', '{}'.format(Impinj3.ImpinjRFControlMode) )

        self.config.Write( 'readerPositionPairs', '{}'.format(ReaderPositionPairs) )
        self.config.Write( 'readerPositionLinear', '{}'.format(ReaderPositionLinear) )
        self.config.Write( 'readerPositionNone', '{}'.format(ReaderPositionNone) )
        self.config.Write( 'TransmitPower', '{}'.format(Impinj3.TransmitPower) )
        self.config.Write( 'ConnectionTimeoutSeconds', '{}'.format(Impinj3.ConnectionTimeoutSeconds) )
        self.config.Write( 'KeepaliveSeconds', '{}'.format(Impinj3.KeepaliveSeconds) )
        self.config.Write( 'RepeatSeconds', '{}'.format(Impinj3.RepeatSeconds) )
        self.config.Flush()

    def readOptions( self ):

        global ReaderPositionPairs
        global ReaderPositionLinear
        global ReaderPositionNone

        print('readOptions', file=sys.stderr)
        self.crossMgrHost[0].SetValue(self.config.Read('CrossMgrHost1', Utils.DEFAULT_HOST))
        self.crossMgrHost[1].SetValue(self.config.Read('CrossMgrHost2', Utils.DEFAULT_HOST))
        self.crossMgrHost[2].SetValue(self.config.Read('CrossMgrHost3', Utils.DEFAULT_HOST))

        hostEnable = [None,None,None]
        hostEnable[0] = (self.config.Read('HostEnable1', 'False').upper()[:1] == 'T')
        hostEnable[1] = (self.config.Read('HostEnable2', 'False').upper()[:1] == 'T')
        hostEnable[2] = (self.config.Read('HostEnable3', 'False').upper()[:1] == 'T')

        self.hostEnable[0].SetValue( hostEnable[0] )
        self.hostEnable[1].SetValue( hostEnable[1] )
        self.hostEnable[2].SetValue( hostEnable[2] )


        self.impinjHost[0].SetValue( self.config.Read('ImpinjAddr1', '0.0.0.0') )
        self.impinjHost[1].SetValue( self.config.Read('ImpinjAddr2', '0.0.0.0') )
        self.impinjHost[2].SetValue( self.config.Read('ImpinjAddr3', '0.0.0.0') )
        self.impinjHost[3].SetValue( self.config.Read('ImpinjAddr4', '0.0.0.0') )

        self.setAntennaStr(0, self.config.Read('Antennas1', '1 2 3 4') )
        self.setAntennaStr(1, self.config.Read('Antennas2', '1 2 3 4') )
        self.setAntennaStr(2, self.config.Read('Antennas3', '1 2 3 4') )
        self.setAntennaStr(3, self.config.Read('Antennas4', '1 2 3 4') )

        ReaderPositionPairs = self.config.Read( 'ReaderPositionPairs', 'True').upper()[:1] == 'T'

        ReaderPositionPairs = self.config.Read( 'ReaderPositionPairs', 'True').upper()[:1] == 'T'
        ReaderPositionLinear = self.config.Read( 'ReaderPositionLinear', 'False').upper()[:1] == 'T'
        ReaderPositionNone = self.config.Read( 'ReaderPositionNone', 'False').upper()[:1] == 'T'

        Impinj3.ImpinjRFControlMode = int(self.config.Read( 'ImpinjRFControlMode', '{}'.format(Impinj3.ImpinjRFControlMode)))
        if Impinj3.ImpinjRFControlMode < 0:
            Impinj3.ImpinjRFControlMode = 0


        Impinj3.TransmitPower = int(self.config.Read( 'TransmitPower', '{}'.format(Impinj3.TransmitPower)))
        Impinj3.ConnectionTimeoutSeconds = int(self.config.Read( 'ConnectionTimeoutSeconds', '{}'.format(Impinj3.ConnectionTimeoutSeconds)))
        Impinj3.KeepaliveSeconds = int(self.config.Read( 'KeepaliveSeconds', '{}'.format(Impinj3.KeepaliveSeconds)))
        Impinj3.RepeatSeconds = int(self.config.Read( 'RepeatSeconds', '{}'.format(Impinj3.RepeatSeconds)))



    def updateHeartBeat( self, heartbeat):

        tNow = datetime.datetime.now()
        running = int((tNow - self.tStart).total_seconds())
        s1 = self.tStart.strftime('%H:%M:%S')

        s2 = tNow.strftime('%H:%M:%S')

        # XXX this throws an exception for either of the strftime calls, only the first time through
        #
        try:
            #s = ''
            #if not heartbeat:
            #        s = '%d' % heartbeat

            #self.resetInfo.SetLabel('Last Reset: %s %2d:%02d:%02d %d' % (
            self.resetInfo.SetLabel('Last Reset: %s %2d:%02d:%02d %s %d' % (
                    s1,
                    running // (60*60),
                    (running // 60) % 60,
                    running % 60,
                    s2,
                    heartbeat
                    ))
            print('updateHeartBeat: OK', file=sys.stderr)
        except:
            print('updateHeartBeat: CAUGHT', file=sys.stderr)
            pass


    def _updateMessages( self, event ):
        wx.CallAfter(self._updateMessages)

        #try:
        #        self._updateMessages(event)
        #except Exception as e:
        #        print('updateMessages: e: %s' % (e), file=sys.stderr)
        #        print(traceback.format_exc(), file=sys.stderr)

    def updateMessages( self, event):

        #print('updateMessages: entered', file=sys.stderr)

        [self.impinjStatusLabel[i].SetLabel( '3' ) for i in range(4)]

        heartbeat = 0
        #self.updateHeartBeat(heartbeat)

        #self.runningTime.SetLabel( '%02d:%02d:%02d' % (running // (60*60), (running // 60) % 60, running % 60) )
        #self.time.SetLabel( tNow.strftime('%H:%M:%S') )

        [self.impinjStatusLabel[i].SetLabel( 'X' ) for i in range(4)]
        mainWin.Show()

        for i in range(4):
            #print('impinjStatus[%d]: %s color: %s' % (i, self.impinjStatus[i][0].name(), self.impinjStatus[i][0].color()))
            self.impinjStatusLabel[i].SetBackgroundColour(self.impinjStatus[i][0].color())
            self.impinjStatusLabel[i].SetLabel( '%s' % (self.impinjStatus[i][0].name()) )
            self.impinjCounterDisplay[i].SetLabel( '%6d' % (self.impinjCounter[i][0]) )
            self.impinjRateDisplay[i].SetLabel( '%4d' % (self.impinjRate[i][0]) )
            self.impinjAntennaLabel[i].SetLabel( '%s' % (self.impinjAntenna[i][0]) )


        for i in range(3):
            self.crossMgrStatusLabel[i].SetBackgroundColour(self.crossMgrStatus[i][0].color())
            self.crossMgrStatusLabel[i].SetLabel( '%s' % (self.crossMgrStatus[i][0].name()) )


        if not self.messageQ:
            print('updateMessages: no messageQ')
            return
        while 1:
            heartbeat += 1
            try:
                d = self.messageQ.get( False )
            except Empty:
                break

            message = ' '.join( str(x) for x in d[1:] )
            if   d[0] == 'Impinj':
                self.impinjMessages.write( message )

            elif   d[0] == 'Impinj-Alert':
                self.impinjMessages.messageList.SetBackgroundColour(self.LightRed )
                self.impinjMessages.write( message )
                self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif   d[0] == 'Impinj-Warning':
                self.impinjMessages.messageList.SetBackgroundColour(self.LightOrange )
                self.impinjMessages.write( message )
                self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'Impinj2Queue':
                self.crossMgrMessages.write( message )

            elif d[0] == 'Impinj2Queue-Alert':
                self.impinjMessages.messageList.SetBackgroundColour(self.LightRed )
                self.crossMgrMessages.write( message )
                self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'Impinj2Queue-Warning':
                self.impinjMessages.messageList.SetBackgroundColour(self.LightOrange )
                self.crossMgrMessages.write( message )
                self.impinjMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'Queue2JChip':
                self.crossMgrMessages.write( message )

            elif d[0] == 'Queue2JChip-Alert':
                self.crossMgrMessages.messageList.SetBackgroundColour(self.LightRed )
                self.crossMgrMessages.write( message )
                self.crossMgrMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'Queue2JChip-Warning':
                self.crossMgrMessages.messageList.SetBackgroundColour(self.LightOrange )
                self.crossMgrMessages.write( message )
                self.crossMgrMessages.messageList.SetBackgroundColour(self.LightGreen )

            elif d[0] == 'BackupFile':
                self.backupFile.SetLabel( os.path.basename(d[1]) )

        #self.updateHeartBeat(heartbeat)
        wx.CallLater(1000, self.updateMessages, None)

def disable_stdout_buffering():
    # XXX dup stderr?
    fileno = sys.stdout.fileno()
    temp_fd = os.dup(fileno)
    sys.stdout.close()
    os.dup2(temp_fd, fileno)
    os.close(temp_fd)
    sys.stdout = os.fdopen(fileno, "w")

redirectFileName = None
mainWin = None
def MainLoop():
    global mainWin, redirectFileName

    try:
        app = wx.App(False)
        app.SetAppName("CrossMgrImpinj4")
    except Exception as e:
        print('MainLoop: e: %s' % (e))
        print(traceback.format_exc())

    #mainWin = MainWin( "x", title=AppVerName, size=(1000,1000) )

    dataDir = Utils.getHomeDir()
    print("dataDir: %s" % (dataDir))

    redirectFileName = os.path.join(dataDir, 'CrossMgrImpinj4.log')

    print("redirectFileName: %s" % (redirectFileName))


    # Set up the log file.  Otherwise, show errors on the screen.
    if __name__ == '__main__':
        print('__main__, disable buffering');
        disable_stdout_buffering()
    else:
        print('Redirect to log');
        try:
            logSize = os.path.getsize( redirectFileName )
            if logSize > 1000000:
                os.remove( redirectFileName )
        except:
            pass

        try:
            app.RedirectStdio( redirectFileName )
        except:
            pass

        try:
            with open(redirectFileName, 'a') as pf:
                pf.write( '********************************************\n' )
                pf.write( '%s: %s Started.\n' % (datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S'), AppVerName) )
        except:
            pass

    mainWin = MainWin( None, title=AppVerName, size=(1000,1000) )
    #mainWin = MainWin( "test", title=AppVerName, size=(1000,1000) )
    mainWin.Show()

    # Set the upper left icon.
    try:
        icon = wx.Icon( os.path.join(getImageFolder(), 'CrossMgrImpinj.ico'), wx.BITMAP_TYPE_ICO )
        mainWin.SetIcon( icon )
    except:
        pass

    # start processing events.
    mainWin.Refresh()
    app.MainLoop()

@atexit.register
def shutdown():
    if mainWin:
        mainWin.shutdown()

from multiprocessing import freeze_support
if __name__ == '__main__':
    freeze_support()
    MainLoop()
