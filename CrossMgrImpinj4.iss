; -- CrossMgrImpinj4.iss --

[Setup]
AppName=CrossMgrImpinj4
#include "Version.py"
DefaultDirName={pf}\CrossMgrImpinj4
DefaultGroupName=CrossMgrImpinj4
UninstallDisplayIcon={app}\CrossMgrImpinj4.exe
Compression=lzma2/ultra64
SolidCompression=yes
SourceDir=dist
OutputDir=..\install
OutputBaseFilename=CrossMgrImpinj4_Setup
ChangesAssociations=yes

[Registry]
Root: HKCR; Subkey: "CrossMgrImpinj4\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\CrossMgrImpinj4.exe,0"
Root: HKCR; Subkey: "CrossMgrImpinj4\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\CrossMgrImpinj4.exe"" ""%1"""

[Tasks] 
Name: "desktopicon"; Description: "Create a &desktop icon"; 
	
[Files]
Source: "*.*"; DestDir: "{app}"; Flags: recursesubdirs

[Icons]
Name: "{group}\CrossMgrImpinj4"; Filename: "{app}\CrossMgrImpinj4.exe"
Name: "{userdesktop}\CrossMgrImpinj4"; Filename: "{app}\CrossMgrImpinj4.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\CrossMgrImpinj4.exe"; Description: "Launch CrossMgrImpinj4"; Flags: nowait postinstall skipifsilent
