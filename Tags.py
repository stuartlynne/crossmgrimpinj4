import re
import math
import os
import sys
import socket
import time
import threading
import datetime
import pprint
import Impinj3
import numpy as np
import enum
import traceback
from enum import Enum
import six
from QuadReg import QuadRegExtreme, QuadRegRemoveOutliersRobust, QuadRegRemoveOutliersRansac, QuadReg

getTimeNow = datetime.datetime.now

def doMsgInternal(messageQ, style, readerID, EPC, count, msg):
    now = getTimeNow()
    messageQ.put( (style, '%s [%d] %s (%03d) %s' % ( now.strftime('%H:%M:%S'), readerID, EPC, count, msg)))

def doMsg (messageQ, readerID, EPC, count, msg): doMsgInternal(messageQ, 'Impinj', readerID, EPC, count, msg)
def doWarning (messageQ, readerID, EPC, count, msg): doMsgInternal(messageQ, 'Impinj-Warning', readerID, EPC, count, msg)




#class TagInfo:
# Track reads for a specific tag on a specific reader/antenna pair.
#
#        firstTime        time of first discovery
#        lastTime        time of most recent read
#        count                the number of reads from this reader
#        stray                set if this tag was stray in FOV
#        gone                   set if this tag was stray and now is gone
#        seenMsgTime        time reported as stray
#        points                array of RSSI values for each read

class TagInfo:

    def __init__(self, time, RSSI, epc):

        self.firstTime = time
        self.lastTime = time
        self.seenMsgTime = datetime.datetime.utcfromtimestamp(0)
        self.count = 0
        self.stray = False
        self.gone = False
        self.points = []
        self.maxRSSI = RSSI
        self.maxTime = time
        self.epc = epc


    # add
    # Add a new read time with RSSI
    def add(self, discoveryTime, RSSI, phaseAngle):

        self.count += 1
        self.lastTime = discoveryTime
        self.gone = False
        if RSSI > self.maxRSSI:
            self.maxRSSI = RSSI
            self.maxTime = discoveryTime

        # ###############
        #
        # special handling for stray tags
        if self.stray:
            return False

        if (self.lastTime - self.firstTime).total_seconds() > 10:
            print('TagInfo.add elapsed: %s > 10 STRAY' %((self.lastTime - self.firstTime).total_seconds()), file=sys.stderr)
            self.stray = True
            self.points = []
            return False


        # Append to rssi and times arrays
        #
        self.points.append((discoveryTime, RSSI, phaseAngle))

        #print('TagInfo.add: firstTime: %s discover: %s count: %d' % (self.firstTime.strftime('%H%M%S.%f'), discoveryTime.strftime('%H%M%S.%f'), self.count), file=sys.stderr)
        return True

    #def elapsedTime(self):
    #        return (getTimeNow() - self.lastTime).total_seconds()


    # return firstTime plus delta seconds
    def addRefTime(self, x):
        return self.firstTime + datetime.timedelta(seconds = x)

    # get x1 as elapsed seconds from firstTime, y1 is rssi
    #
    def getPoints(self):
        x0 = [i[0] for i in self.points]
        y1 = [i[1] for i in self.points]
        return [ (i - self.firstTime).total_seconds() for i in x0], y1, len(self.points)

    def getXY(self):
        return [ (
                float((i[0] - self.firstTime).total_seconds()),
                float(i[1])
                ) for i in self.points]

    # get max read
    def getMaxRSSIRead(self):
        return (self.maxTime - self.firstTime).total_seconds(), self.maxRSSI


    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        #                   1            2          3        4        5        6           7
        return '<firstTime: %s lastTime: %s count: %d stray: %s gone: %s \n\tSeenmsg: %s\n\tpoints: %s>\n' % (
                self.firstTime.strftime('%H%M%S.%f'),        #1
                self.lastTime.strftime('%H%M%S.%f'),        #2
                self.count,                                #3
                self.stray,                                #4
                self.gone,                                #5
                self.seenMsgTime.strftime('%H:%M:%S'),  #8
                #['{},{:.3f},{},{}'.format(i.strftime('%H%M%S.%f'),j,k,l) for i,j,k,l in self.points],

                len(self.points),
                #["(%s,%3.0f,%3.0f)" % (i.strftime('%H%M%S.%f'),j,k) for i,j,k in self.points],

                )


# class Tag
#         tagdict
#
class Tag( object ):

    def __init__(self, epc, time, messageQ):
        self.epc = epc
        self.tagdict = {}
        self.firstTime = time
        self.lastTime = time
        self.count = 0
        self.readers = 0
        self.forwarded = False
        self.stray = False
        self.gone = False
        self.missing = False
        self.goneMsgTime = datetime.datetime.utcfromtimestamp(0)
        self.messageQ = messageQ

    #def __str__(self):
    #        return self.__repr__()

    def elapsedTime(self):
        return (getTimeNow() - self.lastTime).total_seconds()

    def readCount(self, readerid):
        if readerid is None:
            return self.count
        try:
            return self.tagdict[readerid].count
        except:
            return self.count

    def add(self, readerID, discoveryTime, RSSI, phaseAngle):

        self.count += 1
        self.lastTime = discoveryTime

        try:
            taginfo = self.tagdict[readerID]
        except:
            taginfo = TagInfo(discoveryTime, RSSI, self.epc)
            self.tagdict[readerID] = taginfo
            self.readers += 1

        self.gone = False
        self.missing = False

        if not taginfo.add(discoveryTime, RSSI, phaseAngle):
            self.stray = True
            if (taginfo.lastTime - taginfo.seenMsgTime).total_seconds() > 60:
                taginfo.seenMsgTime = getTimeNow()
                print('[%d] %-20s %s %d STATIONARY TAG' % (readerID, self.epc, taginfo.lastTime.strftime('%a %b %d %H:%M:%S.%f %Z %Y-%m-%d'), taginfo.count), file=sys.stderr)
                #doWarning(self.messageQ, readerID, self.epc, self.count, 'STATIONARY in FOV')
                present = True
            return False

        return True


    def checkIfGone(self):
        strayLimit = 10
        elapsed = (getTimeNow() - self.lastTime).total_seconds()
        if elapsed < strayLimit:
            print('tag.checkIfGone: %s stray elapsed: %6.2f < %3.1f' % (self.epc, elapsed, strayLimit), file=sys.stderr)
            return True

        print('tag.checkIfGone: %s stray elapsed: %6.2f > %3.1f' % (self.epc, elapsed, strayLimit), file=sys.stderr)
        self.gone = True
        return False

    def checkIfMissing(self):
        goneLimit = 20
        elapsed = (getTimeNow() - self.lastTime).total_seconds()
        if elapsed < goneLimit:
            print('tag.checkIfMissing: %s gone elapsed: %6.2f < %3.1f' % (self.epc, elapsed, goneLimit), file=sys.stderr)
            return True

        print('tag.checkIfMissing: %s gone elapsed: %6.2f > %3.1f' % (self.epc, elapsed, goneLimit), file=sys.stderr)
        self.missing = True
        self.goneMsgTime = getTimeNow()
        print('[%d] %-20s %s %d STATIONARY TAG GONE' % (0, self.epc, self.lastTime.strftime('%a %b %d %H:%M:%S.%f %Z %Y-%m-%d'), self.count), file=sys.stderr)
        #doWarning(self.messageQ, 0, self.epc, self.count, 'STATIONARY TAG GONE')
        return False

    def checkIfStillMissing(self):
        missingLimit = 40
        elapsed = (getTimeNow() - self.lastTime).total_seconds()
        if elapsed < missingLimit:
            print('tag.checkStillMissing: %s missing elapsed: %6.2f < %3.1f' % (self.epc, elapsed, missingLimit), file=sys.stderr)
            return True
        print('tag.checkStillMissing: %s still missinged: %6.2f > %3.1f' % (self.epc, elapsed, missingLimit), file=sys.stderr)
        return False


    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return '<firstTime: %s lastTime: %s count: %d readers: %d forwarded: %s stray: %s gone: %s \n%s>' % (
                self.firstTime.strftime('%H%M%S.%f'),
                self.lastTime.strftime('%H%M%S.%f'),
                self.count,
                self.readers,
                self.forwarded,
                self.stray,
                self.gone,
                self.tagdict)


#
#

class Algorithm (Enum):
    none = 0
    singleton = 1
    average = 2
    maxDB = 3
    QFit = 4
    Ransac = 5


class Estimate(object):
    def __init__(self, reader, epc, taginfo, default):
        self.epc = epc
        self.reader = reader
        self.maxRead = taginfo.getMaxRSSIRead()
        self.default = default
        #self.default = 'Max'
        #self.default = 'Avg'
        self.getBest(taginfo)

    def set(self, x, y, d, c, a, e, f, l):
        self.t = f + datetime.timedelta(seconds = x)
        self.x = x
        self.y = y
        self.det = d
        self.count = c
        self.alg = a
        self.err = e
        self.firstTime = f
        self.lastTime = l
        return self

    def average(self, x1, y1, count, err, f, l):
        x, y = self.maxRead
        if self.default == Algorithm.maxDB:
            return self.set(x, y, 0, count, Algorithm.maxDB, err, f, l)
        else:
            return self.set(sum(x1) / count, sum(y1) / count, 0, count, Algorithm.average, err, f, l)

    def polyfit(self, x1, y1):

        print('polyfit: x0: %s' % x1, file=sys.stderr)
        #x1 = [ (i - self.firstTime).total_seconds() for i in x0]

        p1 = np.poly1d(np.polyfit(x1, y1, 2))
        print('polyfit: p1: %s type(p1): %s' % (p1, type(p1)), file=sys.stderr)
        try:
            x2 = np.linspace(x1[0], x1[-1], 100)
            y2 = p1(x2)
            x3 = (p1.coeffs[1]/p1.coeffs[0]/2*(-1))
            y3 = p1(x3)
        except Exception as e:
            print('polyfit: e: %s' % (e), file=sys.stderr)
            print(traceback.format_exc(), file=sys.stderr)
            return None, None, None


        # calculate the determination
        #
        p2 = np.poly1d(p1)
        yhat = p2(x1)
        ybar = np.sum(y1) / len(y1)
        ssreg = np.sum((yhat-ybar)**2)
        sstot = np.sum((y1 - ybar)**2)
        if sstot == 0:
            print('stot zero', file=sys.stderr)
            return None, None, None

        det = ssreg / sstot
        return x3, y3, det

        # sanity check
        #print('polyfit: x3: %8.3f y3: %6.2f det: %4f len: %d' % (x3, y3, det, len(x1)), file=sys.stderr)

        #x = x0[0] + datetime.timedelta(seconds = x3)

        #if x3 < 0 or x3 > 2 or np.isnan(det) or math.isinf(det):
        #        print('polyfit x3 > 2 or det isnan or det isinf', file=sys.stderr)
        #        return x0[0], y1[0], 0, len(x1), '1st'
        #return x, y3, det, len(x1), 'Qfit'




    def getBest(self, taginfo):

        # get best est
        #
        #x, y, det, count, alg = taginfo.bestRSSI()

        x1, y1, count = taginfo.getPoints()

        # check for singleton reads
        #
        if count < 3:
            return self.set(x1[0], y1[0], 0, count, Algorithm.singleton, '', taginfo.firstTime, taginfo.lastTime)

        # check for too few reads, use average
        #
        if count < 10:
            print('getBest[%s]: count: %d < 10' % (self.epc, count), file=sys.stderr)
            return self.average(x1, y1, count, '<10', taginfo.firstTime, taginfo.lastTime)


        # ransac

        #r = QuadRegRemoveOutliersRansac(taginfo.getXY(), test=True, key=taginfo.epc)
        #if r is not None:
        #        a, b, c = r
        #        tr = -b / (2.0 * a)
        #        print('getBest: ransac OK a: %s b: %s c: %s tr: %s' % (a, b, c, tr), file=sys.stderr)

            #li = len(inliers) if inliers is not None else 0
            #lo = len(outliers) if outliers is not None else 0
            #print('getBest: ransac inliers: %d outliers: %d abc: %s' % (li, lo, abc), file=sys.stderr)
            #print('getBest: ransac inliers: %s outliers: %s abc: %s' % (inliers, outliers, abc), file=sys.stderr)
        #else:
        #        print('getBest: ransac NO MODEL', file=sys.stderr)
        #else:
        #        print('getBest: ransac NO RESULTS', file=sys.stderr)
        #print('getBest: ransac IGNORED', file=sys.stderr)

        # get poly fit
        x3, y3, det = self.polyfit(x1, y1)

        # test polyfit results for sanity, if suspect we use the average of times
        #
        if x3 is None:
            print('getBest[%s]: x3 > 2 ' % self.epc, file=sys.stderr)
            return self.average(x1, y1, count, 'stot zero', taginfo.firstTime, taginfo.lastTime)

        if x3 < 0.0:
            print('getBest[%s]: x3 < 0.0 ' % self.epc, file=sys.stderr)
            return self.average(x1, y1, count, 'x3<0', taginfo.firstTime, taginfo.lastTime)

        if x3 > x1[-1]:
            print('getBest[%s]: x3 > %6.2f ' % (self.epc, x1[-1]), file=sys.stderr)
            return self.average(x1, y1, count, 'x3>%6.2f' % x1[-1], taginfo.firstTime, taginfo.lastTime)


        if np.isnan(det):
            print('getBest[%s]: det isnan' % self.epc, file=sys.stderr)
            return self.average(x1, y1, count, 'nan', taginfo.firstTime, taginfo.lastTime)

        if math.isinf(det):
            print('getBest[%s]: det isinf' % self.epc, file=sys.stderr)
            return self.average(x1, y1, count, 'inf', taginfo.firstTime, taginfo.lastTime)

        # polyfit results look sane, return them
        #
        #return x3, y3, det, count, Algorithm.QFit, '', taginfo.firstTime, taginfo.lastTime
        return self.set(x3, y3, det, count, Algorithm.QFit, '', taginfo.firstTime, taginfo.lastTime)

    def __repr__(self):
        return 'Estimate[%s-%s] x: %s y: %s det: %6.2f count: %d alg: %s err: %s first: %s last: %s' % (
                self.reader, self.epc, self.x, self.y, self.det, self.count, self.alg, self.err, self.firstTime, self.lastTime)


class ReaderPriority(Enum):
    none = 0
    pairs = 1
    linear = 2

class Criteria(Enum):
    none = 0
    first = 1
    priority = 2
    qfitOverSingleton = 3
    qfitOverDefault = 4
    defaultOverPoorQfit = 5
    higherDB = 6
    betterQfit = 7
    anythingOverSingleton = 8
    higherCountAndDB = 9
    higherCount = 10
    singletonHigherDB = 11

    def __repr__(self):
        return '%s' % self.bestEstimate




class BestEst(object):

    def __init__(self, epc, readerPositioning, default):
        self.epc = epc
        self.bestEstimate = None
        self.readerHist = dict()
        self.readerPositioning = readerPositioning
        self.readerPriority = ReaderPriority.none
        self.default = default
        self.criteria = Criteria.none
        print('BestEst: readerPriority: %s' % self.readerPriority, file=sys.stderr)

    def readerPri(self, r):
        reader = int(r)
        if self.readerPriority == ReaderPriority.pairs:
            #print('readerPri: pairs readerPriority: %s' % self.readerPriority, file=sys.stderr)
            if reader < 30: return 1
            else: return 0
        elif self.readerPriority == ReaderPriority.linear:
            #print('readerPri: linear readerPriority: %s' % self.readerPriority, file=sys.stderr)
            return -reader
        else:
            #print('readerPri: none readerPriority: %s' % self.readerPriority, file=sys.stderr)
            return 0

    def add(self, r, e):

        k = '%s-%s' % (r, e)

        #self.others[reader] = (t, x, y, )
        self.readerHist[r] = e

        # First?
        #
        if self.bestEstimate is None:
            print('bestEst.add[%s]: First x: %6.2f y: %6.2f det: %4.2f count: %d %s %s' % (k, e.x, e.y, e.det, e.count, e.alg.name, e.err), file=sys.stderr)
            self.set(e, Criteria.first)
            return

        # priority indicates reader positioning, lower priority readers are positioned farther away from the finish line
        # so should only be used as backup.
        #
        print('bestEst.add: priority: self: %d new: %d' % (self.readerPri(e.reader), self.readerPri(self.bestEstimate.reader)), file=sys.stderr)
        if self.readerPri(e.reader) > self.readerPri(self.bestEstimate.reader):
            print('bestEst.add[%s]: x: %6.2f y: %6.2f Save higher priority reader det: %6.2f' % (k, e.x, e.y, e.det), file=sys.stderr)
            self.set(e, Criteria.priority)
            return

        if self.readerPri(e.reader) < self.readerPri(self.bestEstimate.reader):
            print('bestEst.add[%s]: x: %6.2f y: %6.2f Ignorning lower priority reader det: %6.2f' % (k, e.x, e.y, e.det), file=sys.stderr)
            return

        # current estimate was created with QFit
        #
        if e.alg == Algorithm.QFit :
            print('bestEst.add[%s]: Checking: x: %6.2f y: %6.2f QFIT ' % (k, e.x, e.y), file=sys.stderr)
            if self.bestEstimate.alg == Algorithm.singleton and e.det > .30:
                print('bestEst.add[%s]: x: %6.2f y: %6.2f Save QFit over Singleton, det: %6.2f' % (k, e.x, e.y, e.det), file=sys.stderr)
                self.set(e, Criteria.qfitOverSingleton)
                return

            if self.bestEstimate.alg == self.default and e.det > .40:
                print('bestEst.add[%s]: x: %6.2f y: %6.2f Save QFit over %s, det: %6.2f' % (k, e.x, e.y, self.default.name, e.det), file=sys.stderr)
                self.set(e, Criteria.qfitOverDefault)
                return

            if self.bestEstimate.alg == self.default and self.bestEstimate.count < 10:
                print('bestEst.add[%s]: x: %6.2f y: %6.2f Save QFit over %s, det: %6.2f' % (k, e.x, e.y, self.default.name, e.det), file=sys.stderr)
                self.set(e, Criteria.defaultOverPoorQfit)
                return

            if self.bestEstimate.alg == self.default and self.bestEstimate.y > e.y:
                print('bestEst.add[%s]: x: %6.2f y: %6.2f Save %s for higher db, det: %6.2f' % (k, e.x, e.y, self.default.name, e.det), file=sys.stderr)
                self.set(e, Criteria.higherDB)
                return

            if self.bestEstimate.alg == Algorithm.QFit:
                #if e.det > self.bestEstimate.det:

                if (e.count * e.det) > (self.bestEstimate.count * self.bestEstimate.det):
                    print('bestEst.add[%s]: Save: x: %6.2f y: %6.2f QFit over 1st, %6.2f > %6.2f' % (
                        k, e.x, e.y, e.det * e.count, self.bestEstimate.det * self.bestEstimate.count) , 
                          file=sys.stderr)
                    self.set(e, Criteria.betterQfit)
                    return

                print('bestEst.add[%s]: Ignorning: x: %6.2f y: %6.2f QFIT over QFIT' % (k, e.x, e.y), file=sys.stderr)
                return

            print('bestEst.add[%s]: Ignorning: x: %6.2f y: %6.2f QFIT ' % (k, e.x, e.y), file=sys.stderr)
            return

        # current estimate was not created with QFit
        #
        if e.alg == self.default:
            print('bestEst.add[%s]: Checking: x: %6.2f y: %6.2f DEFAULT %s ' % (k, e.x, e.y, self.default.name), file=sys.stderr)
            if self.bestEstimate.alg == Algorithm.singleton:
                print('bestEst.add[%s]: x: %6.2f y: %6.2f Save %s over Singleton, det: %6.2f' % (k, e.x, e.y, self.default.name, e.det), file=sys.stderr)
                self.set(e, Criteria.anythingOverSingleton)
                return

            if self.bestEstimate.alg == self.default and e.count > 5 and e.y > self.bestEstimate.y:
                print('bestEst.add[%s]: x: %6.2f y: %6.2f Save %s over %s, higher db, det: %6.2f' % (k, e.x, e.y, self.default.name, self.default.name, e.det), file=sys.stderr)
                self.set(e, Criteria.higherCountAndDB)
                return

            if self.bestEstimate.alg == self.default and e.count > self.bestEstimate.count:
                print('bestEst.add[%s]: x: %6.2f y: %6.2f Save %s over %s, higher count, det: %6.2f' % (k, e.x, e.y, self.default.name, self.default.name, e.det), file=sys.stderr)
                self.set(e, Criteria.higherCount)
                return

            #if self.bestEstimate.alg == Algorithm.Algorithm.QFit
            #        if det > self.bestEstimate.det:
            #                print('bestEst.add[%s][%s]: Save: x: %6.2f y: %6.2f QFit over 1st, %6.2f > %6.2f' % ( e.x, e.y, e.det, self.det), file=sys.stderr)
            #                self.set(reader, t, e.x, e.y, e.det, e.count, e.alg, e.err)
            #                return

            print('bestEst.add[%s]: Ignorning: x: %6.2f y: %6.2f DEFAULT %s unknown test' % (k, e.x, e.y, self.default.name), file=sys.stderr)
            return


        if e.alg == Algorithm.singleton :

            # XXX Last message in hung err logs
            #
            print('bestEst.add[%s]: Checking: x: %6.2f y: %6.2f Singleton ' % (k, e.x, e.y), file=sys.stderr)

            if self.bestEstimate.alg == Algorithm.singleton:
                if e.y > self.bestEstimate.y:
                    print('bestEst.add[%s]: Save: x: %6.2f y: %6.2f Singleton over Singleton, highest db %6.2f > %6.2f' % (k, e.x, e.y, e.x, self.bestEstimate.x), file=sys.stderr)
                    self.set(e, Criteria.singletonHigherDB)
                    return

                # XXX previous stderr log about float
                print('bestEst.add[%s]: Ignorning: x: %6.2f y: %6.2f Singleton over Singleton' % (k, e.x, e.y), file=sys.stderr)
                return

            if self.bestEstimate.alg == self.default:
                print('bestEst.add[%s]: Ignorning: x: %6.2f y: %6.2f Singleton against %s' % (k, e.x, e.y, self.default.name), file=sys.stderr)
                return
            if self.bestEstimate.alg == Algorithm.QFit:
                print('bestEst.add[%s]: Ignorning: x: %6.2f y: %6.2f Singleton against QFit' % (k, e.x, e.y), file=sys.stderr)
                return

            print('bestEst.add[%s]: Ignorning: x: %6.2f y: %6.2f Singleton unknown test' % (k, e.x, e.y), file=sys.stderr)
            return

        print('bestEst.add[%s]: Ignorning: x: %6.2f y: %6.2f Unknown' % (k, e.x, e.y), file=sys.stderr)
        return

    def getBest(self):
        readerStats = dict()
        print('getBest: bestEstimate: %s' % self.bestEstimate, file=sys.stderr)
        print('getBest: readerHist: %s' % self.readerHist, file=sys.stderr)

        # Iterate across the readerHist for this tag,
        # compute readerStats, save best dbs for each reader
        #
        # XXX
        #for r, e in self.readerHist.iteritems():
        for r, e in six.iteritems(self.readerHist):
            offset = e.x - self.bestEstimate.x
            print('getBest: offset: %s y: %s' % (offset, e.y), file=sys.stderr)
            readerStats[r] = (offset, e.y)

        # return the bestEstimate, criteria used to select best estimate, readerStats
        return self.bestEstimate, self.criteria, readerStats

    def stats(self):
        return

    def set(self, estimate, criteria):
        self.bestEstimate = estimate
        self.criteria = criteria

    def __repr__(self):
        return '%s' % self.bestEstimate

class Bias(object):

    def __init__(self, reader, bias):
        self.reader = reader
        self.bias = [bias]

    def add(self, bias):
        self.bias.append(bias)


class Stat(object):
    def __init__(self, alg, db):
        self.count = 0
        self.qfit = 0
        self.dbtot = 0
        self.add(alg, db)
    def add(self, alg, db):
        self.count += 1
        self.qfit += 1 if alg == Algorithm.QFit else 0
        self.dbtot += db

    def __repr__(self):
        return '[Stat: count: %s qfit: %s]' % ( self.count, self.qfit)

class StatsHistory(object):
    def __init__(self):
        self.history = []

    def append(self, est):
        self.history.append((getTimeNow(), est))

    def getStats(self):
        now = getTimeNow()
        self.history = [x for x in self.history if (now -x[0]).total_seconds() < 60]
        if not len(self.history): return None
        stats = dict()
        for t, e in self.history:
            key = '%s-%s' % (e.reader, e.epc)
            if not key in stats:
                stats[key] = Stat(e.alg,e.y)
            else:
                stats[key].add(e.alg,e.y)
        return stats

#

class SaveAverage(object):
    def __init__(self, offset):
        self.count = 0
        self.offset = 0.0
        self.add(offset)
    def add(self, offset):
        self.count += 1
        self.offset += offset
    def getAverage(self):
        return float(self.offset) / float(self.count)

class ReaderHistory(object):
    def __init__(self):
        self.history = []

    def append(self, readerHist):
        self.history.append((getTimeNow(), readerHist))

    def getReaderStats(self):
        now = getTimeNow()
        self.history = [x for x in self.history if (now -x[0]).total_seconds() < 60]
        if not len(self.history): return None
        offsetAverages = dict()
        dbAverages = dict()
        #print('ReaderHistory.getReaderStats: history: %s' % self.history, file=sys.stderr)
        for t, l in self.history:
            #print('ReaderHistory.getReaderStats: t: %s l: %s' % (t, l), file=sys.stderr)
            for r in l:
                d = l[r]
                #print('ReaderHistory.getReaderStats: r: %s d: %s' % (r, d), file=sys.stderr)
                offset, db = d
                #print('ReaderHistory.getReaderStats: offset: %s db: %s' % (offset, db), file=sys.stderr)

                if not r in offsetAverages:
                    offsetAverages[r] = SaveAverage(offset)
                else:
                    offsetAverages[r].add(offset)
                if not r in dbAverages:
                    dbAverages[r] = SaveAverage(db)
                else:
                    dbAverages[r].add(db)

        readerStats = dict()
        # XXX
        # for r, a in offsetAverages.iteritems():
        for r, a in six.iteritems(offsetAverages):
            #readerStats[r] = a.getAverage()
            readerStats[r] = (offsetAverages[r].getAverage(), dbAverages[r].getAverage())
        return readerStats
