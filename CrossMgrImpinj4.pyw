#try:
#	import MainWin
#except ImportError:
#        print('CrossMgrImpinj4: could not find MainWin, trying CrossMgr/MainWin')

from MainWin4 import MainWin
from MainWin4 import MainLoop

from multiprocessing import freeze_support

if __name__ == '__main__':
	freeze_support()			# Required so that multiprocessing works with py2exe.
	MainLoop()
