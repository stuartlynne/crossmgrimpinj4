CrossMgrImpinj2

This is a forked CrossMgrImpinj.

The changes are:

        1. support up to two Impinj Readers
        2. support up to three CrossMgr hosts



The configuration information is saved in:

        ~/.CrossMgrImpinj2

Each of the two readers can be separately configured:

        1. Enabled / disabled
        2. IP Address or Host name
        3. Antenna selection

Each of up to three CrossMgr hosts can be separately configured:

        1. Enabled 
        2. IP Address

N.B. Reader timestamps are aligned to the local time.

N.B. Tag de-duplication is performed to eliminate duplicate tag reads
from either a single reader or from both readers. Configuration of 
the repeat seconds is in the Advanced configuration area.



Installation requirements for building and testing


Building

Under windows, in Cygwin terminal:

        cmd
        compile3.bat

Linux - apt-get install pythgon-wxgtk2.8

