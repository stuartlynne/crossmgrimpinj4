#Using RSSI for Best Read                                           Stuart Lynne
#Wimsey                                             Sun Apr 01 00:24:06 PDT 2018 


##Overview

There are two ways to operate the RFID reader to scan tags and report a time
for tags.

* First Read
* Best Read


## First Read

First read uses two readers to inventory tags in Single Target mode using 
Sessions 2 and 3. Reporting is done after N tags or a 500ms timeout. This
results in early and fast capture of tags as they enter the FOV. Once read
they do not respond again. This keeps the inventory times low as there
is rarely more than 1 (typically max 4) tags ever being inventoried in a 
single inventory operation. 

Pros:
* fast inventory
* low network use
* less tag interference

Cons:
* tag may be read a significant distance from center of FOV (finish line)



## Best Read

Best read uses the two readers to inventory tags in Dual Target mode using
Sessions 2 and 3. Reporting is done after T attempts, with T set to a low 
number (typically 1). 

The reads are not forwarded immediately, but saved for a short period. As
additional reads are received for a tag, if the RSSI value is higher the
new timestamp is saved. If a tag has not been updated for one second, then
it is forwarded. 

Pros:
* the highest RSSI read has a higher probability of being be the closest to the center of the FOV 

Cons:
* higher processing
* higher network



## Implementation

Currently each ImpinjServer (one per RFID Reader) puts each tag read into the dataQ for each of the CrossMgrServers 
(there is one for each target laptop that is getting tag data.)

A new intermediate server called BestReadServer will be implmented with a single dataQ. The
ImpinjServers will queue data to the dataQ for the BestReadServer.

The BestReadServer will consist of a loop implementing:

        tags
        tagsinqueue
        timeout

        while t = get(true, timeout)
            
            currenttime = 

            for k, v in tags:
                
                if k = t[0] and v[0] >= t[1]
                    # update

                else if v[1] + 1 < currentime
                    # forward
                    # delete


                    


