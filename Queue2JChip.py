import re
import os
import sys
import select
import socket
import time
import threading
import datetime
import pprint
import Impinj3
import traceback
from Impinj3 import ConnectionStatus
from Tags import TagInfo, Tag, BestEst
import numpy as np
from queue import Empty
from queue import Queue
from Utils import readDelimitedData, timeoutSecs, Bell
import keepalive

import os
import datetime
getTimeNow = datetime.datetime.now
HOME_DIR = os.path.expanduser("~")

ConnectionTimeoutSecondsDefault =  10            # Interval for connection timeout
ConnectionRetrySecondsDefault = 6             # Interval for connection timeout

RepeatSecondsDefault                    = 10            # Interval in which a tag is considered a repeat read.


ConnectionTimeoutSeconds        = ConnectionTimeoutSecondsDefault
ConnectionRetrySeconds        = ConnectionRetrySecondsDefault
RepeatSeconds                   = RepeatSecondsDefault



#------------------------------------------------------------------------------
# JChip delimiter (CR, **not** LF)
CR = chr( 0x0d )
bCR = CR.encode()




#------------------------------------------------------------------------------
# Function to format number, lap and time in JChip format
#
# Z413A35 10:11:16.4433 10  10000      C7
count = 0
def xformatMessage( tagID, t ):
    global count
    message = "DA{} {} 10  {:05X}      C7 date={}{}".format(
            tagID,                          # Tag code in decimal, no leading zeros.
            t.strftime('%H:%M:%S.%f'),      # hh:mm:ss.ff
            count,                          # Data index number in hex.
            t.strftime('%Y%m%d'),           # YYYYMMDD
            CR
    )
    count += 1
    return message

def formatMessage( tagID, t ):
    global count
    message = "DA{} {} 10  {:05X}      C7 date={}{}".format(
            tagID,                          # Tag code in decimal, no leading zeros.
            t.strftime('%H:%M:%S.%f'),      # hh:mm:ss.ff
            count,                          # Data index number in hex.
            t.strftime('%Y%m%d'),           # YYYYMMDD
            CR
    )
    count += 1
    return message


class Queue2JChip( object ):
    def __init__( self, dataQ, messageQ, shutdownEvent, crossMgrHost, crossMgrPort, status, name='Impinj4' ):
        ''' Queues:
                        dataQ:                tag/timestamp data to be written out to CrossMgr.
                        messageQ:        queue to write status messages.
                        shutdownQ:        queue to receive shutdown message to stop gracefully.
         '''
        print('CrossMgrServer: __init__ started AAAA', file=sys.stderr)
        self.dataQ = dataQ
        self.messageQ = messageQ
        #self.shutdownQ = shutdownQ
        self.shutdownEvent = shutdownEvent
        self.crossMgrHost = crossMgrHost
        self.crossMgrPort = crossMgrPort
        self.tagCount = 0
        self.status = status
        self.messageQ.put( ('Queue2JChip', '[%s] INIT...' % self.crossMgrHost) )
        self.name = name
        self.emptyMessage = formatMessage( 0, datetime.datetime( 1900, 1, 1, 0, 0, 0 ))

    def xshutdown( self ):
        print('CrossMgrServer: shutdown AAAA', file=sys.stderr)

    def checkKeepGoing( self, msg='', timeout=0):
        #print('CrossMgrServer: checkKeepGoing: %d %s' % (timeout, self.shutdownEvent.is_set()), file=sys.stderr)

        #print('CrossMgrServer[%s]: keepGoing %s %s' % (self.crossMgrHost, not self.shutdownEvent.is_set(), msg), file=sys.stderr)
        #return self.shutdownEvent.is_set()
        return not self.shutdownEvent.wait(timeout=timeout)

    def getCmd( self, sock ):
        # Read from the socket until we get data ending in the delimiter.
        # Handle all exceptions, and return them.
        received = b''
        while not self.shutdownEvent.is_set() and received[-1:] != bCR:
            try:
                ready = select.select([sock], [], [], 6.0)
                if not ready[0]:
                    print('getCmd: timeout', file=sys.stderr)
                    return received.decode(), 'Timeout'
                received += sock.recv(4096)
            except Exception as e:
                print('getCmd: exception e: %s' %(e), file=sys.stderr)
                return received.decode(), e
        return received[:-1].decode(), None


    def crossMgrRunServer( self ):
        print('CrossMgrServer: crossMgrRunServer AAAA', file=sys.stderr)
        instance_name = '{}--{}-{}'.format(socket.gethostname(), self.name, os.getpid())
        self.messageQ.put( ('Queue2JChip', '[%s] Started' % self.crossMgrHost) )
        self.status[0] = ConnectionStatus.Started
        # XXX
        # with settimeout here things work well for the first connection attempt,
        # but the second connection attempt fails with a very short timeout.
        # This can be seen, connection is reliable and instant when reset is pressed and CrossMgr 
        # is accepting connections. But when CrossMgr is not accepting connections, the connection
        # attempts fail quickly with a short timeout, eventually the connection may get established.
        #
        # Two options:
        #    1. move the settimeout to the top of the next loop
        #    2. move all of the socket creation to the top of the next loop
        while self.checkKeepGoing('mainloop'):
            print('CrossMgrServer: crossMgrRunServer BBBB', file=sys.stderr)
            #self.messageQ.put( ('Queue2JChip', 'state', False) )

            # Old location to create socket and set timeout
            #sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            #sock.settimeout( ConnectionTimeoutSeconds )

            #keepalive.set(sock, after_idle_sec=4, interval_sec=1, max_fails=3)

            print('CrossMgrServer: crossMgrRunServer CCCC', file=sys.stderr)
            self.status[0] = ConnectionStatus.Connecting

            #------------------------------------------------------------------------------
            # Connect to the CrossMgr server.
            self.tagCount = 0
            while self.checkKeepGoing('connect loop'):
                print('CrossMgrServer[%s]: crossMgrRunServer' % (self.crossMgrHost, ), file=sys.stderr)
                self.status[0] = ConnectionStatus.Connecting
                self.messageQ.put( ('Queue2JChip', '%s Trying to connect to CrossMgr timout in %d sec...'%
                        (self.crossMgrHost, ConnectionTimeoutSeconds)) )
                time.sleep( 1 )
                # alternate location to create socket and set timeout
                sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                sock.settimeout( ConnectionTimeoutSeconds )
                keepalive.set(sock, after_idle_sec=4, interval_sec=1, max_fails=3)
                try:
                    sock.connect((self.crossMgrHost, self.crossMgrPort))
                    print('CrossMgrServer[%s]: crossMgrRunServer connected' % (self.crossMgrHost, ), file=sys.stderr)
                    self.messageQ.put( ('Queue2JChip', '[%s] Connected' % (self.crossMgrHost) ))
                    self.status[0] = ConnectionStatus.Connected
                    break
                except socket.error:
                    self.messageQ.put( ('Queue2JChip', '[%s] Socket Error' % (self.crossMgrHost) ))
                    print('CrossMgrServer[%s]: crossMgrRunServer sock error' % (self.crossMgrHost, ), file=sys.stderr)
                    #self.messageQ.put( ('Queue2JChip-Alert', '[%s] CrossMgr Connection Failed.  retrying in %d sec...'%
                    #        (self.crossMgrHost, ConnectionRetrySeconds)) )
                    self.status[0] = ConnectionStatus.Error
                    time.sleep(1)

                    #if not self.checkKeepGoing('connect for loop', ConnectionRetrySeconds):
                    #    break
                    #for t in range(2):
                    #    time.sleep( ConnectionRetrySeconds )
                    #    self.status[0] = ConnectionStatus.Timeout
                    #    if not self.checkKeepGoing('connect for loop'):
                    #        break

            print('CrossMgrServer[%s]: crossMgrRunServer GGGG' % (self.crossMgrHost, ), file=sys.stderr)
            if self.shutdownEvent.is_set():
                print('CrossMgrServer[%s]: crossMgrRunServer HHHH' % (self.crossMgrHost, ), file=sys.stderr)
                break

            # Set the timeout with CrossMgr to 2 seconds.  If CrossMgr fails to respond within this time, re-establish the connection.
            sock.settimeout( 2.0 )

            #------------------------------------------------------------------------------
            self.messageQ.put( ('Queue2JChip', '[%s] CrossMgr Connection succeeded!' % self.crossMgrHost ) )
            self.messageQ.put( ('Queue2JChip', '[%s] Sending identifier ...' % self.crossMgrHost ))
            try:
                sock.sendall("N0000{}{}".format(instance_name, CR).encode() )
            except socket.timeout:
                self.messageQ.put( ('Queue2JChip', '[%s] CrossMgr connection timed out [1].'%(self.crossMgrHost)) )
                self.status[0] = ConnectionStatus.Error
                sock.close()
                continue

            #------------------------------------------------------------------------------
            self.messageQ.put( ('Queue2JChip', '[%s] Waiting for "get time" command from CrossMgr...'%(self.crossMgrHost) ))

            received, e = self.getCmd( sock )
            print('runServer: waiting for get time received: %s e: %s' % (received, e), file=sys.stderr)
            if not self.checkKeepGoing():
                print('runServer: not checkKeepGoing AAAA', file=sys.stderr)
                break
            if e:
                print('runServer: CrossMgr Error e: %s AAAA' % (e), file=sys.stderr)
                self.messageQ.put( ('Queue2JChip', '[%s] CrossMgr error: %s' % (self.crossMgrHost, e, )) )
                sock.close()
                sock = None
                continue
            self.messageQ.put( ('Queue2JChip', '[%s] Received: "{}" from CrossMgr %s' % (self.crossMgrHost, received)) )
            if received != 'GT':
                print('runServer: expected GT', file=sys.stderr)
                self.messageQ.put( ('Queue2JChip', '[%s] Incorrect command (expected GT).' % (self.crossMgrHost,)) )
                sock.close()
                sock = None
                continue
            else:
                self.messageQ.put( ('Queue2JChip', '[%s] received "get time" command from CrossMgr...'%(self.crossMgrHost) ))

            # Send 'GT' (GetTime response to CrossMgr).
            self.messageQ.put( ('Queue2JChip', '[%s] Sending GT (get time) response...' % (self.crossMgrHost,)) )
            # format is GT0HHMMSShh<CR> where hh is 100's of a second.  The '0' (zero) after GT is the number of days running, ignored by CrossMgr.
            dBase = datetime.datetime.now()
            message = 'GT0{} date={}{}'.format(
                dBase.strftime('%H%M%S%f'),
                dBase.strftime('%Y%m%d'),
                CR)
            print('runServer: GT message: %s' % (message), file=sys.stderr)
            self.messageQ.put( ('Queue2JChip', '[%s] %s' % (self.crossMgrHost, message[:-1]) ))
            try:
                sock.sendall( message.encode() )
            except Exception as e:
                self.messageQ.put( ('Queue2JChip', '[%s] CrossMgr exception: %s.' % (self.crossMgrHost, e)) )
                sock.close()
                sock = None
                continue

            #------------------------------------------------------------------------------
            if not self.checkKeepGoing():
                print('runServer: not checkKeepGoing BBBB', file=sys.stderr)
                break

            self.messageQ.put( ('Queue2JChip', '[%s] Waiting for S0000 (send) command from CrossMgr...' % (self.crossMgrHost,)) )
            received, e = self.getCmd( sock )
            print('runServer: waiting for S0000 received: %s e: %s' % (received, e), file=sys.stderr)
            if not self.checkKeepGoing():
                print('runServer: not checkKeepGoing CCCC', file=sys.stderr)
                break
            print('runServer: AAAA', file=sys.stderr)
            if e:
                print('runServer: CrossMgr Error e: %s BBBB' % (e), file=sys.stderr)
                self.messageQ.put( ('Queue2JChip', '[%s] CrossMgr error: %s ' % (self.crossMgrHost, e)) )
                sock.close()
                sock = None
                continue
            print('runServer: BBBB', file=sys.stderr)
            self.messageQ.put( ('Queue2JChip', '[%s] Received: "%s" from CrossMgr' % (self.crossMgrHost, received)) )
            if not received.startswith('S'):
                print('runServer: Expected S0000: %s' % (received), file=sys.stderr)
                self.messageQ.put( ('Queue2JChip', '[%s] Incorrect command (expected S0000).' % (self.crossMgrHost,)) )
                sock.close()
                sock = None
                continue
            self.messageQ.put( ('Queue2JChip', '[%s] Received S0000...' % (self.crossMgrHost)))
            print('runServer: CCCC', file=sys.stderr)


            #------------------------------------------------------------------------------
            # Enter "Send" mode - keep sending data until we get a shutdown.
            # If the connection fails, return to the outer loop.
            #
            self.messageQ.put( ('Queue2JChip', '[%s] Start sending data to CrossMgr...' % (self.crossMgrHost)))
            self.messageQ.put( ('Queue2JChip', '[%s] Waiting for RFID reader data...'   % (self.crossMgrHost)))
            while self.checkKeepGoing():
                # Get all the entries from the receiver and forward them to CrossMgr.
                empty = False
                try:
                    d = self.dataQ.get(timeout=5)
                    if d == 'shutdown':
                        break
                except Empty:
                    #print('Queue2JChip: dataQ Empty', file=sys.stderr)
                    empty = True
                    #print('runServer: empty GT message: %s' % (message), file=sys.stderr)
                    #self.messageQ.put( ('Queue2JChip', '[%s] %s' % (self.crossMgrHost, message[:-1]) ))


                # Expect message if the form [tag, time].
                message = self.emptyMessage if empty else formatMessage( d[0], d[1] )
                try:
                    sock.sendall( message.encode() )
                    if not empty:
                        self.tagCount += 1
                        print('Queue2JChip: message: %s' % (message), file=sys.stderr)
                        self.messageQ.put( ('Queue2JChip', '[%s] %s: %s' % (self.crossMgrHost, self.tagCount, message[:-1])) )
                    else:
                        self.messageQ.put( ('Queue2JChip', '[%s] keep alive' % (self.crossMgrHost, )))
                except Exception as e:
                    print('Queue2JChip: sendall exception: %s' % (e), file=sys.stderr)
                    if not Empty:
                        self.dataQ.put( d )     # Put the data back on the queue for resend.
                    self.messageQ.put( ('Queue2JChip', '[%s] CrossMgr error: %s.' % (self.crossMgrHost, e)) )
                    self.messageQ.put( ('Queue2JChip', '[%s] Attempting to reconnect...' % (self.crossMgrHost,)) )
                    break

            sock.close()
            sock = None

def CrossMgrServer( dataQ, messageQ, shutdownEvent, crossMgrHost, crossMgrPort, status, name ):

    print('CrossMgrServer: started AAAA', file=sys.stderr)
    impinj2JChip = Queue2JChip( dataQ, messageQ, shutdownEvent, crossMgrHost, crossMgrPort, status, name=name )
    print('CrossMgrServer: calling crossMgrRunServer ', file=sys.stderr)
    try:
        impinj2JChip.crossMgrRunServer()
    except Exception as e:
        print('CrossMgrServer: exception %s' % e, file=sys.stderr)
        print(traceback.format_exc(), file=sys.stderr)
