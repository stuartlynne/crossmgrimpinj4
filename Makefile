# vim: shiftwidth=8 tabstop=8 noexpandtab textwidth=0

#

VERSION=$(shell  ./Version.py version)
APP_VER=$(shell  ./Version.py AppVerName)

compiled.py:
	date +'compile_date = "%Y%m%d-%H:%M"' > $@


test:
	@echo APP_VER: \"${APP_VER}\"
	@echo ECG_VER: \"${ECG_VER}\"
	@echo VERSION: \"${VERSION}\"

cprofile: 
	python3 -m cProfile -o MainWin4.pstats MainWin4.py
	gprof2dot -n 4 -f pstats MainWin4.pstats | dot -Tpng -o MainWin4.png


impinj4-console:
	time -p python -m nuitka \
		--onefile \
		--disable-plugin=pyside6 \
		--disable-plugin=tk-inter \
		--noinclude-unittest-mode=nofollow \
    		--windows-icon-from-ico=./images/rfid_5518063.png \
		MainWin4.py
	mv MainWin4.exe ${APP_VER}.exe
	belcarra-signtool ${APP_VER}.exe

impinj4-onefile:
	time -p python -m nuitka \
		--onefile \
	 	--windows-disable-console \
		--disable-plugin=pyside6 \
		--disable-plugin=tk-inter \
		--noinclude-unittest-mode=nofollow \
    		--windows-icon-from-ico=./images/rfid_5518063.png \
		MainWin4.py
	mv MainWin4.exe ${APP_VER}.exe
	belcarra-signtool ${APP_VER}.exe

impinj4-inno: 
	-rm -rf dist
	-mkdir -p dist
	cp -v ${APP_VER}.exe dist/Impinj4.exe
	time -p /cygdrive/c/Program\ Files\ \(x86\)/Inno\ Setup\ 6/ISCC.exe \
		/DName="Impinj4" /DVername="Impinj4 ${VERSION}" /DVersion=${VERSION} \
		/FImpinj4_${VERSION}_setup impinj4.iss
	belcarra-signtool install/Impinj4_${VERSION}_setup.exe
	cd install; zip -eP alpha1 Impinj4_${VERSION}_setup.zip Impinj4_${VERSION}_setup.exe

clean:
	-rm -rf *.exe install
