; -- CrossMgrImpinj2.iss --

[Setup]
AppName=CrossMgrImpinj2
#include "Version.py"
DefaultDirName={pf}\CrossMgrImpinj2
DefaultGroupName=CrossMgrImpinj2
UninstallDisplayIcon={app}\CrossMgrImpinj2.exe
Compression=lzma2/ultra64
SolidCompression=yes
SourceDir=dist
OutputDir=..\install
OutputBaseFilename=CrossMgrImpinj2_Setup
ChangesAssociations=yes

[Registry]
Root: HKCR; Subkey: "CrossMgrImpinj2\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\CrossMgrImpinj2.exe,0"
Root: HKCR; Subkey: "CrossMgrImpinj2\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\CrossMgrImpinj2.exe"" ""%1"""

[Tasks] 
Name: "desktopicon"; Description: "Create a &desktop icon"; 
	
[Files]
Source: "*.*"; DestDir: "{app}"; Flags: recursesubdirs

[Icons]
Name: "{group}\CrossMgrImpinj2"; Filename: "{app}\CrossMgrImpinj2.exe"
Name: "{userdesktop}\CrossMgrImpinj2"; Filename: "{app}\CrossMgrImpinj2.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\CrossMgrImpinj2.exe"; Description: "Launch CrossMgrImpinj2"; Flags: nowait postinstall skipifsilent
