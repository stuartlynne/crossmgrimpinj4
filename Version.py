#!/usr/bin/env python3

import sys

Version = '1.05'
AppVerName=f"Impinj4_{Version}"

if __name__ == "__main__":
    if len(sys.argv) == 2:
        if sys.argv[1] == 'version':
            print(Version)
        elif sys.argv[1] == 'AppVerName':
            print(AppVerName)
