import re
import os
import sys
import time
import math
import socket
import threading
import datetime
import random
import enum
import six
import traceback
import keepalive
from enum import Enum
from queue import Empty
from Utils import readDelimitedData, timeoutSecs, Bell
#import cStringIO as StringIO
from pyllrp.pyllrp import *
import wx

getTimeNow = datetime.datetime.now

HOME_DIR = os.path.expanduser("~")

ConnectionTimeoutSecondsDefault                = 10                # Interval for connection timeout
KeepaliveSecondsDefault                        = 2                # Interval to request a Keepalive message
RepeatSecondsDefault                        = 10                # Interval in which a tag is considered a repeat read.

ImpinjRFControlModeDefault = 0        # max throughput
ImpinjSearchModeDefault = 1        # single target
ImpinjTriggerTypeDefault = 0 # N_Tags or timeout
ImpinjNumberOfTagsDefault = 20
ImpinjNumberOfAttemptsDefault = 1
ImpinjTDefault = 1
ImpinjTimeoutDefault = 500
TransmitPowerDefault = 0
#TransmitPowerLevelTableEntryDefault = 0

ImpinjRFControlMode                 = ImpinjRFControlModeDefault
ImpinjSearchMode                = ImpinjSearchModeDefault

ImpinjTriggerType                 = ImpinjTriggerTypeDefault
ImpinjT                         = ImpinjTDefault
ImpinjTimeout                         = ImpinjTimeoutDefault

ConnectionTimeoutSeconds        = ConnectionTimeoutSecondsDefault
KeepaliveSeconds                = KeepaliveSecondsDefault
RepeatSeconds                        = RepeatSecondsDefault

ReconnectDelaySeconds                = 2                # Interval to wait before reattempting a connection
ReaderUpdateMessageSeconds        = 5                # Interval to print we are waiting for input.

#TransmitPower = 1500
TransmitPower = TransmitPowerDefault
#TransmitPowerLevelTableEntry = TransmitPowerLevelTableEntryDefault

class ConnectionStatus(Enum):
    Disabled = 0
    Started = 1
    Connecting = 2
    Connected = 3
    Finished = 4
    Timeout = 5
    Error = 6

    def name(self):
        if self.value == 0:
            return 'Disabled'
        if self.value == 1:
            return 'Started'
        if self.value == 2:
            return 'Connecting'
        if self.value == 3:
            return 'Connected'
        if self.value == 4:
            return 'Finished'
        if self.value == 5:
            return 'Timeout'
        if self.value == 6:
            return 'Error'
        return 'UNKNOWN'

    def color(self):
        if self.value == 0:
            return None
        if self.value == 2:
            return wx.Colour(255,179,71) # Light Orange
            #return 'red'
        elif self.value == 3:
            return wx.Colour(153,255,153) # Light Green
            #return 'yellow green'
            #return 'green'
        else:
            return wx.Colour(255,153,153) # Light Red
            #return 'pink'

class ImpinjRFControl(Enum):
    MaxThroughput = 0
    Hybrid_80k_160k = 1
    DenseReader_M4_HiSpeed = 2
    DenseReader_M8_LoSpeed = 3
    DenseReader_MaxMiller = 4


class ReadHistory(object):
    def __init__(self):
        self.history = []

    def add(self, count):
        self.history.append((getTimeNow(), count))

    def therate(self):
        now = getTimeNow()
        while len(self.history):
            firstTime, count = self.history[0]
            seconds = (now - firstTime).total_seconds()
            #print('rate: seconds: %6.2f' % seconds, file=sys.stderr)
            if seconds <= 0.0:
                return 0.0
            if seconds < 60:
                totalReads = sum([c for t,c in self.history])
                return float(totalReads) / float(seconds)
            del self.history[0]
            continue
        return float(0)


def GetAddRospecRSSIMessage( MessageID = None, ROSpecID = 123, inventoryParameterSpecID = 1234,
                antennas = None, modeIdentifiers = None, maxNumberOfAntennaSupported = None,
                session = 2, modeIndex = None,
        ):
    #-----------------------------------------------------------------------------
    # Create a read everything Operation Spec message
    #
    if not antennas:        # Default to all antennas if unspecified.
        antennas = [0]

    if maxNumberOfAntennaSupported:
        antennas = [a for a in antennas if a <= maxNumberOfAntennaSupported]

    if modeIndex is None:
        if not modeIdentifiers: # Default to ModeIndex=1000 as this is common.
            modeIdentifiers = [1000]

        # Pick a mode index from those available in the reader.
        for modeIndex in (0,1000):
            if modeIndex in modeIdentifiers:
                break
        else:
            modeIndex = modeIdentifiers[0]  # If we can't find the ones we are looking for, pick a valid one.

    rospecMessage = ADD_ROSPEC_Message( MessageID = MessageID, Parameters = [
            # Initialize to disabled.
            ROSpec_Parameter(
                    ROSpecID = ROSpecID,
                    CurrentState = ROSpecState.Disabled,
                    Parameters = [
                        ROBoundarySpec_Parameter(               # Configure boundary spec (start and stop triggers for the reader).
                            Parameters = [
                                # Start immediately.
                                ROSpecStartTrigger_Parameter(ROSpecStartTriggerType = ROSpecStartTriggerType.Immediate),
                                # No stop trigger.
                                ROSpecStopTrigger_Parameter(ROSpecStopTriggerType = ROSpecStopTriggerType.Null),
                            ]
                        ),

                        AISpec_Parameter(                               # Antenna Inventory Spec (specifies which antennas and protocol to use).
                            AntennaIDs = antennas,          # Use specified antennas.
                            Parameters = [
                                AISpecStopTrigger_Parameter(
                                    AISpecStopTriggerType = AISpecStopTriggerType.Null,
                                ),
                                InventoryParameterSpec_Parameter(
                                    InventoryParameterSpecID = inventoryParameterSpecID,
                                    ProtocolID = AirProtocols.EPCGlobalClass1Gen2,
                                    Parameters = [
                                        AntennaConfiguration_Parameter(
                                            AntennaID = 0,  # All antennas
                                            Parameters = [
                                                C1G2InventoryCommand_Parameter(
                                                    TagInventoryStateAware = False,
                                                    Parameters = [
                                                        C1G2RFControl_Parameter(
                                                                ModeIndex = modeIndex,
                                                                Tari = 0,
                                                        ),
                                                        C1G2SingulationControl_Parameter(
                                                                Session = session,
                                                                TagPopulation = 1,
                                                                TagTransitTime = 0,
                                                        )
                                                        ],
                                                    ),
                                                ],
                                            ),
                                        ],
                                    ),
                                ]
                        ),

                        ROReportSpec_Parameter(                 # Report spec (specifies what to send from the reader).
                                ROReportTrigger = ROReportTriggerType.Upon_N_Tags_Or_End_Of_ROSpec,
                                N = 1,
                                Parameters = [
                                        TagReportContentSelector_Parameter(
                                                EnableAntennaID = True,
                                                EnableFirstSeenTimestamp = True,
                                                EnablePeakRSSI = True,
                                        ),
                                ]
                        ),
                    ]
            )       # ROSpec_Parameter
    ])      # ADD_ROSPEC_Message
    return rospecMessage


class Impinj( object ):

    def ResetAntennaConnectionsCheck(self):
        with self.tAntennaConnectedLastLock:
            self.tAntennaConnectedLast -= datetime.timedelta( days=200 )


    def __init__( self, readerID, dataQ0, messageQ, shutdownEvent, impinjHost, impinjPort, antennaStr, impinjSession,
                    impinjMode, counter, rate, status, autoAntennas, statusCB ):


        print('Impinj: readerID: %d' % readerID, file=sys.stderr)


        self.verbose = 0
        self.readerID = readerID
        self.impinjHost = impinjHost
        self.impinjPort = impinjPort
        self.impinjMode = impinjMode
        self.impinjSession = impinjSession
        self.impinjTriggerType = ImpinjTriggerType
        self.impinjT = ImpinjT
        self.impinjTimeout = ImpinjTimeout
        self.impinjR420 = True
        self.statusCB = statusCB

        self.tAntennaConnectedLast = getTimeNow() - datetime.timedelta( days=200)
        self.tAntennaConnectedLastLock = threading.Lock()

        if not antennaStr:
            self.antennas = []
        else:
            self.antennas = [int(a) for a in antennaStr.split()]


        modes = {
                0: 'high speed 640k',
                1: 'hybrid 80k/160k',
                2: 'dense reader M=4 hispd',
                3: 'dense reader M=8 LoSpd',
                4: 'dense reader MaxMiller' }
        searchModes = { 1: 'Single Target', 2: 'Dual Target' }
        triggerTypes = {
                0: 'Upon seeing N tags or timeout',
                1: 'Upon seeing no new Tags for t ms or timeout',
                2: 'N attempts to see all tags in FOV or timeout'
                }


        self.dataQ0 = dataQ0                        # Queue to write tag reads.
        self.messageQ = messageQ        # Queue to write operational messages.
        self.shutdownEvent = shutdownEvent        # Queue to listen for shutdown.
        self.rospecID = 123
        self.readerSocket = None
        self.timeCorrection = None        # Correction between the reader's time and the computer's time.
        self.start()

        impinjModeStr = modes.get(self.impinjMode)
        impinjSearchModeStr = searchModes.get(2)
        impinjTriggerTypeStr = triggerTypes.get(self.impinjTriggerType)

        print('Impinj: set impinjMode: %s' % (impinjModeStr), file=sys.stderr)
        print('Impinj: set impinjSearchMode: %s' % (impinjSearchModeStr), file=sys.stderr)
        print('Impinj: set impinjTriggerMode: %s' % (impinjTriggerTypeStr), file=sys.stderr)
        print('Impinj: set TransmitPower: %s' % (TransmitPower), file=sys.stderr)

        self.messageQ.put( ('Impinj', '[%s] Started - %s - %s TransmitPower %d' %
                (impinjHost, impinjModeStr, impinjSearchModeStr, TransmitPower)) )
        self.messageQ.put( ('Impinj', '[%s] %d: %s t: %d timeout: %d' % (
                self.impinjHost,
                self.impinjTriggerType,
                impinjTriggerTypeStr,
                ImpinjT,
                ImpinjTimeout,
                )) )


        if self.dataQ0 is not None:
            #self.messageQ.put( ('Impinj', '[%s] Forwarded dataQ0'  % (self.impinjHost)) )
            self.dataQ0.put( (
                    'started',
                    self.readerID,
                    impinjHost,
                    impinjModeStr,
                    impinjSearchModeStr,
                    impinjTriggerTypeStr,
                    ImpinjT,
                    ImpinjTimeout,
                    ) )

        self.counter = counter
        self.rate = rate
        self.status = status                    # Impinj Reader Connection status
        self.autoAntennas = autoAntennas

    def start( self ):
        # Create a log file name.
        #tNow = getTimeNow()
        #dataDir = os.path.join( HOME_DIR, 'ImpinjData' )
        #if not os.path.isdir( dataDir ):
        #        os.makedirs( dataDir )
        #self.fname = os.path.join( dataDir, tNow.strftime('Impinj-%Y-%m-%d-%H-%M-%S.txt') )
        #print ('Impinj:Start open %s' % (self.fname), file=sys.stderr)
        #with open(self.fname, 'w') as pf:
        #        pf.write( 'Tag ID, Discover Time, ActualDiscover, Elapsed, SameTimeFlag, Reader, AntennaID, Sequence, PeakRSSI, TagSeenCount\n' )

        self.tagCount = 0

    #-------------------------------------------------------------------------

    def checkKeepGoing( self, timeout=0 ):
        #print('Impinj3: checkKeepGoing: %d %s' % (timeout, self.shutdownEvent.is_set()), file=sys.stderr)
        return not self.shutdownEvent.wait(timeout=timeout)

        #try:
        #    # Check the shutdown queue for a message.  If there is one, shutdown.
        #    d = self.shutdownQ.get( False )
        #    self.messageQ.put( ('Impinj', 'Impinj [%s] Shutdown'  % self.impinjHost) )
        #    self.keepGoing = False
        #    return False
        #except Empty:
        #    #print( 'Impinj [%d] Continue'  % self.readerID , file=sys.stderr)
        #    #self.messageQ.put( ('Impinj', 'Impinj [%s] Continue'  % self.impinjHost) )
        #    return True


    def reconnectDelay( self ):
        #print('Start delay: %d' % ReconnectDelaySeconds, file=sys.stderr)
        self.checkKeepGoing( ReconnectDelaySeconds )
        #print('End delay: %d' % ReconnectDelaySeconds, file=sys.stderr)

    #-------------------------------------------------------------------------

    def sendCommand( self, message ):
        if self.verbose:
            self.messageQ.put( ('Impinj', '-----------------------------------------------------') )
            self.messageQ.put( ('Impinj', '[%s] Sending Message:\n%s\n'  % (self.impinjHost, message)) )
        try:
            message.send( self.readerSocket )
        except Exception as e:
            self.messageQ.put( ('Impinj-Alert', '[%s] Send command fails: %s'  % (self.impinjHost, e)) )
            return False

        try:
            response = WaitForMessage( message.MessageID, self.readerSocket )
        except Exception as e:
            self.messageQ.put( ('Impinj-Alert', '[%s] Get response fails: %s'  % (self.impinjHost, e)) )
            return False
        #print('Response: %s' % response, file=sys.stderr)
        if self.verbose:
            self.messageQ.put( ('Impinj', '[%s] Received Response:\n%s\n'  % (self.impinjHost, response)) )
        return True, response

    #-------------------------------------------------------------------------

    def sendCommands( self ):
        self.messageQ.put( ('Impinj', '[%d] Connected to: (%s:%d)'  % (self.readerID, self.impinjHost, self.impinjPort) ) )

        print( 'Impinj %s sendCommands\n' % self.impinjHost  , file=sys.stderr)

        if self.verbose:
            self.messageQ.put( ('Impinj', '[%d] Waiting for READER_EVENT_NOTIFICATION...' % self.readerID) )

        response = UnpackMessageFromSocket( self.readerSocket )

        if self.verbose:
            self.messageQ.put( ('Impinj', '\n[%d] Received Response:\n%s\n'  % (self.readerID, response)) )
            print( ('Impinj [%s] Received Response:\n%s\n'  % (self.impinjHost, response)) , file=sys.stderr)

        connectionAttemptEvent = response.getFirstParameterByClass(ConnectionAttemptEvent_Parameter)
        status = str(connectionAttemptEvent)

        self.messageQ.put( ('Impinj', '\n[%d] ConnectionAttemptEvent:\n%s\n'  % (self.readerID, connectionAttemptEvent)) )

        print( ('Impinj [%d] ConnectionAttemptEvent:"%s"'  % (self.readerID, connectionAttemptEvent)) , file=sys.stderr)
        print( ('Impinj [%d] ConnectionAttemptEvent:"%s"'  % (self.readerID, status)) , file=sys.stderr)

        if 'Failed' in status:
            print( 'Impinj [%d] Another connection exists'  % (self.readerID), file=sys.stderr)
            #self.messageQ.put( ('Impinj-Alert', 'Impinj [%s] Another connection exists'  % (self.impinjHost))
            return False




        # Compute a correction between the reader's time and the computer's time.
        readerTime = response.getFirstParameterByClass(UTCTimestamp_Parameter).Microseconds
        readerTime = datetime.datetime.utcfromtimestamp( readerTime / 1000000.0 )
        self.timeCorrection = getTimeNow() - readerTime

        self.messageQ.put( ('Impinj', '[%s] Reader time is %f seconds different from computer time'  %
                (self.readerID, self.timeCorrection.total_seconds())))

        # Reset to factory defaults.
        print( 'Impinj %s READER CONFIG\n' % self.impinjHost  , file=sys.stderr)
        success, response = self.sendCommand( SET_READER_CONFIG_Message(ResetToFactoryDefault = True) )
        if not success:
            #print( 'Impinj %s READER CONFIG failed\n' % self.impinjHost  , file=sys.stderr)
            return False
        #print('Capabilities: %s' % response, file=sys.stderr)

        # Get the connected antennas.
        self.getConnectedAntennas()

        # Enable Impinj extensions
        #print( 'Impinj %s  ENABLE EXTENSIONS\n' % self.impinjHost  , file=sys.stderr)
        success, response = self.sendCommand( IMPINJ_ENABLE_EXTENSIONS_Message() )
        if not success:
            print( 'Impinj %s  ENABLE EXTENSIONS FAILED\n' % self.impinjHost  , file=sys.stderr)
            return False
        else:
            print( 'Impinj %s  ENABLE EXTENSIONS OK\n' % self.impinjHost  , file=sys.stderr)
        #print('Capabilities: %s' % response, file=sys.stderr)

        # Get Capabilities

        print( 'Impinj %s  GET READER CAPABILITIES\n' % self.impinjHost  , file=sys.stderr)

        #success, response = self.sendCommand( GET_READER_CAPABILITIES_Message(RequestedData = 3 ) )
        #success, response = self.sendCommand(GET_READER_CAPABILITIES_Message(RequestedData = GetReaderCapabilitiesRequestedData.All ))
        modeIdentifiers = None
        maxNumberOfAntennaSupported = 4
        try:
            success, response = self.sendCommand(GET_READER_CAPABILITIES_Message(RequestedData = GetReaderCapabilitiesRequestedData.All))
            if success:
                modeIdentifiers = [e.ModeIdentifier for e in response.getFirstParameterByClass(C1G2UHFRFModeTable_Parameter).Parameters]
                gdc = response.getFirstParameterByClass(GeneralDeviceCapabilities_Parameter)
                maxNumberOfAntennaSupported = gdc.MaxNumberOfAntennaSupported
                print('Impinj: modeIdentifiers: %s' % modeIdentifiers, file=sys.stderr)
            else:
                self.messageQ.put( ('Impinj', 'GET_READER_CAPABILITIES fails.') )
        except Exception as e:
            self.messageQ.put( ('Impinj', 'GET_READER_CAPABILITIES Exception: {}:\n{}'.format(e, traceback.format_exc())) )

        if not success:
            #print( 'Impinj %s  GET READER CAPABILITIES FAILED\n' % self.impinjHost  , file=sys.stderr)
            return False
        #print('Capabilities: %s' % response, file=sys.stderr)

        index = 0
        pindex = 0
        power = 0
        model = ''
        # iterate across the response parameters to look at Regulatory Capabilities
        for r in response.Parameters:

            print('response.Parameters: %s' % r, file=sys.stderr)

            # look for Regulatory Capabilities instance
            #if not isinstance (r, RegulatoryCapabilities_Parameter) :
            #        next

            # iterate across Regulatory Capabilities Parameters to find UHF Band Capabilties
            if isinstance (r, RegulatoryCapabilities_Parameter) :
                for u in r.Parameters:

                    # look for UHF Band Capabilities instance
                    if not isinstance (u, UHFBandCapabilities_Parameter):
                        next

                    # iterate the UHB Band Capabilities to get the maximum Transmit Power Index
                    for t in u.Parameters:

                        # look for entries that are a Transmit Power Level Table Entry class
                        if isinstance(t, TransmitPowerLevelTableEntry_Parameter) :
                            print('transmitPowerValue[%d] %d > %d looking for %d' % (t.Index, t.TransmitPowerValue, power, TransmitPower), file=sys.stderr)

                            if not TransmitPower or t.TransmitPowerValue < TransmitPower:
                                pindex = t.Index
                                power = t.TransmitPowerValue
                                index = t.Index
                                continue

                            if t.TransmitPowerValue > TransmitPower and power == 0:
                                power = t.TransmitPowerValue
                                index = t.Index

            if isinstance (r, ImpinjDetailedVersion_Parameter) :
                print('impinj model: %s' % r.ModelName, file=sys.stderr)
                model = r.ModelName


        print('model: "%s"' % model, file=sys.stderr)
        #if model == 'Impinj Speedway R1000':
        if 'R1000' in model:
            print('R1000', file=sys.stderr)
            self.impinjR420 = False


        print('[%d] %s CAPABILITY max index: %d power: %d'  % (self.readerID, model, index, power), file=sys.stderr)
        self.messageQ.put( ('Impinj', '[%d] %s CAPABILITY max index: %d power: %d'  % (self.readerID, model, index, power)))
        receiverSensitivityParameter = []
        transmitPowerParameter = []

        #testIndex = [50, 30, 30, 16]
        # XXX
        print('TransmitPower: %s index: %d pindex: %s' % (TransmitPower, index, pindex), file=sys.stderr)
        if TransmitPower > 0:
            index = pindex
        print('index: %d' % index, file=sys.stderr)
        #index = testIndex[self.readerID-1]
        #print('index[%d]: %d TEST' % (self.readerID, index), file=sys.stderr)
        #index = 25
        #index = 30
        #index = 45
        #index = 50
        # XXX

        # send a reader configuration message
        #
        # N.B. This should be customized for the specific reader
        # N.B. Currently this is set up for R1000 and R420,
        # N.B. Specifically, this may not work for R120, R220, which may not support mode 0 or 1
        # (from Octane manual)
        #
        print('Sending READER CONFIG mode: %d session: %d' % (self.impinjMode, self.impinjSession), file=sys.stderr)
        success, response = self.sendCommand( SET_READER_CONFIG_Message(Parameters = [
                AntennaConfiguration_Parameter(
                        AntennaID = 0,
                        Parameters = receiverSensitivityParameter + transmitPowerParameter + [
                                RFTransmitter_Parameter(TransmitPower = index, HopTableID = 1, ChannelIndex = 0), C1G2InventoryCommand_Parameter(
                                        Parameters = [
                                                C1G2RFControl_Parameter(
                                                        #ModeIndex = 2,              # dense reader M=4 hispd
                                                        #ModeIndex = 1,              # hybrid
                                                        #ModeIndex = 0,              # max throughput
                                                        ModeIndex = self.impinjMode,
                                                ),
                                                C1G2SingulationControl_Parameter(
                                                        Session = self.impinjSession,
                                                        TagPopulation = 1,
                                                        TagTransitTime = 0,
                                                ),
                                                ImpinjInventorySearchMode_Parameter(
                                                        #InventorySearchMode = 1, # "Single_Target",
                                                        InventorySearchMode = 2, # "Dual_Target",
                                                        #InventorySearchMode = self.impinjSearchMode,
                                                ),
                                        ]
                                        ),
                                ]
                ),
        ] ) )

        # Configure a period Keepalive message.
        print('sending keeepalive config', file=sys.stderr)
        success, response = self.sendCommand( SET_READER_CONFIG_Message(Parameters = [
                                KeepaliveSpec_Parameter(        KeepaliveTriggerType = KeepaliveTriggerType.Periodic, PeriodicTriggerValue = int(KeepaliveSeconds*1000),),
                        ] ) )
        if not success:
            return False

        # Get Antenna Configuration

        if not self.antennas:
            #print( ('Impinj Checking Antenna Connectons AAA') , file=sys.stderr)
            success, response = self.sendCommand( GET_READER_CONFIG_Message(RequestedData = 2 ) )
            if not success:
                return False
            self.autoAntennas[0] = []
            for p in response.Parameters:
                if isinstance (p, AntennaProperties_Parameter) :
                    if p.AntennaConnected:
                        self.autoAntennas[0].append(p.AntennaID)
        else:
            self.autoAntennas[0] = self.antennas


        self.messageQ.put( ('Impinj', '[%d] CAPABILITY max index: %d power: %d Antennas: %s Auto: %s'  %
                (self.readerID, index, power, self.antennas, self.autoAntennas[0])))
        #self.messageQ.put( ('Impinj', 'Antennas: %s Auto: %s'  % (self.impinjHost, self.antennas), self.autoAntennas[0]))


        # Disable all rospecs in the reader.
        success, response = self.sendCommand( DISABLE_ROSPEC_Message(ROSpecID = 0) )
        if not success:
            return False

        # Delete our old rospec.
        success, response = self.sendCommand( DELETE_ROSPEC_Message(ROSpecID = self.rospecID) )
        if not success:
            return False

        # Configure our new rospec.
        if False:
            cmd = GetBasicAddRospecMessage(
                        ROSpecID = self.rospecID,
                        antennas = self.autoAntennas[0],

                        # XXX
                        # impinjR420 = self.impinjR420,
                        # impinjMode = self.impinjMode,

                        #triggerType = ImpinjTriggerType,
                        #numberOfTags = ImpinjNumberOfTags,
                        #T = ImpinjT,
                        #timeout = ImpinjTimeout
                        )
        else:
            cmd = GetAddRospecRSSIMessage(
                        ROSpecID = self.rospecID,
                        antennas = self.autoAntennas[0],
                        modeIdentifiers = [self.impinjMode],
                        session = self.impinjSession,
                        inventoryParameterSpecID = 1234,
                        modeIndex = self.impinjMode
                        )
        success, response = self.sendCommand(cmd)
        if not success:
            return False

        # Enable our new rospec.
        success, response = self.sendCommand( ENABLE_ROSPEC_Message(ROSpecID = self.rospecID) )
        if not success:
            return False


        success = (success and isinstance(response, ENABLE_ROSPEC_RESPONSE_Message) and response.success())

        success, response = self.sendCommand( GET_READER_CONFIG_Message(RequestedData = 2 ) )
        if not success:
            return False

        return success

    #-------------------------------------------------------------------------

    def getConnectedAntennas( self ):
        success, response = self.sendCommand( GET_READER_CONFIG_Message(RequestedData=GetReaderConfigRequestedData.AntennaProperties) )
        if success:
            self.connectedAntennas = [p.AntennaID for p in response.Parameters
                    if isinstance(p, AntennaProperties_Parameter) and p.AntennaConnected and p.AntennaID <= 4]
            return success

    #-------------------------------------------------------------------------

    def impinjRunServer( self ):

        #self.messageQ.put( ('BackupFile', self.fname) )

        self.messageQ.put( ('Impinj', '*****************************************' ) )
        self.messageQ.put( ('Impinj', '[%d] Reader Server Started: (%s:%d)'  % (self.readerID, self.impinjHost, self.impinjPort) ) )

        #self.status[0] = 'Started';
        self.status[0] = ConnectionStatus.Started

        # Create an old default time for last tag read.
        tOld = getTimeNow() - datetime.timedelta( days = 100 )

        while self.checkKeepGoing():

            #------------------------------------------------------------
            # Connect Mode.
            #

            # Create a socket to connect to the reader.
            self.readerSocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            self.readerSocket.settimeout( ConnectionTimeoutSeconds )
            keepalive.set(self.readerSocket, after_idle_sec=4, interval_sec=1, max_fails=3)


            #self.messageQ.put( ('Impinj', '') )
            #self.messageQ.put( ('Impinj-Alert', 'Trying to Connect to Reader: (%d:%d)...' % (self.readerID, self.impinjPort) ) )
            #self.messageQ.put( ('Impinj-Alert', 'ConnectionTimeout={:.2f} seconds'.format(ConnectionTimeoutSeconds) ) )

            #self.status[0] = 'Connecting';
            self.status[0] = ConnectionStatus.Connecting

            try:
                print( 'Impinj %s Connecting timeout: %d' % (self.impinjHost, ConnectionTimeoutSeconds) , file=sys.stderr)
                self.readerSocket.connect( (self.impinjHost, self.impinjPort) )
            except Exception as e:
                #print( 'Impinj %s Timed out Retry in %d' % (self.impinjHost, ReconnectDelaySeconds), file=sys.stderr)
                self.status[0] = ConnectionStatus.Timeout
                #self.messageQ.put( ('Impinj-Alert', 'Reader Connection Failed: %s' % e ) )
                self.readerSocket.close()
                #self.messageQ.put( ('Impinj-Alert', 'Attempting Reconnect in %d seconds...' % ReconnectDelaySeconds) )
                self.reconnectDelay()
                continue

            print( 'Impinj %s CONNECTED\n' % self.impinjHost  , file=sys.stderr)

            try:
                print( 'Impinj %s SENDCOMMANDS' % self.impinjHost  , file=sys.stderr)
                success = self.sendCommands()
            except Exception as e:
                print( ('Impinj %s' % self.impinjHost, 'Send Command Error=%s' % e) , file=sys.stderr)
                print(traceback.format_exc(), file=sys.stderr)
                self.messageQ.put( ('Impinj-Alert', 'Send Command Error=%s' % e) )
                success = False

            if not success:
                print( 'Impinj %s INIT FAILED' % self.impinjHost  , file=sys.stderr)
                self.messageQ.put( ('Impinj-Alert', 'Reader Initialization Failed.') )
                self.messageQ.put( ('Impinj-Alert', 'Disconnecting Reader.' ) )
                self.readerSocket.close()
                #self.messageQ.put( ('Impinj-Alert', 'Attempting Reconnect in %d seconds...' % ReconnectDelaySeconds) )
                self.status[0] = ConnectionStatus.Error
                self.reconnectDelay()
                continue

            print( 'Impinj %s CONNECTED\n' % self.impinjHost  , file=sys.stderr)
            #self.status[0] = 'Connected';
            self.status[0] = ConnectionStatus.Connected
            tUpdateLast = tKeepaliveLast = getTimeNow()
            self.tagCount = 0

            readCount = 0
            readhistory = ReadHistory()

            tUpdateLast = tKeepaliveLast = getTimeNow()
            antennaCheckInterval = 10.0
            with self.tAntennaConnectedLastLock:
                self.tAntennaConnectedLast = tUpdateLast - datetime.timedelta(seconds=antennaCheckInterval-2.0)

            while self.checkKeepGoing():

                #print('Impinj[%d]:------------------------------', % (self.readerID), file=sys.stderr)
                arate = readhistory.therate()
                #self.rate[0] = rate

                t = getTimeNow()
                #print('Impinj[%d]: t: %s last: %s seconds: %s' % (self.readerID, t, self.tAntennaConnectedLast, (t - self.tAntennaConnectedLast).total_seconds()), file=sys.stderr)
                if (t - self.tAntennaConnectedLast).total_seconds() >= antennaCheckInterval:
                    #print( ('Impinj[%d] Checking Antenna Connectons' % (self.readerID)) , file=sys.stderr)
                    #self.messageQ.put( ('Impinj[%d]' % (self.readerID), 'Checking Antenna Connectons') )
                    try:
                        GET_READER_CONFIG_Message(RequestedData=GetReaderConfigRequestedData.AntennaProperties).send( self.readerSocket )
                        with self.tAntennaConnectedLastLock:
                            self.tAntennaConnectedLast = t
                    except Exception as e:
                        self.messageQ.put( ('Impinj', 'GET_READER_CONFIG send fails: {}'.format(e)) )
                        self.readerSocket.close()
                        self.messageQ.put( ('Impinj', 'Attempting Reconnect...') )
                        break

                #------------------------------------------------------------
                # Read Mode.
                #
                try:
                    response = UnpackMessageFromSocket( self.readerSocket )
                except socket.timeout:
                    t = getTimeNow()

                    if (t - tKeepaliveLast).total_seconds() > KeepaliveSeconds * 2:
                        print( ('Impinj', 'Reader Connection Lost (missing Keepalive).') , file=sys.stderr)
                        self.messageQ.put( ('Impinj-Alert[%d]' % (self.readerID), 'Reader Connection Lost (missing Keepalive).') )
                        self.readerSocket.close()
                        print( ('Impinj', 'Attempting Reconnect...') , file=sys.stderr)
                        self.messageQ.put( ('Impinj-Alert[%d]' % (self.readerID), 'Attempting Reconnect...') )
                        break

                    if (t - tUpdateLast).total_seconds() >= ReaderUpdateMessageSeconds:
                        print( ('Impinj', 'Listening for Impinj reader data...') , file=sys.stderr)
                        self.messageQ.put( ('Impinj-Alert[%d]' % (self.readerID), 'Listening for Impinj reader data...') )
                        tUpdateLast = t

                    print('Impinj: timeout', file=sys.stderr)
                    continue

                if isinstance(response, KEEPALIVE_Message):
                    #print('Impinj[%d]: KEEPALIVE' % (self.readerID), file=sys.stderr)

                    # Respond to the KEEP_ALIVE message with KEEP_ALIVE_ACK.
                    try:
                        KEEPALIVE_ACK_Message().send( self.readerSocket )
                    except socket.timeout:
                        print( ('Impinj', 'Reader Connection Lost (Keepalive_Ack timeout).') , file=sys.stderr)
                        self.messageQ.put( ('Impinj-Alert[%d]' % (self.readerID), 'Reader Connection Lost (Keepalive_Ack timeout).') )
                        self.readerSocket.close()
                        print( ('Impinj[%d]' % (self.readerID), 'Attempting Reconnect...') , file=sys.stderr)
                        self.messageQ.put( ('Impinj-Alert[%d]' % (self.readerID), 'Attempting Reconnect (keepalive)...') )
                        break

                    tKeepaliveLast = getTimeNow()
                    continue


                #------------------------------------------------------------
                # Reader config (to get antenna connection status).
                #
                if isinstance(response, GET_READER_CONFIG_RESPONSE_Message):
                    self.connectedAntennas = sorted( p.AntennaID for p in response.Parameters
                            if isinstance(p, AntennaProperties_Parameter) and p.AntennaConnected and p.AntennaID <= 4 )
                    #print('Impinj[%d]: READER_CONFIG' % (self.readerID), file=sys.stderr)
                    print('Impinj[%d]: READER_CONFIG connectedAntennas: %s' % (self.readerID, self.connectedAntennas), file=sys.stderr)
                    self.messageQ.put( ('Impinj[%d]' % (self.readerID), 'Connected antennas: {}'.format(','.join(str(a) for a in self.connectedAntennas)) ) )
                    #self.statusCB(
                    #        readerID = self.readerID,
                    #        connectedAntennas = self.connectedAntennas,
                    #        timeCorrection = self.timeCorrection,
                    #        )
                    self.autoAntennas[0] = self.connectedAntennas
                    continue


                if not isinstance(response, RO_ACCESS_REPORT_Message):
                    print('Impinj[%d]: non RO_ACCESS_REPORT?' % (self.readerID), file=sys.stderr)
                    if not isinstance(response, READER_EVENT_NOTIFICATION_Message):
                        print( ('impinjRunServerSkipping: %s' % response.__class__.__name__) , file=sys.stderr)
                        self.messageQ.put( ('Impinj', 'Skipping: %s' % response.__class__.__name__) )
                    continue


                #print('Impinj: RO_ACCESS_REPORT? %s' % response, file=sys.stderr)

                readCount += 1
                thisCount = -1
                lastDiscoveryTime = 0
                firstDiscoveryTime = 0
                forwarded = 0
                for tag in reversed(response.getTagData()):
                    if not self.checkKeepGoing():
                        break
                    self.tagCount += 1

                    impinjRFPhaseAngle = 0
                    impinjPeakRSSI = 0
                    impinjRFDopplerFrequency = 0
                    #if thisCount == 0:
                    #        print('--------- %d' % readCount, file=sys.stderr)
                    #print('impinjRunServer[%d] tag: %s' % (self.tagCount, tag), file=sys.stderr)
                    # XXX
                    #for key, data in tag.iteritems():
                    # XXX
                    peakRSSI = 0
                    for key, data in six.iteritems(tag):
                        #print('impinjRunServer: key: %s data: %s' % (key, data), file=sys.stderr)

                        if key == 'ImpinjRFPhaseAngle': impinjRFPhaseAngle = data
                        if key == 'ImpinjPeakRSSI': impinjPeakRSSI = data
                        if key == 'ImpinjRFDopplerFrequency': impinjRFDopplerFrequency = data
                        if key == 'EPC': tagID = data
                        if key == 'AntennaID': antennaID = data
                        if key == 'PeakRSSI': peakRSSI = data
                        if key == 'TagSeenCount': tagSeenCount = data

                    thisCount += 1

                    #self.counter[0] += 1
                    self.counter[0] += 1

                    #if isinstance( tagID, (int, long) ):
                    #        tagID = str(tagID)
                    #else:
                    try:
                        tagID = HexFormatToStr( tagID )
                    except Exception as e:
                        print( ('impinjRunServerReceived {}.  Skipping: HexFormatToStr fails.  Error={}'.format(self.tagCount, e)) , file=sys.stderr)
                        self.messageQ.put( ('Impinj', 'Received {}.  Skipping: HexFormatToStr fails.  Error={}'.format(self.tagCount, e)) )
                        continue

                    try:
                        discoveryTime = tag['Timestamp']  # In microseconds since Jan 1, 1970
                    except Exception as e:
                        print( ('impinjRunServerReceived %d.  Skipping: Missing Timestamp' % self.tagCount) , file=sys.stderr)
                        self.messageQ.put( ('Impinj', 'Received %d.  Skipping: Missing Timestamp' % self.tagCount) )
                        continue

                    # Convert discoveryTime to Python format and correct for reader time difference.
                    discoveryTime = datetime.datetime.utcfromtimestamp( discoveryTime / 1000000.0 ) + self.timeCorrection


                    elapsedTime = 0
                    sameInventoryFlag = 0
                    thisDiscoveryTime = discoveryTime
                    elapsedSeconds = 0

                    if (lastDiscoveryTime != 0) :
                        elapsedTime = discoveryTime - lastDiscoveryTime
                        elapsedSeconds = elapsedTime.total_seconds() * 1000
                        #if (elapsedTime.total_seconds() < 0.005) :
                        if (elapsedTime.total_seconds() < 5) :
                            sameInventoryFlag = 1
                        if (firstDiscoveryTime == 0) :
                            firstDiscoveryTime = lastDiscoveryTime

                    # reset first discovery flag if same time flag not set
                    if (sameInventoryFlag == 0) :
                        firstDiscoveryTime = 0;
                    #else:
                    #        discoveryTime = firstDiscoveryTime

                    lastDiscoveryTime = discoveryTime

                    forwarded += 1
                    print('Impinj[%d%d] %s FORWARDING %s %3.1f %3.2f %4.2f %6.2f'%
                            (self.readerID, antennaID, discoveryTime.strftime('%H%M%S.%f'), tagID,
                            peakRSSI, impinjRFPhaseAngle, impinjPeakRSSI, impinjRFDopplerFrequency), file=sys.stderr)


                    # Put this read on the queue for transmission to CrossMgr.
                    if self.dataQ0 is not None:
                        #self.messageQ.put( ('Impinj', '[%s] Forwarded dataQ0 %s'  % (self.impinjHost, tagID)) )
                        self.dataQ0.put( (
                                        self.readerID,         #1
                                        tagID,                 #2
                                        discoveryTime,         #3
                                        peakRSSI,         #4
                                        impinjRFPhaseAngle,         #5
                                        impinjPeakRSSI,         #6
                                        impinjRFDopplerFrequency, #7
                                        antennaID,         #8
                                        thisCount         #9
                                        ) )

                    # Convert tag and discovery Time
                    #discoveryTimeStr = discoveryTime.strftime('%Y/%m/%d_%H:%M:%S.%f')
                    #self.messageQ.put( ('Impinj', '[{}[{}]:{}] Received {}. tag={}, time={}'.format(
                    #                        self.impinjHost, antennaID, thisCount, self.tagCount, tagID, discoveryTimeStr)) )


                readhistory.add(thisCount+1)
                if (forwarded): print('Impinj FORWARDED %d' % forwarded, file=sys.stderr)

        if self.readerSocket:
            print('ImpinjServer: shutting done connection', file=sys.stderr)
            try:
                response = self.sendCommand( DISABLE_ROSPEC_Message(ROSpecID = 0) )
                response = self.sendCommand( CLOSE_CONNECTION_Message() )
            except socket.timeout:
                pass
            self.readerSocket.close()
            self.readerSocket = None

            #self.status[0] = 'Finished';
            self.status[0] = ConnectionStatus.Finished
            self.messageQ.put( ('Impinj', '[%d] impinjRunServer finished' % self.readerID) )
        return True

    #def purgeDataQ( self ):
    #        while 1:
    #                try:
    #                        d = self.dataQ1.get( False )
    #                except Empty:
    #                        break

def ImpinjServer( readerid, dataQ0, messageQ, shutdownEvent, impinjHost, impinjPort, antennaStr, impinjSession,
                impinjMode, counter, rate, status, autoAntennas, statusCB):
    impinj = Impinj(readerid, dataQ0, messageQ, shutdownEvent, impinjHost, impinjPort, antennaStr, impinjSession,
                    impinjMode, counter, rate, status, autoAntennas, statusCB)
    impinj.impinjRunServer()
