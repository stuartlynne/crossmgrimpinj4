CrossMgrImpinj3 Build                                               Stuart Lynne
Wimsey                                              Wed Mar 21 13:58:19 PDT 2018 

Testing is typically done by running the program directly:


    python Mainwin3.py


Deployment to Windows is done by compiling and building into a Windows Installer.
The kit is built using the:

    compile3.bat


1. Linux

Packages:
    python2.7
    python2.7-pip

Via pip:
    wxpython
    bitstring


2. Windows

Currently we appear to need to use the native Windows python, not cygwin:


Py2exe:
        http://www.jrsoftware.org/isdl.php#stable
        py2exe-0.6.9.win64-py2.7.amd64.exe

N.B. py2exe may complain about missing DLL's. Search for them in Windows
and copy to the c:\pythong27\dll directory.



Inno build:

        http://www.jrsoftware.org/isdl.php#stable




